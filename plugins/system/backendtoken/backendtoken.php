<?php
/**
* plg_backendtoken
*
* Axel < axel[at]quelloffen.com >
* http://www.joomlaconsulting.de
*
* All rights reserved. 
*
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
* plg_backendtoken is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
*
**/

// no direct access
defined('_JEXEC') or die('Restricted access');

jimport( 'joomla.plugin.plugin' );

class plgSystemBackendToken extends  JPlugin 
{

	function plgBackendToken( &$subject, $config = array() )
	{
		parent :: __construct($subject, $config);
		
		$plugin =& JPluginHelper::getPlugin( 'system', 'backendtoken');
 		$this->params = new JParameter( $plugin->params );	
	}

	function onAfterInitialise()
	{
		$app = JFactory::getApplication();

		if( !$app->isAdmin() ) 
		{
			return;
		}
				
		//already logged in
		$user =& JFactory::getUser();
 
		if( !$user->guest )
		{
		 	return;
		}
				
		$token   = $this->params->get('token', 1);
		

		if( JRequest::getMethod() == 'GET' )
		{		
			$request = JRequest::getVar( 'token', 'no token set', 'GET' );
		}
		
		if( JRequest::getMethod() == 'POST' )
		{		
			$ref =  $_SERVER['HTTP_REFERER'];
			$u =& JURI::getInstance( $ref );
			$request = $u->getVar( 'token', 'no token set' );			
		}		
		
		//invalid access token
		if( $token != $request )
		{
			$url = $this->params->get('url' );
			
			//fallback to site
			if( 0 == strlen( $url ) )
			{
				$url = JURI::root();		
			}
			
			$app->redirect( $url );
			die;			
		}
	}
}

?>