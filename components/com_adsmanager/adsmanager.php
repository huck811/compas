<?php
/**
 * @package		AdsManager
 * @copyright	Copyright (C) 2010-2011 JoomPROD.com. All rights reserved.
 * @license		GNU/GPL
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

// Require the com_content helper library
require_once(JPATH_COMPONENT.DS.'controller.php');

// Component Helper
jimport('joomla.application.component.helper');

require_once(JPATH_COMPONENT.DS.'lib'.DS.'core.php');

// Create the controller
/*if(version_compare(JVERSION,'1.6.0','>=')){
	$controller = JController::getInstance('adsmanager');
} else {
	$controller = new AdsmanagerController();
}*/

// Perform the Request task
$doc = JFactory::getDocument();
$task = JRequest::getCmd('task');
//$controller->execute($task);
$renderer   = $doc->loadRenderer('module');
$options    = array('style' => 'raw');
$module     = JModuleHelper::getModule('mod_adsmanager_ads'); // �������� ������
$module->params    = "heading=2\nlimit=10";
echo $renderer->render($module, $options);
 
// Redirect if set by the controller
//$controller->redirect();