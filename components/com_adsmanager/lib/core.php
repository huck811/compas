<?php
// no direct access
defined('_JEXEC') or die('Restricted access');

require_once('phpcompat.php');
require_once('route.php');
require_once('pagination.php');

if ( file_exists( JPATH_ROOT. "/components/com_paidsystem/api.paidsystem.php")) 
{
	require_once(JPATH_ROOT . "/components/com_paidsystem/api.paidsystem.php");
}

$app = JFactory::getApplication();
$templateDir = JPATH_ROOT . '/templates/' . $app->getTemplate();
if (is_file($templateDir.'/html/com_adsmanager/images/nopic.gif')) {
	define('ADSMANAGER_NOPIC_IMG',JURI::base() . 'templates/' . $app->getTemplate().'/html/com_adsmanager/images/nopic.gif');
} else {
	define('ADSMANAGER_NOPIC_IMG',JURI::base() . 'components/com_adsmanager/images/nopic.gif');
}

$db = JFactory::getDBO();
$db->setQuery("SELECT * FROM #__adsmanager_config");
$config = $db->loadObject();
if (isset($config->special))
	define('ADSMANAGER_SPECIAL',$config->special);
else
	define('ADSMANAGER_SPECIAL','');

if (file_exists(JPATH_ROOT.'/components/com_comprofiler/')) {
	define('COMMUNITY_BUILDER',1);
} else {
	define('COMMUNITY_BUILDER',0);
}