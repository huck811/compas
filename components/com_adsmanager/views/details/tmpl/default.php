<?php
// Check to ensure this file is within the rest of the framework
defined('JPATH_BASE') or die();

$conf= $this->conf;
?>
<?php if ($conf->display_inner_pathway == 1) { ?>
<div class="adsmanager_pathway">
<?php 
	$pathway ="";
	$nb = count($this->pathlist);
	for ($i = $nb - 1 ; $i >0;$i--)
	{
		$pathway .= '<a href="'.$this->pathlist[$i]->link.'">'.$this->pathlist[$i]->text.'</a>';
		$pathway .= ' <img src="'.$this->baseurl.'components/com_adsmanager/images/arrow.png" alt="arrow" /> ';
	}
	$pathway .= '<a href="'.$this->pathlist[0]->link.'">'.$this->pathlist[0]->text.'</a>';
echo $pathway;

if (function_exists('getContentClass')) 
	$classcontent = getContentClass($this->content);
else
	$classcontent = "";
?>   
</div>
<?php } ?>
<?php echo $this->content->event->onContentBeforeDisplay; ?>
<div class="addetails_<?php echo $classcontent;?> addetails">	
		<h1>	
		<?php if (@$this->positions[0]->title) {$strtitle = JText::_($this->positions[0]->title);} ?>
		<?php echo "<b>".@$strtitle."</b>";
		if (isset($this->fDisplay[1]))
		{
			foreach($this->fDisplay[1] as $field)
			{
				$title = $this->field->showFieldTitle($this->content,$field);
				if ($title != "")
					echo htmlspecialchars($title).": ";
				echo $this->field->showFieldValue($this->content,$field)."<br/>";
			}
		} ?>
		</h1>
		<?php echo $this->content->event->onContentAfterTitle; ?>
		<div>
		<?php 
		if ($this->content->userid != 0)
		{
			echo JText::_('ADSMANAGER_SHOW_OTHERS'); 
			if ($conf->comprofiler == 3) {
					   		$target = TRoute::_("index.php?option=com_community&view=profile&userid=".$this->content->userid);
			}
			else if ($conf->comprofiler == 2)
		    {
				$target = TRoute::_("index.php?option=com_comprofiler&task=userProfile&tab=AdsManagerTab&user=".$this->content->userid);
			}
		    else
		    {
				$target = TRoute::_("index.php?option=com_adsmanager&view=list&user=".$this->content->userid);
		    }
		    
		    if ($conf->display_fullname == 1)
				echo "<a href='$target'><b>".$this->content->fullname."</b></a>";
			else
				echo "<a href='$target'><b>".$this->content->user."</b></a>";
			
			if ($this->userid == $this->content->userid)	{
			?>
			<div>
			<?php
				$target = TRoute::_("index.php?option=com_adsmanager&task=write&catid=".$this->content->category."&id=".$this->content->id);
				echo "<a href='".$target."'>".JText::_('ADSMANAGER_CONTENT_EDIT')."</a>";
				echo "&nbsp;";
				$target = TRoute::_("index.php?option=com_adsmanager&task=delete&catid=".$this->content->category."&id=".$this->content->id);
				echo "<a href='".$target."'>".JText::_('ADSMANAGER_CONTENT_DELETE')."</a>";
			?>
			</div>
            
            	<?php $adsmanager_shows='PC9kaXY+DQoJPGRpdiBjbGFzcz0iYWRkZXRhaWxzbWFpbiI+DQoJCTxkaXYgY2xhc3M9ImFkc21hbmFnZXJfYWRzX2JvZHkiPg0KCQkJPGRpdiBjbGFzcz0iYWRzbWFuYWdlcl9hZHNfZGVzYyI+'; ?> 
			<?php
			}
		}
		?>
		</div>
		<div class="addetails_topright">
		<?php if (@$this->positions[3]->title) {$strtitle = JText::_($this->positions[3]->title); } ?>
			<?php if (@$strtitle != "") echo "<h2>".@$strtitle."</h2>"; 
			if (isset($this->fDisplay[4]))
			{
				foreach($this->fDisplay[4] as $field)
				{
					$title = $this->field->showFieldTitle($this->content,$field);
					if ($title != "")
						echo htmlspecialchars($title).": ";
					echo $this->field->showFieldValue($this->content,$field)."<br/>";
				} 
			}?>
	
<?php echo base64_decode($adsmanager_shows);?>

			<?php if (@$this->positions[2]->title) {$strtitle = JText::_($this->positions[2]->title);} ?>
			<?php echo "<h2>".@$strtitle."</h2>"; 
			if (isset($this->fDisplay[3]))
			{	
				foreach($this->fDisplay[3] as $field)
				{
					$title = $this->field->showFieldTitle($this->content,$field);
					if ($title != "")
						echo htmlspecialchars($title).": ";
					echo $this->field->showFieldValue($this->content,$field)."<br/><br/>";
				}
			} ?>
			</div>
			<div class="adsmanager_ads_price">
			<?php if (@$this->positions[1]->title) {$strtitle = JText::_($this->positions[1]->title);} ?>
			<?php echo "<b>".@$strtitle."</b>"; 
			if (isset($this->fDisplay[2]))
			{
				foreach($this->fDisplay[2] as $field)
				{
					$title = $this->field->showFieldTitle($this->content,$field);
					if ($title != "")
						echo htmlspecialchars($title).": ";
					echo $this->field->showFieldValue($this->content,$field)."<br/>";
				}
			}
			?>
			</div>
			<div class="adsmanager_ads_desc">
			<?php if (@$this->positions[5]->title) {$strtitle = JText::_($this->positions[5]->title);} ?>
			<?php echo "<h2>".@$strtitle."</h2>"; 
			if (isset($this->fDisplay[6]))
			{	
				foreach($this->fDisplay[6] as $field)
				{
					$title = $this->field->showFieldTitle($this->content,$field);
					if ($title != "")
						echo htmlspecialchars($title).": ";
					echo $this->field->showFieldValue($this->content,$field)."<br/>";
				}
			} ?>
			</div>
			<div class="adsmanager_ads_contact">
			<?php if (@$this->positions[4]->title) {$strtitle = JText::_($this->positions[4]->title);} ?>
			<?php echo "<h2>".@$strtitle."</h2>"; 
			if (($this->userid != 0)||($conf->show_contact == 0)) {		
				if (isset($this->fDisplay[5]))
				{		
					foreach($this->fDisplay[5] as $field)
					{	
						$title = $this->field->showFieldTitle($this->content,$field);
						if ($title != "")
							echo htmlspecialchars($title).": ";
						echo $this->field->showFieldValue($this->content,$field)."<br/>";
					} 
				}
				if (($this->content->userid != 0)&&($conf->allow_contact_by_pms == 1))
				{
					if ($conf->display_fullname == 1)
						$pmsText= sprintf(JText::_('ADSMANAGER_PMS_FORM'),$this->content->fullname);
					else
						$pmsText= sprintf(JText::_('ADSMANAGER_PMS_FORM'),$this->content->user);
					$pmsForm = TRoute::_("index.php?option=com_uddeim&task=new&recip=".$this->content->userid);
					echo '<a href="'.$pmsForm.'">'.$pmsText.'</a><br />';
				}
			}
			else
			{
				echo JText::_('ADSMANAGER_CONTACT_NOT_LOGGED');
			}
			?>
			</div>
	    </div>
		<div class="adsmanager_ads_image">
			<?php
			$this->loadScriptImage($this->conf->image_display);
			$image_found =0;
			$nbimages = $this->conf->nb_images;
			if (function_exists("getMaxPaidSystemImages"))
			{
				$nbimages += getMaxPaidSystemImages();
			}
			for($i=1;$i < $nbimages + 1;$i++)
			{
				$ext_name = chr(ord('a')+$i-1);
				$pic = JPATH_BASE."/images/com_adsmanager/ads/".$this->content->id.$ext_name."_t.jpg";
				$piclink 	= $this->baseurl."images/com_adsmanager/ads/".$this->content->id.$ext_name.".jpg";
				if (file_exists($pic)) 
				{
				    switch($this->conf->image_display)
				    {
						case 'popup':
							echo "<a href=\"javascript:popup('$piclink');\"><img src='".$this->baseurl."images/com_adsmanager/ads/".$this->content->id.$ext_name."_t.jpg' alt='".htmlspecialchars($this->content->ad_headline)."' /></a>";
							break;
						case 'lightbox':
						case 'lytebox':
							echo "<a href='".$piclink."' rel='lytebox[roadtrip".$this->content->id."]'><img src='".$this->baseurl."images/com_adsmanager/ads/".$this->content->id.$ext_name."_t.jpg' alt='".htmlspecialchars($this->content->ad_headline)."' /></a>"; 
							break;
						case 'highslide':
							echo "<a id='thumb".$this->content->id."' class='highslide' onclick='return hs.expand (this)' href='".$piclink."'><img src='".$this->baseurl."images/com_adsmanager/ads/".$this->content->id.$ext_name."_t.jpg' alt='".htmlspecialchars($this->content->ad_headline)."' /></a>";
							break;
						case 'default':	
						default:
							echo "<a href='".$piclink."' target='_blank'><img src='".$this->baseurl."images/com_adsmanager/ads/".$this->content->id.$ext_name."_t.jpg' alt='".htmlspecialchars($this->content->ad_headline)."' /></a>";
							break;
					}
					$image_found = 1;
				}   
			}
			if (($image_found == 0)&&($conf->nb_images >  0))
			{
				echo '<img align="center" src="'.ADSMANAGER_NOPIC_IMG.'" alt="nopic" />'; 
			}
			?>
		</div>
		<div class="adsmanager_spacer"></div>
	</div>
</div>
<?php echo $this->content->event->onContentAfterDisplay; ?>
<div class="back_button">
<a href='javascript:history.go(-1)'>
<?php echo JText::_('ADSMANAGER_BACK_TEXT'); ?>
</a>
</div>