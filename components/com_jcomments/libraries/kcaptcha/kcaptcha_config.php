<?php
$alphabet = "0123456789abcdefghijklmnopqrstuvwxyz";
$allowed_symbols = "23456789";
$fontsdir = 'fonts';	
$length = 3;
$width = 121;
$height = 60;
$fluctuation_amplitude = 2;
$no_spaces = true;
$show_credits = false;
$credits = 'www.joomlatune.ru';
$foreground_color = array(55, 29, 68); 
$background_color = array(254, 233, 242);
$jpeg_quality = 90;
?>