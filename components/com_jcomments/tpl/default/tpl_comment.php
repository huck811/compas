<?php

(defined('_VALID_MOS') OR defined('_JEXEC')) or die;

require_once 'geo/class/app/app.php';



/*
 *
 * Comment item template. Results of rendering used in tpl_list.php
 *
 */
class jtt_tpl_comment extends JoomlaTuneTemplate
{
	function render()
	{
        $comment = $this->getVar('comment');
        
        
        
        $user =& JFactory::getUser();
        $user_id = $user->get('id');

		if (isset($comment)) {
            $ads_manage = new Ads_AdsManage(); 
            $ads_manage->get_ads(false,$comment->object_id);
            $_SESSION['author_id'] = $ads_manage->user_id;

            if ($comment->userid == $_SESSION['author_id'])
                $is_author = 'author';
            else
                $is_author = '';
                
			if ($this->getVar('get_comment_vote', 0) == 1) {
				// return comment vote
			 	//$this->getCommentVoteValue( $comment );
			} else if ($this->getVar('get_comment_body', 0) == 1) {
				// return only comment body (for example after quick edit)
				echo $comment->comment;
			} else {
				// return all comment item
?>
<?php

				$comment_number = $this->getVar('comment-number', 1);
				$thisurl = $this->getVar('thisurl', '');

				$commentBoxIndentStyle = ($this->getVar('avatar') == 1) ? ' avatar-indent' : '';

				if ($this->getVar('avatar') == 1) {
?>
<div class="comment-avatar"><?php echo $comment->avatar; ?></div>
<?php
				}
?>
<div class="comment-avatar"><img src="templates/compas_main/img/no-avatar-comment.gif" height="62" width="62" /></div>
<div class="comment-box<?php echo $commentBoxIndentStyle; ?> <?=$is_author?>"  ><div class="head-comment">
<?php
				if ($this->getVar('comment-show-vote', 0) == 1) {
					//$this->getCommentVote( $comment );
				}
?>
<?php
				if (($this->getVar('comment-show-title') > 0) && ($comment->title != '')) {
?>
<span class="comment-title"><?php echo $comment->title; ?></span> &mdash; 
<?php
				}
				if ($this->getVar('comment-show-homepage') == 1) {
?>
<a class="author-homepage" href="<?php echo $comment->homepage; ?>" rel="nofollow" title="<?php echo $comment->author; ?>"><?php echo $comment->author; ?></a>
<?php
				} else {
?>
<span class="comment-author"><?php echo $comment->author; /*if ($is_author) {echo '<i> / ответ</i>';} else {echo '<i> / вопрос</i>';}*/?> </span>
<?php
				}

?>
<span class="comment-date"><?php echo JCommentsText::formatDate($comment->date, 'd M Y, H:i'); ?></span>
</div>
<div class="comment-body" id="comment-body-<?php echo $comment->id; ?>"><?php echo $comment->comment; ?></div>
<?php
if ($user_id){?>
    <style type="text/css">
        label[for="comments-form-name"], #comments-form-name,label[for="comments-form-email"], #comments-form-email{display: none;}
    </style>
<?php }
				if (($this->getVar('button-reply') == 1)
				|| ($this->getVar('button-quote') == 1)
				|| ($this->getVar('button-report') == 1)) {
?>

<span class="comments-buttons">
<?php            	if ($this->getVar('button-reply') == 1 && ($user_id == $_SESSION['author_id'] && $comment->userid == $user_id) || ($user->groups[8] || $user->groups[7])) {
					   $this->getCommentAdministratorPanel( $comment );
                    }  

           if (!$comment->parent && $is_author=='' && ($user_id == $_SESSION['author_id'] || ($user->groups[8] || $user->groups[7]))){
?>
    <p class="toolbar_reply"><a href="#" title="ответить" onclick="jcomments.showReply(<?php echo $comment->id; ?>); return false;"><img src="templates/compas_main/img/replay-comment.png" width="19" height="16" /></a></p>

<?php
}
				
					if ($this->getVar('button-report') == 1) {
						if ($this->getVar('button-quote') == 1 || $this->getVar('button-reply') == 1) {
?>
 | 
<?php
						}
                        
?>

<a href="#" onclick="jcomments.reportComment(<?php echo $comment->id; ?>); return false;"><?php echo JText::_('BUTTON_REPORT'); ?></a>
<?php
					}
?>
</span>
<?php
				}
?>
</div><div class="clear"></div>
<?php
				// show frontend moderation panel
				
?>
<?php
			}
		}
	}

	/*
	 *
	 * Displays comment's administration panel
	 *
	 */
	function getCommentAdministratorPanel( &$comment )
	{
		if ($this->getVar('comments-panel-visible', 0) == 1) {
?>
<p class="toolbar" id="comment-toolbar-<?php echo $comment->id; ?>">
<?php
			if ($this->getVar('button-edit') == 1) {
				$text = JText::_('BUTTON_EDIT');
?>
	<a class="toolbar-button-edit" href="#" onclick="jcomments.editComment(<?php echo $comment->id; ?>); return false;" title="<?php echo $text; ?>"></a>
<?php
			}

			if ($this->getVar('button-delete') == 1) {
				$text = JText::_('BUTTON_DELETE');
?>
	<a class="toolbar-button-delete" href="#" onclick="if (confirm('<?php echo JText::_('BUTTON_DELETE_CONIRM'); ?>')){jcomments.deleteComment(<?php echo $comment->id; ?>);}return false;" title="<?php echo $text; ?>"></a>
<?php
			}

			if ($this->getVar('button-publish') == 1) {
				$text = $comment->published ? JText::_('BUTTON_UNPUBLISH') : JText::_('BUTTON_PUBLISH');
				$class = $comment->published ? 'publish' : 'unpublish';
?>
	<a class="toolbar-button-<?php echo $class; ?>" href="#" onclick="jcomments.publishComment(<?php echo $comment->id; ?>);return false;" title="<?php echo $text; ?>"></a>
<?php
			}

			if ($this->getVar('button-ip') == 1) {
				$text = JText::_('BUTTON_IP') . ' ' . $comment->ip;
?>
	<a class="toolbar-button-ip" href="#" onclick="jcomments.go('http://www.ripe.net/perl/whois?searchtext=<?php echo $comment->ip; ?>');return false;" title="<?php echo $text; ?>"></a>
<?php
			}

			if ($this->getVar('button-ban') == 1) {
				$text = JText::_('BUTTON_BANIP');
?>
	<a class="toolbar-button-ban" href="#" onclick="jcomments.banIP(<?php echo $comment->id; ?>);return false;" title="<?php echo $text; ?>"></a>
<?php
			}
?>
</p>
<?php
		}
	}

	function getCommentVote( &$comment )
	{
		$value = intval($comment->isgood) - intval($comment->ispoor);

		if ($value == 0 && $this->getVar('button-vote', 0) == 0) {
			return;
		}
?>
<span class="comments-vote">
	<span id="comment-vote-holder-<?php echo $comment->id; ?>">
<?php
		if ($this->getVar('button-vote', 0) == 1) {
?>
<a href="#" class="vote-good" title="<?php echo JText::_('BUTTON_VOTE_GOOD'); ?>" onclick="jcomments.voteComment(<?php echo $comment->id;?>, 1);return false;"></a><a href="#" class="vote-poor" title="<?php echo JText::_('BUTTON_VOTE_BAD'); ?>" onclick="jcomments.voteComment(<?php echo $comment->id;?>, -1);return false;"></a>
<?php
		}
		echo $this->getCommentVoteValue( $comment );
?>
	</span>
</span>
<?php
	}

	function getCommentVoteValue( &$comment )
	{
		$value = intval($comment->isgood - $comment->ispoor);

		if ($value == 0 && $this->getVar('button-vote', 0) == 0 && $this->getVar('get_comment_vote', 0) == 0) {
			// if current value is 0 and user has no rights to vote - hide 0
			return;
		}

		if ($value < 0) {
			$class = 'poor';
		} else if ($value > 0) {
			$class = 'good';
			$value = '+' . $value;
		} else {
			$class = 'none';
		}
?>
<span class="vote-<?php echo $class; ?>"><?php echo $value; ?></span>
<?php
	}
}
?>