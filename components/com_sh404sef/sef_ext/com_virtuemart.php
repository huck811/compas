<?php
/**
 * sh404SEF support for com_content component.
 * @author      $Author: shumisha $
 * @copyright   Yannick Gaultier - 2007-2011
 * @package     sh404SEF-16
 * @license     http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @version     $Id: com_content.php 2384 2012-08-02 15:26:55Z silianacom-svn $
 */

defined( '_JEXEC' ) or die( 'Direct Access to this location is not allowed.' );

// ------------------  standard plugin initialize function - don't change ---------------------------
$sefConfig = & Sh404sefFactory::getConfig();
$shLangName = '';
$shLangIso = '';
$title = array();
$shItemidString = '';
$dosef = shInitializePlugin( $lang, $shLangName, $shLangIso, $option);
if ($dosef == false) return;
// ------------------  standard plugin initialize function - don't change ---------------------------

$originalVars = empty( $originalUri) ? $vars : $originalUri->getQuery( $asArray = true);
ShlSystem_Log::debug( 'sh404sef', 'Loading component own router.php file from inside com_virtuemart.php');
$functionName = ucfirst( str_replace( 'com_', '', $option)) . 'BuildRoute';
if (!function_exists( $functionName)) {
  include(JPATH_ROOT .'/components/' . $option . '/router.php');
}
$helper = vmrouterHelper::getInstance($originalVars);
$menuItem = $helper->menuVmitems;
$shopName = empty($menuItem) ? 'vm' : $menuItem[0]->alias;

// check for shop root url, else normal routing
if(!empty( $originalVars['view']) && $originalVars['view'] == 'virtuemart') {

  // if VM is homepage, then that's fine
  if(!shIsAnyHomepage($string)) {
    // else use menu item alias as slug
    $title[] = $shopName;
    unset( $originalVars['view']);
  }

} else  {

  if(!empty($originalVars['view']) && $originalVars['view'] == 'category') {
    if(!isset( $originalVars['limitstart'])) {
      $limitstart = 0;
      shAddToGETVarsList( 'limitstart', $limitstart);
      shRemoveFromGETVarsList('limitstart');
      // router.php expects this to be start, not limitstart
      $originalVars['start'] = $limitstart;
    } else {
      $originalVars['start'] = $originalVars['limitstart'];
      unset( $originalVars['limitstart']);
    }
  }

  // have router.php build url
  $title = $functionName( $originalVars);
  $title = $pageInfo->router->encodeSegments( $title);

  // add shop menu item, if asked to
  if($sefConfig->shVmInsertShopName) {
    array_unshift( $title, $shopName);
  }

}

// add user defined prefix
$prefix = shGetComponentPrefix( $option);
if (!empty( $prefix)) {
  array_unshift( $title, $prefix);
}

// manage GET var lists ourselves, as Joomla router.php does not do it
if (!empty($vars)) {
  // there are some unused GET vars, we must transfer them to our mechanism, so
  // that they are eventually appended to the sef url
  foreach( $vars as $k => $v) {
    switch ($k) {
      case 'option':
      case 'Itemid':
      case 'lang':
        shRemoveFromGETVarsList( $k);
        break;
      default:
        // if variable has not been used in sef url, add it to list of variables to be
        // appended to the url as query string elements
        if (array_key_exists( $k, $originalVars)) {
          shAddToGETVarsList( $k, $v);
        } else {
          shRemoveFromGETVarsList( $k);
        }
        break;
    }
  }
}

// ------------------  standard plugin finalize function - don't change ---------------------------
if ($dosef){
  $string = shFinalizePlugin( $string, $title, $shAppendString, $shItemidString,
      (isset($limit) ? $limit : null), (isset($limitstart) ? $limitstart : null),
      (isset($shLangName) ? $shLangName : null), (isset($showall) ? $showall : null),
      $suppressPagination = true);
}
// ------------------  standard plugin finalize function - don't change ---------------------------

