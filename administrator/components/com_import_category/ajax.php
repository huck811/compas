<?php

include '../../../geo/class/app/app.php';

define('NUM_PROCESS', 1000);

define('TMP_FILE', BASE.'administrator/components/com_import_category/tmp.txt');

$file = $_POST['file'];

$import = new Import_Category(BASE."administrator/components/com_import_category/f_import/".$file);


$num = $import->get_num();
if (isset($_POST['position']))
    $position = $_POST['position'];
else  
    $position = 0; 
    
     
$i = 0;
$position = ($position > $num) ? $num : $position;

$itogo = (1-($num-$position)/$num);

$procent = floor($itogo*100);

$z = floor($itogo*10);
    

echo 'Всего записей: '.$num.'<br/>';

if ($procent < 97)
    echo "Внимание идет импорт, осталось: <span style='color: red;'>".str_repeat('*',$z)."</span> (".(100-$procent)."%)<br/>";
else
    echo '<span style="color: green; font-weight: bold;">Категории импортированы успешно!!!</span><br/>';    

if ($position == 0)
    unlink(TMP_FILE);
        
$statistic = $import->get_statistic();    
$data = $import->import($position, TMP_FILE);

$num = $import->get_num();

$i = $data['pos'];

/*if ($i !== false && $i <= $num){
    $uri = explode('?',$_SERVER['REQUEST_URI']);
    exit('<script>document.location.href="'.$uri[0].'?option=com_import_category&position='.($i-1).'"</script>');
}*/


echo '-----------------------------------Статистика--------------------------------------<br/>';
echo '<br/> Всего разделов: '.$statistic['category_count'];
echo '<br/> Всего подкатегорий: '.$statistic['subcategory_count'];
echo '<br/> Всего подкатегорий 2го уровня: '.$statistic['subsubcategory_count'];
echo '<br/> Уникальных категорий: '.count($statistic['all_cat_list_uniq']).', дублей: ' . $statistic['cat_duble'];
echo '<br/>Всего производителей: '.$statistic['producers_count'].' Дублей: '.$statistic['producers_count_double'].' Уникальных значений: '.($statistic['producers_count']-$statistic['producers_count_double']);
echo '<br/>----------------------------------------------------------------------------------------<br/>';
echo '<br/><br/>';


//if ($position > $num - NUM_PROCESS)
    echo $import->get_report();

echo '#####'.$num;
