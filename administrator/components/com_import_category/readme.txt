Разработан скрипт который позволяет из текстового списка импортировать разделы/категории/подкатегории/производителей в базу данных.

1) Доступ к скрипту можно получить по адресу site.ru/administrator/index.php?option=com_import_category или выбрав в списке компонентов "Импорт категорий"

2) Файл импорта должен иметь формат "txt". Прежде чем загрузить файл импорта убедитесь что он сохранен в кодировке UTF-8 (ВНИМАНИЕ без BOM!!!). 

3) В компоненте импорта можно загружать сколько угодно файлов. Загруженные файлы переименовываются присвоением текущей даты (r.txt - загружаемый файл, 19_10_2013_r.txt - переименованный файл). 
Все когда либо загруженные файлы можно выбрать из списка и произвести импорт.

4) Типовая структура файла импорта

#СПЕЦТЕХНИКА                                            // раздел
##землеройно-транспортная техника                       // категория
###Бульдозеры                                           //подкатегория
BELL                                                    // производитель
BEML
...
###ГРЕЙДЕРЫ
ADAMS
AKERMAN
....

5) При импорте появиться индикатор, показывающий сколько процентов выполнено. (ВНИМАНИЕ!!! следует дождаться окончания импорта и не следует закрывать страницу или начинать новый импорт)

************РЕКОМЕНДАЦИИ*******************

6) Перед произведением импорта настоятельно рекомендуем сделать бэкап всей базы данных, а лучше 3х таблиц  (tsj35_adsmanager_categories, tsj35_adsmanager_adcat, tsj35_adsmanager_prodcat). При бэкапе лучше исключить таблицы с префиксом "geo__", поскольку они имеют большой "вес" и в импорте категорий никак не задействованы.

7) Если необходимо импортировать с нуля все категории, то лучше удалить все категории из таблиц базы данных перед тем как начинать импорт. Замена всей базы с нуля может потребоваться если большая часть категорий переименованна. Если же необходимо добавить новые категории, то удалять все категории нет необходимости. Скрипт проверяет категории на совпадение в базе  и те категории, которые уже есть в базе дважды не будут добавлены.
Для удаления старых записей о категориях и производителях  зайдите в базу данных через phpmyadmin выберите таблицу tsj35_adsmanager_categories и нажмите кнопку "Empty"  - очистить. То же самое нужно сделать с таблицами tsj35_adsmanager_adcat,  tsj35_adsmanager_producer и  tsj35_adsmanager_prodcat
Примечание: Настройки для работы с базой данных находятся в файле /config.php
 