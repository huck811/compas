<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<?php

include '../geo/class/app/app.php';

define('NUM_PROCESS', 1000);

define('TMP_FILE', BASE.'/administrator/components/com_import_category/tmp.txt');

//$import = new Import_Category(BASE."/administrator/components/com_import_category/f_import/r.txt");


if ($_POST['add_file']){
    $uploaddir = BASE."/administrator/components/com_import_category/f_import/";
    $uploadfile = $uploaddir . date('d_m_Y_') . basename($_FILES['file1']['name']);
    
    $arr = explode('.',$uploadfile);
    
    if ($arr[count($arr)-1] == 'txt'){
        
        if (move_uploaded_file($_FILES['file1']['tmp_name'], $uploadfile)) {
            echo "Файл успешно загружен.<br/>";
        } else {
            echo "не удалось загрузить файл!<br/>";
        }
    }
    else{
        echo 'неверный формат файла';
    }
}

$files = glob(BASE.'/administrator/components/com_import_category/f_import/*.txt');
$i=0;
foreach($files as $f){
  $tmp = explode('/',$f);
  $f = $tmp[count($tmp)-1];
  $files[$i] = $f;
  $i++;  
}

?>
<table width="100%">
    <tr>
        <td>Выберите файл: <select id="file"><?php foreach($files as $f){?><option><?=$f?></option><?php }?></select>&nbsp;&nbsp;<input type="submit" value="Импортировать" onclick="position=0; _import(0)" /></td>
        <td><form enctype="multipart/form-data" action="<?=$_SERVER['REQUEST_URI']?>" method="POST"><input type="file" name="file1" />&nbsp;&nbsp;<input type="submit" name="add_file" value="загрузить файл" /></form></td>
        <td align="right"><a href="/administrator/components/com_import_category/readme.txt" target="_blank">Инструкция</a></td>
         
    </tr>
</table>
<style>
    input[type=submit]:hover{cursor: pointer;}
</style>
<br />
<br />

<div id="result">

</div>
<script>
    var position = 0;
    var file = jQuery('#file').val();
    
    function _import(position){
        if (position == 0){
            file = jQuery('#file').val();
            jQuery('#result').html('Идет импорт...');
        }
        
        jQuery.post(
        
          "/administrator/components/com_import_category/ajax.php",
          {
            position: position,
            file: file
          },
          onAjaxSuccess
        );
    }
    
    function onAjaxSuccess(data){
        arr = data.split('#####');
        
        jQuery('#result').html(arr[0]);
        
        num = arr[1];
        if (position > num)
            return;
        position+=<?=NUM_PROCESS?>;
        
        _import(position);
    }
    

</script>
