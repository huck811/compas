<?php
/**
 * SEF module for Joomla!
 * Originally written for Mambo as 404SEF by W. H. Welch.
 *
 * @author      $Author: shumisha $
 * @copyright   Yannick Gaultier - 2007-2011
 * @package     sh404SEF-16
 * @license     http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @version     $Id: admin.sh404sef.php 2419 2012-08-27 13:25:06Z silianacom-svn $
 */

// Security check to ensure this file is being included by a parent file.
if (!defined('_JEXEC')) die('Direct Access to this location is not allowed.');
if (is_readable( JPATH_ADMINISTRATOR .'/components/com_sh404sef/sh404sef.php')) {
  require_once 'sh404sef.php';
} else {
  echo 'Missing main sh404SEF file: probably corrupted installation.';
}
