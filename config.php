<?php

include_once "configuration.php";
$cfg = new JConfig();

// настройки для подключения к БД (берем из конфига джумлы)
define('SERVER', $cfg->host);
define('DB_NAME', $cfg->db);
define('DB_USER', $cfg->user);
define('DB_PASS', $cfg->password);
define('TABLE_PREFIX', str_replace('_', '', $cfg->dbprefix));

define('SHOW_DB_ERROR', 0);

if ($_SERVER['SERVER_ADDR'] == '192.168.1.34' || $_SERVER['SERVER_ADDR'] == 'phpunit')
  $development = true;
else
  $development = false;

if ($_SERVER['SERVER_ADDR'] == 'phpunit')
  $phpunit = true;

define('NUM_ADS_IN_TOP', 4); // кол-во показываемых объявлений на странице ТОПа. 0 - ТОП не показывается

$cost_category = array(0 => 'Премиум', 2 => 'Средняя+', 1 => 'Средняя', 3 => 'Бюджетная');

define('COST_CATEGORY', serialize($cost_category)); // ценовая категория
//картинки ценовых категорий
$images_cc[] = '/templates/compas_main/img/icon-mag3.png';
$images_cc[] = '/templates/compas_main/img/icon-mag2.png';
$images_cc[] = '/templates/compas_main/img/icon-mag4.png';
$images_cc[] = '/templates/compas_main/img/icon-mag1.png';

define('COST_CATEGORY_IMG', serialize($images_cc));



// Общее время кэширования
define('CACHE_TIME', 720000);
define('ENABLE_CACHE_FUNCTION', true);
define('ENABLE_CACHE_MYSQL', false);

if ($development)
  define('BASE_URL', '/sub/compas/'); // Папка в которой лежит сайт (www.site.ru/papka/). Укажите (/) если файлы располагаются в корне.
else
  define('BASE_URL', '/');

define('CACHE_FOLDER', $_SERVER['DOCUMENT_ROOT'] . BASE_URL . '/cache/');
define('CACHE_CITY_ALL_FILE', 'all_cities.cache');
define('CACHE_CATEGORY_ALL_FILE', 'all_category.cache');
define('CACHE_CATEGORY_LIST', 'category_list.cache');
define('CACHE_CATEGORY_FILE', '_category.cache');
define('CACHE_CITY_BY_REGION_FILE', 'cities_by_region');
define('CACHE_PRODUCER_BY_CATEGORY_FILE', 'producer_by_category');
define('LEVEL_CATEGORY', 2);
define('DIR_SEP', '/');

//Настройки для загрузки изображений
if ($development){
  if (!isset($phpunit))
    define('BASE', '/var/www/sub/compas/');
  else
    define('BASE', 'f:\webservers\home\192.168.1.35\www\sub\compas\\');
}
else
  define('BASE', '/var/www/fcompas/data/www/fcompas.com/');

$uploaddir = BASE . 'images' . DIR_SEP . 'com_ads' . DIR_SEP; // папка, в которой располагаются изображения
define('UPLOAD_DIR', $uploaddir);


define('LOG_CRON_ENABLE', true);
define('LOG_ENABLE', false);
define('ADMIN_EMAIL_FOR_LOG', false);
define('LOG_FILE_CRON', BASE . 'logs/cron.log');
define('LOG_FILE_CRON_ERROR', BASE . 'logs/cron_error.log');
define('LOG_FILE', BASE . 'logs/log.log');
define('LOG_FILE_ERROR', BASE . 'logs/log_error.log');
define('LOG_FILE_DELETE_ADS', BASE . 'logs/log_delete_ads.log');
define('LOG_FILE_CHANGE_ADS', BASE . 'logs/log_change_ads.log');

// Ограничения весовых и габаритных размеров фотографий

define('MAX_FILE_SIZE', 3000);
define('MAX_FILE_AVATAR_SIZE', 3000);
define('MAX_PHOTO_ITEMS', 1);

define('MAX_WIDTH', 800);
define('MAX_HEIGHT', 600);

define('MAX_AVATAR_WIDTH', 299);
define('MAX_AVATAR_HEIGHT', 204);

define('PREVIEW_WIDTH', 299);
define('PREVIEW_HEIGHT', 204);

define('THUMB_PREFIX', 'small_' . PREVIEW_WIDTH . '_' . PREVIEW_HEIGHT . '_');
define('NUM_ADS_ON_PAGE', 20); // кол-во миниатюр на странице под основным изображением

define('MAX_BRAND_NUM', 50); // максимальное количество брендов для одной карточки компании


define('WELLCOME', '<span class="welcome">Добро пожаловать! <br/>Всегда рады Вас видеть!</span>');
define('PROFILE_INFO', '<i style="font-size:10px;">После добавления компании, вы можете изменить информацию о<br/> предоставляемых брендах на странице редактирования компании</i>');

define('VALIDATE_FORM', 1); // валидация форм
define('DEFAULT_MIN_MAX_YEAR_OPTION', 'Не важно');
define('DEFAULT_SELECT_VAL', 'Выбрать');
define('DEFAULT_SELECT_VAL1', 'Выберите город');

define('NEW_ITEM_VAL', 'новая');
define('OLD_ITEM_VAL', 'б/у');
define('NEW_ITEM_LABEL', 'Новая');
define('OLD_ITEM_LABEL', 'Б/У');

define('SELL_ITEM_VAL', 'продам');
define('RENT_ITEM_VAL', 'сдам');
define('SELL_ITEM_LABEL', 'Продам');
define('RENT_ITEM_LABEL', 'Сдам');

define('STATE_PASSIVE_ADS_VAL', 'Не активно');
define('STATE_ACTIVE_ADS_VAL', 'Активно');
define('STATE_SELLED_ADS_VAL', 'Продано');
define('STATE_PASSIVE_ADS_LABEL', 'Не активно');
define('STATE_ACTIVE_ADS_LABEL', 'Активно');
define('STATE_SELLED_ADS_LABEL', 'Продано');

define('REGION_LABEL', 'Регион');
define('CITY_LABEL', 'Город');
define('COMMENT_LABEL', 'Описание компании');

define('COST_LABEL', 'Цена');

define('YEAR_BORN_LABEL', 'Год выпуска:');
define('MODEL_LABEL', 'Модель:');
define('CATEGORY_LABEL', 'Категория:');
define('CATEGORY_SUB_LABEL', 'Подкатегория:');

define('HEADLINE_LABEL', '<strong>Название</strong>');
define('BTN_LOAD_IMAGE_VAL', 'Загрузить');
define('LOAD_IMAGE_LABEL', 'Логотип');

define('LINK_ADS_MAKE_UP', 'Поднять');
define('LINK_ADS_MAKE_TOP', 'Разместить в ТОП');
define('LINK_ADS_MAKE_SPECIAL', 'Выделить');

define('MESSAGE_WRONG_IMAGE_FORMAT', 'Загрузите изображение в формате JPG, PNG не более %s');
define('MESSAGE_INCORRECT_EMAIL', 'Неверный формат почты...');

define('DEFAULT_BRAND_VAL', 'Введите название бренда');

//search panel

define('SELECT_COUNTRY', 'Выберите страну/регион');
define('SELECT_RAION', 'Выберите район');
define('SELECT_METRO', 'Выберите станцию метро');
define('SELECT_CATEGORY', 'Выберите категорию');
define('SELECT_SUB_CATEGORY', 'Выберите подкатегорию');
define('SELECT_TYPE', 'Выберите тип');
define('SELECT_BRAND', 'Выберите бренд');
define('SELECT_COST_CATEGORY', 'Ценовая категория');
define('DEFAULT_SEARCH_WORD', 'Введите слово для поиска, магазин, бренд, товар');

//list category 

define('CLIST_MOTO_HOUR', 'наработка');
define('CLIST_MOTO_HOUR_1', 'наработка / пробег');
define('CLIST_MOTO_HOUR_TRANSPORT', 'пробег в км');
define('CLIST_MOTO_HOUR_TRANSPORT_1', 'пробег');
define('CLIST_MOTO_COST', 'цена');
define('CLIST_MOTO_COST_RENT_1', 'за час');
define('CLIST_MOTO_COST_RENT_2', 'за смену');
define('CLIST_MOTO_YEAR', 'год');
define('CLIST_MOTO_MODEL', 'модель');
define('CLIST_MOTO_PRODUCER', 'производитель');
define('CLIST_MOTO_CATEGORY', 'категория');
define('CLIST_MOTO_CITY', 'город');
define('CLIST_MOTO_PHOTO', 'фото');

//Соответствие полей базы данных и формы
// поля бд карточки компании                
$ads = array('userid' => 'user_id', 'images' => 'main_image',
        'ad_headline' => 'head', 'ad_text' => 'descript', 'published' => 'published',
        'ad_city' => 'city', 'ad_raiyon' => 'raiyon', 'ad_near_metro' => 'metro',
        'ad_adress' => 'adress', 'ad_phone' => 'phone1',
        'ad_phone1' => 'phone2', 'ad_phone2' => 'phone3', 'email' => 'email',
        'ad_work_type' => 'worktype', 'ad_map' => 'map_api', 'ad_kindof' => 'cost_category',
        'ad_site' => 'site');

define('ADS_NAMES', serialize($ads));

$profile = array('email' => 'email', 'ad_avatar' => 'ad_avatar', 'ad_city' => 'city',
        'fio' => 'fio', 'phone' => 'phone', 'descript' => 'descript', 'fact_adress' => 'adress',
        'company_name' => 'company_name', 'phone_1' => 'phone_1', 'phone_2' => 'phone_2',
        'phone_predstav' => 'phone_predstav', 'phone_predstav_1' => 'phone_predstav_1',
        'near_metro' => 'metro', 'raiyon' => 'raiyon', 'ad_site' => 'site',
        'ad_site' => 'site', 'work_type' => 'work_type', 'cost_category' => 'cost_category');

define('PROFILE_USER', serialize($profile));

$profile_company = array('company_name' => 'company_name', 'logo' => 'images',
        'short_descript' => 'short_descript', 'full_descript' => 'full_descript',
        'name' => 'name', 'phone1' => 'phone1', 'phone2' => 'phone2',
        'adress' => 'adress', 'company_phone1' => 'company_phone1',
        'company_phone2' => 'company_phone2', 'company_phone3' => 'company_phone3',
        'company_adress' => 'company_adress', 'company_site' => 'company_site',
        'email' => 'email', 'company_email' => 'company_email');

define('COMPANY_PROFILES_FIELDS', serialize($profile_company));

// Основные URL адреса
define('ITEMID_MY_ADS', 181);
define('URL_BASE', BASE);
define('URL_PROFILE', 'index.php?Itemid=136'); // профиль
define('URL_MY_CAB', 'lichnyj-kabinet.html');  // личный кабинет
define('URL_MY_ADS', 'index.php?Itemid=' . ITEMID_MY_ADS);  // мои объявления
define('URL_ADD_ADS', 'index.php?option=com_content&view=article&id=15&Itemid=178');  // добавить объявление
define('SINGLE_ADS', 'index.php?Itemid=138');  // объявление
define('URL_SEF_MY_ADS', BASE_URL . 'spisok-kartochek-tovara.html'); // мои объявления ЧПУ
//УРЛ разделов
//define(URL_SPECTECH, 'index.php?option=com_content&Itemid=138&search_category=1&id=11&lang=ru&view=article');
//При импорте новых категорий измените ИД разделов на актуальные
define('CAT_ID_SPECTECH', 1);    // ИД категории спецтехника
define('CAT_ID_KOMMTECH', 117);   // ИД категории комунальная техника
define('CAT_ID_TRASPORT', 69);   // ИД категории комунальная транспорт
define('CAT_ID_SKLADTECH', 129);  // ИД категории складская техника
define('CAT_ID_SELHOZTECH', 141); // ИД категории сельхозтехника
// Title главной страницы.
define('MAIN_PAGE_TITLE', 'Модный компас');
// Город по умолчанию. Если IP адреса нет в списке Российский городов, то подставляется город по умолчанию
define('DEFAULT_CITY', 'Новосибирск');
define('DEFAULT_CITY_ID', 2012);
// Любой IP адрес города по умолчанию
define('DEFAULT_IP', '37.193.180.76');
// Заменять локальные адреса вида 192.168... 
define('REPLACE_LOCAL_IP', true);

define('SUBJECT_MAIL_1', "Сообщение с портала fcompas.com");
define('ADMIN_EMAIL', "admin@fcompas.com");
define('FROM_NAME', "администрация портала fcompas.com");

//****Стоимость и сроки размещения объявлений
// включение системы платежей
define('PAYMENTS_ENABLE', false);

//********Тестовый период
// включение (true) / выключение (false) тестового периода
define('TEST_PERIOD_ENABLE', false);

// количество объявлений которые можно разместить бесплатно в тестовом режиме
define('TEST_PERIOD_NUM_ADS', 1);

define('PAY_COMPANY_SITE_TEXT', 'Стоимость создания сайта 1001 руб. Для создания сайта компании необходимо связаться с администрацией сайта по тел. 8-913-456-05-27 или e-mail: info@fcompas.com');
define('NULL_ADS', 'Чтобы создать сайт, создайте карточку магазина');
define('CATEGORY_NOT_FOUND', 'Если Вы не нашли категорию или тип товара, внесите отсутствующую информацию в поле "Описание компании"');

require_once 'cfg/cities.php.inc';
require_once 'cfg/dates.php.inc';
