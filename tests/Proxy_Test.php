<?php
/**
 * Created by PhpStorm.
 * User: huck
 * Date: 27.10.14
 * Time: 12:39
 */

include('f:\webservers\home\192.168.1.35\www\sub\compas\geo\class\app\app_for_phpunit.php');

class simple_class{
  private $text = 'default text';
  public function __construct(){
  }

  public function get_text(){return $this->text;}

  public function set_text($txt){$this->text = $txt;}
}

class Cache_ProxyTest extends PHPUnit_Framework_TestCase {
  protected $text = 'new text';
  protected $real_obj;
  protected $proxy_obj;

  public function SetUp(){
    // проверка корректности доступа к методам через прокси
    $this->real_obj = new simple_class();
    $this->proxy_obj = new Cache_Proxy($this->real_obj);

    // Вызываем существующий метод, устанавливливаем новый текст (не кешируется)
    $this->proxy_obj->set_text($this->text);
  }

  public function testProxyCorrectCallSetMethodNoCache()
  {

    // проверка изменения текста у реального объекта (выполнение метода реального объекта)
    $this->assertEquals($this->text, $this->real_obj->get_text());

    // метод отсутствует в кэше?
    $this->assertFalse($this->proxy_obj->last_method_is_cached());

  }

  public function testProxyCorrectCallGetMethodNoCache()
  {

    // вызов метода, который попадет в кэш, проверка того, что данные верны
    $this->assertEquals($this->proxy_obj->get_text(), $this->text);

    // нет обращения к кэшу?
    $this->assertFalse($this->proxy_obj->last_method_is_cached_now());

  }

  public function testProxyCorrectCallGetMethodCached()
  {

    // вызов метода, который попадет в кэш, проверка того, что данные верны
    $this->assertEquals($this->proxy_obj->cache(600)->get_text(), $this->text);

    // метод в кэше?
    $this->assertTrue($this->proxy_obj->last_method_is_cached_now());

  }

  /**
   * @expectedException Exception
   */
  public function testProxyNotExistingMethod()
  {

    // вызов не существующего метода
    $this->proxy_obj->cache(500)->abracadabra();

  }

  public function testProxyClearCache()
  {
    // очищаем кэш
    $this->proxy_obj->ClearCache()->get_text();

    // проверка очистки кэша
    $this->assertFalse($this->proxy_obj->last_method_is_cached());

  }

}
 