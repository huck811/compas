<?php
/**
 * Created by PhpStorm.
 * User: huck
 * Date: 27.10.14
 * Time: 12:39
 */

include('f:\webservers\home\192.168.1.35\www\sub\compas\geo\class\app\app_for_phpunit.php');

class Cache_CacheTest extends PHPUnit_Framework_TestCase {

  protected $content = 'test content!!!';
  protected $test_file = 'test.cache';

  public function testCacheFileNotExists()
  {
    // проверка чтения из кеша
    // входной пареметр только не пустая строка

    $Cobj = new Cache_Cache();

    //файл не существует
    $this->assertEquals($Cobj->get($this->test_file), null);

    //пустой файл
    $this->assertEquals($Cobj->get(''), null);

    //пробел
    $this->assertEquals($Cobj->get(' '), null);

    //массив
    $this->assertEquals($Cobj->get(array()), null);
  }

  public function testSetCache()
  {
    // проверка записи в кэш

    $Cobj = new Cache_Cache();

    //$Cobj->set($content, $filename, $ttl, $tag = '');


    // время жизни 1 сек.
    $ttl = 1;

    // теги для кэша
    $tag = array('test_tag', 'test_tag2');

    //устанавливаем кэш
    $this->assertTrue($Cobj->set($this->content, $this->test_file, $ttl, $tag));

    //данные из кеша верны?
    $this->assertEquals($Cobj->get($this->test_file), $this->content);

    //читаем метаданные
    $meta = $Cobj->get_meta($this->test_file);

    // метаинформация верная?
    $this->assertEquals($meta, array('ttl' => $ttl, 'file' => $this->test_file, 'tags' => $tag));

    // после задержки жданные уже не актуальны
    sleep(2);

    // кэш устарел?
    $this->assertEquals($Cobj->get($this->test_file), null);


  }


  public function testFileCacheExists()
  {
    $Cobj = new Cache_Cache();
    //файл существует
    $ttl = 60;
    $Cobj->set($this->content, $this->test_file, $ttl);
    $this->assertTrue($Cobj->get($this->test_file) !== null);
  }

  public function testClear(){
    // удаляем файл кэша
    $Cobj = new Cache_Cache();
    $this->assertTrue($Cobj->clear($this->test_file));

    // кэш пуст
    $this->assertEquals($Cobj->get($this->test_file), null);
  }

}
 