<?php
/**
 * @package		Joomla.Site
 * @subpackage	mod_banners
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */
// no direct access
defined('_JEXEC') or die;
?>
<div id="slider_big">
    <div id='coin-slider'>
        <a href="#">
            <img src="/templates/compas_main/img/big-slider/slide1.jpg" alt="">
        </a>
        <a href="#">
            <img src="/templates/compas_main/img/big-slider/slide2.jpg" alt="">
        </a>
    </div>
    <script src="/templates/compas_main/js/coin-slider.min.js"></script>
    <script type="text/javascript">
        jQuery('#coin-slider').coinslider({width: 1582, height: 241, navigation: true, links: true, delay: 5000, effect: 'random'});
        jQuery('#slider_big #cs-prev-coin-slider, #slider_big #cs-next-coin-slider').html('');
    </script>
</div>