<?php
/**
 * @package		Joomla.Site
 * @subpackage	mod_banners
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;
$baseurl = JURI::base();

require_once 'geo/class/app/app.php';
require_once "geo/cache.php";

$company_id = JRequest::getVar('user_id');
$user = JFactory::getUser();
$user_id = $user->id;
$user_has_company_edit = false; 
if ($company_id == $user_id){
    $user_has_company_edit = true; 
}

$ads_user = new Ads_User();
$form_c = new FormConstructor();

$uri = explode('?', $_SERVER['REQUEST_URI']); 

if (isset($_GET['action']) && in_array($_GET['action'], array('delete'))){
    if ($_GET['action'] == 'delete' && $user_has_company_edit){
        $ads_user->delete_news($_GET['newsid'], $user_id);
        header('Location: '.$uri[0]);
        exit();
    }
}

$company_data = $ads_user->get_company($company_id); 



if (!isset($_GET['addnews']) && !isset($_GET['newsid'])){
    $list_news = $ads_user->set_user($company_id)->paginate(50, false, false)->get_news_list($company_id);    
    include 'default.phtml';
}    
else if ($user_has_company_edit){
    if (isset($_GET['newsid']))
        $list_news = $ads_user->get_news_list($company_id, $_GET['newsid']);
    include 'addnews.phtml';
}
    
?>
<style type="text/css">
    .block-interview:first-child{
        display: none;
    }
    
    p{margin-bottom: 10px;}
    
    #form_company_site textarea, #form_company_site input[type="text"]{
        width: 708px;
    }
    div.system_message{
        margin-left: 15px;
    }
    
    #center_block h3{
        margin-bottom: 5px;
    }
    
    a.lk{margin: 0; width: 140px;}
    span.date{
        color:#bab6b1;
    }
    
    p.panel a{ margin-right: 20px;}
    
    .news_item:after{
        display: block;
        content: '';
        background: url("/templates/compas_main/img/img-line-dotted.png") repeat-x 0px 0px;
        height: 3px;
        width: 100%;
    }
    
    .news_item p{
        word-wrap: break-word;
    }
    
    ul.news_list li:last-child .news_item:after{
        display: none;
    }
    
    ul.news_list, ul.photo_list{
        margin: 0;
        padding: 0;
    }
    
    ul.photo_list li{
        display: inline-block;
    }
    a.del{
        margin-left: 5px;
    }
    
    div.paginate{
        text-align: center;
    }
    

</style>