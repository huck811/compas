<?php
$arr_paginate = $ads_user->arr_paginate;
if (count($arr_paginate['pages']) > 1) {
    ?>
    <?php $uri = isset($_REQUEST['ajax_url']) ? explode('?', $_REQUEST['ajax_url']) : explode('?', $_SERVER['REQUEST_URI']); ?>
    <div class="paginate">
        <ul class="navi-magasine">
            <li><a href="<?= $uri[0] ?>?page=<?= $arr_paginate['previous'] ?>" class="prev"></a></li>
            <?php foreach ($arr_paginate['pages'] as $pg) { ?>
                <li><a href="<?= $uri[0] ?>?page=<?= $pg ?>" <?php if ($arr_paginate['current'] == $pg) { ?>class="cur"<?php } ?>><?= $pg ?></a></li>   
            <?php }
            ?>
            <li><a href="<?= $uri[0] ?>?page=<?= $arr_paginate['next'] ?>" class="next"></a></li>
        </ul>
    </div>
<?php
}?>