jQuery(document).ready(function() {
    // валидация и отправка формы
    jQuery('#form_company_site').submit(function() {
        var status = jQuery('#system-message-container');
        error = '';
        jQuery('input[name="images"]').val('');
        jQuery('ul.photo_list li img').each(function(){
            file = jQuery(this).attr('file');
            jQuery('input[name="images"]').val(jQuery('input[name="images"]').val()+file+'--#--');
        });
        jQuery('#form_company_site input[type="text"], #form_company_site select, #form_company_site textarea').removeClass('red'); 
        jQuery('div.loading').fadeIn(200);
        jQuery.post("/modules/mod_show_company_news/tmpl/add_news.php?uid=" + user_id + "&hash=" + news_hash, jQuery("#form_company_site").serialize(), function(response) { // добавляем новость
            response = response.split('##');
            if (response[0] === "success") { // успех
                status.html('<div class="message">' + response[1] + '</div>');
                alert(response[1]);
                document.location.href = uri;
            }
            else { // добавить не получилось
                status.html(response[0] + response[1]);
                if (response[2] != ''){
                        var fields_error = JSON.parse(response[2]);
                        for(i=0; i < fields_error.length; i++){
                            jQuery('#form_company_site *[name="'+fields_error[i]+'"]').addClass('red');
                        }
                    }
            }
           jQuery('div.loading').fadeOut(200);
        });
        return false;
    });


});


function _delete(obj) { // Удаляем картинку по клику.    
    var li = obj.parent(); // запоминаем контейнер картинки
    li.remove();
    return false;
}