<?php
/**
 * @package		Joomla.Site
 * @subpackage	mod_login
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;
//JHtml::_('behavior.keepalive');
require_once 'geo/class/app/app.php';

$user =& JFactory::getUser();
$id = $user->get('id');

//$usr = new Ads_User();
//$u_data = $usr->get_user_data($id);

?>
<?php if ($type == 'logout') : ?>
<form action="<?php echo JRoute::_('index.php', true, $params->get('usesecure')); ?>" method="post" id="login-form">
<?php if ($params->get('greeting')) : ?>
	<div class="login-greeting">
	<?php if($params->get('name') == 0) : {
		//echo JText::sprintf('MOD_LOGIN_HINAME', htmlspecialchars($user->get('name')));
	} else : {
		//echo JText::sprintf('MOD_LOGIN_HINAME', htmlspecialchars($user->get('username')));
	} endif; ?>
	</div>
<?php endif; ?>
	<div class="logout-button" >
	
        <input type="submit" name="Submit" class="button style-button"  value="<?php echo JText::_('JLOGOUT'); ?>" />
		<input type="hidden" name="option" value="com_users" />
		<input type="hidden" name="task" value="user.logout"  />
		<input type="hidden" name="return" value="<?php echo $return; ?>" />
		<?php echo JHtml::_('form.token'); ?>
        
	</div>
</form>
<?php else : ?>
<form action="<?php echo JRoute::_('index.php', true, $params->get('usesecure')); ?>" method="post" id="login-form" >
	<?php if ($params->get('pretext')): ?>
		<div class="pretext">
		<p><?php echo $params->get('pretext'); ?></p>
		</div>
	<?php endif; ?>
    <input id="modlgn-username" type="text" value="логин" name="username" class="inputbox"  size="18" onblur="if (this.value=='') this.value='логин';" onfocus="if (this.value=='логин') this.value='';" />

	<input id="modlgn-passwd" type="password" name="password" class="inputbox" size="18"  />

	<?php if (JPluginHelper::isEnabled('system', 'remember')) : ?>
	<input id="modlgn-remember" type="checkbox" name="remember" style="display: none;" class="inputbox" checked="checked" value="yes"/>

	<?php endif; ?>
	<input type="submit" name="Submit" class="button" value="Вход" />
	<input type="hidden" name="option" value="com_users" />
	<input type="hidden" name="task" value="user.login" />
	<input type="hidden" name="return" value="<?php echo $return; ?>" />
	<?php echo JHtml::_('form.token'); ?>
    <span class="reg">
        <a href="<?php echo JRoute::_('index.php?option=com_users&view=registration&Itemid=135'); ?>"><?php echo JText::_('MOD_LOGIN_REGISTER'); ?></a> /
        <a href="<?php echo JRoute::_('index.php?option=com_users&view=reset&Itemid=137'); ?>">
			<?php echo JText::_('MOD_LOGIN_FORGOT_YOUR_PASSWORD'); ?></a>
    </span>
                
	<?php if ($params->get('posttext')): ?>
		<div class="posttext">
		<p><?php echo $params->get('posttext'); ?></p>
		</div>
	<?php endif; ?>
</form>
<?php endif; ?>
