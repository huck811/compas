<?php
/**
 * @package		Joomla.Site
 * @subpackage	mod_menu
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access.
defined('_JEXEC') or die;
$user = & JFactory::getUser();
$user_id = $user->get('id');

$session = JFactory::getSession();
$session->set('user', new JUser($user_id));
$user = & JFactory::getUser();

require_once 'geo/class/app/app.php';

$sef = new MySef();
$ads_user = new Ads_User();
$ads_manage = new Ads_AdsManage();

// go to profile if it's empty 
include 'templates/compas_main/redirect_to_profile.php';
 
//$ads_user->get_user_data($user_id);
$ads_manage->set_user($user_id);
$count_ads = $ads_manage->get_list_ads(1);

$company_url = $sef->set_company($user_id)->get_sef_url();
//var_dump($company_url);
?>
<ul class="menu center" style="margin-top: -16px; margin-bottom: 26px;">
    <?php if ($user->company_site_enable && $company_url) { ?>
        <li><a href="<?= $company_url ?>" class="company_site">Новости компании</a></li>
    <?php } ?>

     <li><a href="index.php?Itemid=197" class="company_data" onclick="return valid_ads_num();"><?php if($company_url){?>Данные компании<?php } else {?>Создать сайт компании<?php }?></a></li>
</ul>

<script>
    if (!FIRSTENTRY) {
        var FIRSTENTRY = true;
<?php if (!$user->company_site_enable) { ?>


<?php } else { ?>

            if (jQuery('ul.menu.center a.company_data').length && jQuery('table.lk a.company_data').length) {

                jQuery('table.lk a.company_data').attr('href', jQuery('ul.menu.center a.company_data').attr('href'));
                jQuery('table.lk a.company_site').attr('href', jQuery('ul.menu.center a.company_site').attr('href'));
            }
<?php } ?>
<?php if (!$company_url) { ?>
            jQuery('table.lk a.company_site').parent().parent().parent().hide();
<?php } ?>


        
        if (jQuery("table.lk a.company_data").length && jQuery("ul.menu.center a.company_data").length)
            jQuery("table.lk a.company_data").html(jQuery("ul.menu.center a.company_data").html())


      jQuery("table.lk a.company_data").click(function() {
        return valid_ads_num();
      });

      function valid_ads_num(){
        if (!<?= (int) $count_ads ?>)
          alert('<?= NULL_ADS ?>');
        else if (!<?= (int) $user->company_site_enable ?>)
          alert('<?= PAY_COMPANY_SITE_TEXT ?>');
        else
          return true;
        return false;
      }
    }
</script>
