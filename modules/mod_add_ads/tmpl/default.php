<?php
/**
 * @package		Joomla.Site
 * @subpackage	mod_banners
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;
$baseurl = JURI::base();

require_once 'geo/class/app/app.php';
require_once "geo/cache.php";

$user =& JFactory::getUser();
$user_id = $user->get('id');

$isAdmin = $user->get('isRoot');

$ads_user = new ads_user();
//$ads_user->get_user_data($user_id);

// go to profile if it's empty 
include 'templates/compas_main/redirect_to_profile.php'; 

    
    
    $form_c = new FormConstructor();
    $city = new Cities();
    $category = new Ads_Category();
    $ads = new Ads_AdsManage();
    $ads_id = htmlspecialchars(Utils_Helpers::GetVar('ads'));



    if ($isAdmin)
        $ads->get_ads(false, $ads_id);
    else    
        $ads->get_ads($user_id, $ads_id);
        
    $cc = array();  
    foreach ($ads->category as $cat){
        $level2 = $category->get_category_full_info($cat); $cc[]=$level2['id'];
        $level1 = $category->get_category_full_info($level2['parent']); $cc[]=$level1['id'];
        $level0 = $category->get_category_full_info($level1['parent']); $cc[]=$level0['id'];
    }
    $ads->category = $cc;    
    
    $arr_category = $category->get_ar_category(0, true, 2);
    $list_razdel = '<div class="block_in superstyle block3"><div class="conttt cont">Тип <span class="redtext">*</span>: </div>';
    $list_category = '<div class="block_in superstyle block4 block3"><div class="conttt cont">Категория <span class="redtext">*</span>: </div>';
    $list_subcategory = '<br/>Выберите подкатегории <span class="redtext">* (Выберите хотя бы одну подкатегорию в каждой активной категории, либо отмените выделение категории выше)</span>: <br/><br/><div class="block_in superstyle">';
    $i=1;
    
    $cat_array = array();
    $sort_razdel = array();
    foreach ($arr_category as $r){
        $checked = in_array($r['id'],$ads->category) ? 'checked="checked"' : '';
        
        if ($r['level']==0){
                $list_razdel .= ' <div class="cont"><input '.$checked.' type="checkbox" value="'.$r['id'].'" class="superstyle" tree_id="'.$r['id_tree'].'" level="0" /> <label>'.$r['name'].'</label></div>';
                $sort_razdel[] = $r['id'];
                $name_razdel[$r['id']] = $r['name'];
        }else if($r['level']==1){
            
            
            $cat_array[$r['id_tree']][] = array('id' => $r['id'], 'checked' => $checked, 'name' => $r['name']); 
            
            $name = $r['name'];
            $arr_subcategory = $category->get_arr_category_by_tree_id($r['parent'], $r['id']);
            if (count($arr_subcategory)){
                $list_subcategory .='<div class="block_in  block5 id'.$r['id'].' hide" tree_id="'.$r['id_tree'].'" level="2" ><span class="title"><span style="text-transform:uppercase;">'.$name_razdel[$r['id_tree']].'</span> '.$name.'</span><br/>';
                foreach($arr_subcategory as $sc){
                    $checked = in_array($sc['id'],$ads->category) ? 'checked="checked"' : '';
                    $list_subcategory .=' <div class="subcont subcont'.$sc['id'].' parent'.$r['id'].'" ><input level="2" '.$checked.' type="checkbox" id="num'.$sc['id'].'" value="'.$sc['id'].'" class="superstyle" tree_id="'.$sc['id_tree'].'" parent="'.$sc['parent'].'"/> <label>'.$sc['name'].'</label></div>';;
                }
                $list_subcategory .='</div>';
                if ($i%2 == 0)
                    $list_subcategory .='<br style="clear:both;"/><hr style="clear:both; height:1px; display:block; border:0; border-bottom:1px dashed; margin:10px 0;"/>';
  
                $i++;
            }

        }
    }
    
    foreach ($sort_razdel as $tree_id){
        $list_category .='<div class="category">';
        foreach ($cat_array[$tree_id] as $item){
            $list_category .=  ' <div class="cont cont'.$item['id'].' hide"><input '.$item['checked'].' type="checkbox" level="1" id="num'.$item['id'].'" value="'.$item['id'].'" class="superstyle" parent="'.$tree_id.'" tree_id="'.$tree_id.'"/> <label>'.$item['name'].'</label></div>';
        }
        
        $list_category .='</div>';
    }
    
    
    $list_razdel .= '</div>';
    $list_category .= '</div><br style="clear:both;"/>';
    $list_subcategory .= '</div><br style="clear:both;"/>';

    ?>
    <script type="text/javascript">
        <?php include "modules/mod_add_ads/tmpl/script.js";?>
    </script>
    <?php   
    
    $form_c->set_validate_object(new Validate_AdsForm);
    
    $head = $form_c
                 ->type()
                 ->label(HEADLINE_LABEL)
                 ->name('head')
                 ->init($ads_id ? $ads->head : $ads_user->company_name)
                 ->attr('maxlength="35"')
                 ->insert();
                 
    $btn_load = $form_c
                     ->name('load')
                     ->type('INPUT','button')
                     ->init(BTN_LOAD_IMAGE_VAL)
                     ->_class('upload')
                     ->label(LOAD_IMAGE_LABEL)
                     ->id('upload')
                     ->insert();                     
      
                     
    $comment_field = $form_c
                       ->type('TEXTAREA')
                       ->_class('comment')
                       ->init($ads_id ? $ads->descript : $ads_user->descript)
                       ->label(COMMENT_LABEL)
                       ->name('descript')
                       ->insert();  
                 
    $city_item = $ads->city ? $ads->city : (!$ads_id ? $ads_user->city : $_COOKIE['compas_city']);

    $city_field = $city
                     ->set_form($form_c)
                     ->set_city($city_item)
                     ->get_cities_by_country('', CITY_LABEL, "city", "", array(DEFAULT_SELECT_VAL),'','','Выберите город...'); 
              

    $rayon_field = '<option>'.SELECT_RAION.'</option>'
            .$ads->get_rayon_and_okrug($ads_user->city, true)->get_options('id', 'name', false, !$ads_id ? $ads_user->raiyon  : $ads->raiyon);

    $rayon_field = html::wrapp(
            html::requried_label(
                    html::wrapp_select($rayon_field , 'raiyon', 'raiyon')
                    , 'Район'
            )
    );

    $metro_field = '<option>'.SELECT_METRO.'</option>'
            .$ads->get_list_metro_and_line($ads_user->city, true)->get_options('id', 'name', false, $ads_id ? $ads->metro : $ads_user->metro);

    $metro_field = html::wrapp(
            html::label(
                    html::wrapp_select($metro_field , 'metro', 'metro')
                    , 'Метро'
            )
    );

    $email = $form_c
                 ->type()
                 ->label('E-mail')
                 ->name('email')
                 ->init($ads_id ? $ads->email : $ads_user->email)
                 ->attr('maxlength="50"')
                 ->insert(); 
                 
    $regim = $form_c
                 ->type()
                 ->label('Режим работы')
                 ->name('worktype')
                 ->init($ads_id ? $ads->worktype : $ads_user->work_type)
                 ->attr('maxlength="50"')
                 ->insert(); 
    $site = $form_c
                 ->type()
                 ->label('Сайт')
                 ->name('site')
                 ->init($ads_id ? $ads->site : $ads_user->site)
                 ->attr('maxlength="50"')
                 ->insert();    
    $adress = $form_c
                 ->type()
                 ->label('Адрес')
                 ->name('adress')
                 ->valid('Заполните адрес...')
                 ->requaried()
                 ->init($ads_id ? $ads->adress : $ads_user->adress)
                 ->insert(); 
    $phone1 = $form_c
                 ->type()
                 ->label('Телефон1')
                 ->name('phone1')
                 ->init($ads_id ? $ads->phone1 : $ads_user->phone)
                 ->insert(); 
    $phone2 = $form_c
                 ->type()
                 ->label('Телефон2')
                 ->name('phone2')
                 ->init($ads_id ? $ads->phone2 : $ads_user->phone_1)
                 ->insert();
    $phone3 = $form_c
                 ->type()
                 ->label('Телефон3')
                 ->name('phone3')
                 ->init($ads_id ? $ads->phone3 : $ads_user->phone_2)
                 ->insert();  
                 
    $cost_category = $city
                     ->set_cost_category($ads_id ? $ads->cost_category : $ads_user->cost_category)
                     ->get_cost_category('cost_category', 'Ценовая категория', 'cost_cat', 'cost_category', array(DEFAULT_SELECT_VAL));                                                                                    
                                                      
     
    $map = $form_c
                 ->type('TEXTAREA')
                 ->_class('map_api')
                 ->label('API карты')
                 ->name('map_api')
                 ->init($ads->map_api)
                 ->nowrapp()
                 ->insert(); 
    $map = '<div class="block_in map_api"><table><tr><td>'.$map.'</td><td valign="top">
    <div style="margin-left:10px"><span class="redtext">Вы можете загрузить скрипт карты используя API:</span>
    <br/><a href="http://api.yandex.ru/maps/tools/constructor/" target="_blank">Яндекс карты</a>
    <a href="http://firmsonmap.api.2gis.ru/" target="_blank">2ГИС Карты</a>
    <a href="https://maps.google.ru/maps?hl=ru&tab=wl" target="_blank">Google Maps</a>
    <br/><br/>Значение ширины <b>width</b> поставьте равным 970 (px)</div></td></tr></table></div>';                         
    
    $submit_btn = $form_c
                     ->clr()
                     ->name('go')
                     ->type('INPUT','submit')
                     ->init('Сохранить')
                     ->label()->nowrapp()
                     ->insert(); 
                      
    $delete_btn = $form_c
                     ->clr()
                     ->name('delete')
                     ->type('INPUT','reset')
                     ->init('Удалить')
                     ->label()->nowrapp()
                     ->insert();
    if ($ads_id)
        $list_brands = $ads->brands; 
    else
        $list_brands = $ads_user->brands; 
    
    $brand = '';
    foreach ($list_brands as $br){
        $brand .= '<div class="item">';
        $brand .= $form_c
                         ->type()
                         ->label('Бренд')
                         ->name('brand')
                         ->init($br['name'])
                         ->insert();
        $brand .= '<input type="button" value="Добавить" class="add" />';
        $brand .= '<input type="button" value="Удалить" class="delete" /></div>';
    }
    $brand .= '<div class="item" '.((count($list_brands) >= MAX_BRAND_NUM) ? "style='display:none;'" : "").'>';
    $brand .=        $form_c
                     ->type()
                     ->label('Бренд')
                     ->name('brand')
                     ->init(DEFAULT_BRAND_VAL)
                     ->insert();

    $brand .= '<input type="button" value="Добавить" class="add" />';
    $brand .= '<input type="button" value="Удалить" class="delete" /></div>';



    Utils_Helpers::var_to_js(array(
            'user_id' => $user_id,
            'hash' => md5($user_id),
            'max_photo_items' => MAX_PHOTO_ITEMS
    ));
?>
<script src="/geo/js/upload_file.js"></script>
<?php
    include 'default.phtml';
?>