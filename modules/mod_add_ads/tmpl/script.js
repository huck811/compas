    // использование Math.round() даст неравномерное распределение!
    function getRandomInt(min, max)
    {
      return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    function num_photo(){
        return jQuery('#files li').length - jQuery('#files li.no_photo').length;
    }
    
    
    function _delete(obj){ // Удаляем картинку по клику.
      var status = jQuery('div.system_message > div');
      var li = obj.parent(); // запоминаем контейнер картинки
      src = li.find('img:first').attr('src'); // получаем путь к файлу
      var first_li_small = jQuery('#files li.small img:first');

      jQuery.get("/modules/mod_add_ads/upload/delete-file.php?img="+src+'&rand='+getRandomInt(1,10000), function(response){ // запрос на удаление
          response = response.split('##');
          if(response[0]==="success"){ // успех
                li.html(''); // очищаем содержимое контейнера с фото
                li.addClass('no_photo');  
                restruct_img();    
          }
          else{ // удалить не получилось
            status.html('<div class="inner">'+response[0]+response[1]+'</div>');
          }
          
      });
       
      return false;
    }
    
    function add_img_to_list(folder, prefix,file, _new){
          
        if (num_photo()==<?php echo MAX_PHOTO_ITEMS;?>)
            return false;
         
        if (jQuery('#files li.main').html()==''){
            jQuery('#files li.main').html('<img src="'+_new+'" alt="" file="'+folder+'/'+file+'" /><a href="#" class="del" onClick="_delete(jQuery(this)); return false;">Удалить</a>');
            jQuery('#files li.main').removeClass('no_photo');
        }    
        else{
            var first = false;
            jQuery('#files li.small').each(function(){
                if (jQuery(this).html()=='' && !first){
                    jQuery(this).html('<img src="'+_new+'" alt="" file="'+folder+'/'+file+'" /><a href="#" class="del" onClick="_delete(jQuery(this)); return false;">Удалить</a>');
                    jQuery(this).removeClass('no_photo');
                    first = true;
                }    
            });
        } 
        return true;   
    }
    
    jQuery(document).ready(function(){
        
        jQuery("select[name=region]").change(function(){ // при смене региона подгружаем список городов этого региона
            var status = jQuery('div.system_message > div'); 
            if (jQuery(this).val()=='Выбрать'){
                jQuery("select[name=city]").html('<option>Выбрать</option>');
            }
            else{
               
              jQuery.get("/modules/mod_add_ads/upload/get_list_city.php?region="+encodeURIComponent(jQuery(this).val()), function(response){ // запрос на список городов по региону
                  response = response.split('##');
                  if(response[0]==="success"){ // успех
                     jQuery("select[name=city]").html(response[1]);
                  }
                  else{ // удалить не получилось
                    status.html('<div class="inner">'+response[0]+response[1]+'</div>');
                  }
                  
              });
            }    
        });
        
                   
        // валидация и отправка формы
        jQuery('#form_add_ads').submit(function (){
            // компонуем типы ценовых категорий
            var cost_cat  = [];
            jQuery('input[name="cost_category"]').val('');
            jQuery('.block3 input[type="checkbox"].cost_cat').each(function(){
                if (jQuery(this).prop('checked'))
                    cost_cat[cost_cat.length] = jQuery(this).val();
                
                if (cost_cat.length){                       
                   jQuery('input[name="cost_category"]').val(cost_cat);
                }   
            });
            
            //компонуем категории
            var categories  = [];
            jQuery('input[name="list_category"]').val('');
            jQuery('input[type="checkbox"][level]').each(function(){
                if (jQuery(this).prop('checked'))
                    categories[categories.length] = jQuery(this).val();
                
            });
            
            if (categories.length) {   
                   jQuery('input[name="list_category"]').val(categories);
            } 
            
            //компонуем бренды
            var brands  = [];
            jQuery('input[name="brands"]').val('');
            jQuery('.all_brands input[type="text"]').each(function(){
                if (jQuery(this).val()!=jQuery('#default_brand_val').val()){
                    str = jQuery(this).val();
                    brands[brands.length] = str.replace(/,/g,'#');
                }    
    
            });

             
            if (brands.length) {  
                   jQuery('input[name="brands"]').val(brands);
            } 
            //alert(jQuery('input[name="brands"]').val()); false;
    
            
            var status = jQuery('div.system_message > div');
            jQuery('.form_add_ads input, .form_add_ads select').removeClass('red');
            error = '';
 
            if(!jQuery('input[level="0"]:checked').length)
                    error +='Выберите хотя бы один тип...';       
                    
            jQuery('input[level="0"]:checked').each(function(){
                tree_id = jQuery(this).attr('tree_id');
                if(!jQuery('input[tree_id="'+tree_id+'"][level="1"]:checked').length)
                    error +='Выберите хотя бы одну категорию...';       
            });
            
            jQuery('input[level="1"]:checked').each(function(){
                _val = jQuery(this).val();
                //alert(jQuery('input[parent="'+_val+'"][level="2"]').length);
                if(jQuery('input[parent="'+_val+'"][level="2"]').length && !jQuery('input[parent="'+_val+'"][level="2"]:checked').length)
                    error +='Выберите хотя бы одну подкатегорию...';       
            });

            
            /*jQuery('input[name=images]').val('');
            jQuery('#files li img').each(function(){
                jQuery('input[name=images]').val(jQuery('input[name=images]').val()+jQuery(this).attr('file')+'##');
            }); */

            if (jQuery('.img_cont li.main img').length)
                jQuery('input[name="images"]').val(jQuery('.img_cont li.main img').attr('file'));
            else
                jQuery('input[name="images"]').val('');
               
            if (error!=''){
                status.html('<div class="inner error">'+error+'</div>');
            }
            else{
                jQuery('input[name="head"], select[name="category"], select[name="subcategory"], select[name="producer"], #rent_radio, #sell_radio').removeAttr('disabled');
                jQuery('div.loading').fadeIn(200);
                jQuery.post("/modules/mod_add_ads/upload/add_ads.php", jQuery("#form_add_ads").serialize(),function(response){ // добавляем объявление
                  response = response.split('##');
                  //jQuery('input[name="head"], select[name="category"], select[name="subcategory"], select[name="producer"], #rent_radio, #sell_radio').attr('disabled','disabled');
                  jQuery('#form_add_ads input[type="text"], #form_add_ads select, #form_add_ads textarea, div.cost_category').removeClass('red');

                  if(response[0]==="success"){ // успех
                     status.html('<div class="message">'+response[1]+'</div>');
                     
                     href = '<?=JRoute::_(URL_MY_ADS)?><?php echo isset($_GET['page']) ? '?page='.$_GET['page'] : ''?>';
                     if ((href.indexOf('?')+1)){
                        href += '<?php echo isset($_GET['rent']) ? '&rent' : ''?>';
                     }
                     else
                        href += '<?php echo isset($_GET['rent']) ? '?rent' : ''?>';
                     document.location.href = href;
                  }
                  else{ // добавить не получилось
                    status.html('<div class="inner error">'+response[0]+response[1]+'</div>');
                    if (response[2] != '' && response[0]==="error"){
                        var fields_error = JSON.parse(response[2]);
                        for(i=0; i < fields_error.length; i++){
                            jQuery('#form_add_ads *[name="'+fields_error[i]+'"]').addClass('red');
                            if (fields_error[i] == 'cost_category'){
                                jQuery('div.cost_category').addClass('red');
                            }
                        }
                    }
                  }

                  jQuery('div.loading').fadeOut(200);
                });
                
            }    
            return false;

        });
        
        jQuery("select[name='city']").change(function(){ // при смене города подгружаем районы
            if (jQuery(this).val()=='<?=DEFAULT_SELECT_VAL?>'){
                jQuery("select[name=raiyon]").html('<option><?=SELECT_RAION?></option>');                
            }
            else{
              set_rayon(jQuery(this).val());
              
            } 
            jQuery("select[name=metro]").html('<option><?=SELECT_METRO?></option>');
            set_metro(jQuery(this).val());
              
        });
        
        jQuery("select[name='raiyon']").change(function(){ // при смене района подгружаем станции метро
            if (jQuery(this).val()=='<?=SELECT_RAION?>'){
                jQuery("select[name=metro]").html('<option><?=SELECT_METRO?></option>');
            } 
              
        });
        
    });
    
    function set_rayon(city){

            jQuery.get("/modules/mod_add_ads/upload/get_list_rayon.php?optgroup=1&city="+city, function(response){ // запрос на список районов по городу
                  response = response.split('##');
                  if(response[0]==="success"){ // успех
                     //
                     jQuery("select[name='raiyon']").html('');
                     //alert(city);
                     jQuery("select[name='raiyon']").html(response[1])
                     jQuery("select[name='metro'] option").removeAttr('selected');
                     jQuery("select[name='s_raion']").trigger('refresh');
                  }
                   
              });
    
              
        }
        
        function set_metro(city){
            jQuery.get("/modules/mod_add_ads/upload/get_list_metro.php?optgroup=1&city="+city, function(response){ // запрос на список районов по городу
                  response = response.split('##');
                  //alert(response);
                  if(response[0]==="success"){ // успех
                     //
                     jQuery("select[name='metro']").html('');
                     jQuery("select[name='metro']").html(response[1]);
                  }
                  
              });
    
              
        }
        
        
        
        jQuery(document).ready(function(){            


            jQuery('select.header-select').change(function(){
                    set_rayon(jQuery.cookie('compas_city'));
                    set_metro(jQuery.cookie('compas_city'));
                });
                
       
                
         });