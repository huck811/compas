<?php
require_once '../../../geo/class/app/app.php';
$p = new PostData();
$ads_manage = new Ads_AdsManage();
if ($p->is_post()){
    $valid = new Validate_AdsForm();
    if ($valid->run()){
        if($ads_manage->is_ads_exsist(false,'',$p->ads_id)){
            $ads_manage->update_ads($p); // обновляем объявление   

            exit('success##объявление №'.$p->ads_id.' Обновлено.'); 
        }     

        else if($id = $ads_manage->insert_ads($p)) // добавляем объявление в базу
            exit('success##объявление №'.$id.' добавлено.'); 
        else      
            exit('error##Ошибка: '.implode(' / ',$ads_manage->error));  
    }
    else{
        exit('##'.$valid->get_string_errors().'##'.$valid->get_errf_to_json());
    }
}
exit('error##данные не переданы');
?>