<?php
require_once '../../../geo/class/app/app.php';

$category = new ads_category();
$ads_manage = new Ads_AdsManage();

$get_category = isset($_GET['get_category']) ? 1 : 0;
$def = Utils_Helpers::GetVar('default_val', SELECT_SUB_CATEGORY);
if (!empty($_GET['category'])){
    $arr = $category->get_list_by_parent_id(Utils_Helpers::GetVar('category'));
    if($get_category)
        $def = '<option>'.SELECT_CATEGORY.'</option>';
    else
        $def = '<option>'.SELECT_SUB_CATEGORY.'</option>';    
    exit('success##'.$def.$category->get_options('id', 'name', $arr));    
}
exit('error##нужно передать параметр');
?>