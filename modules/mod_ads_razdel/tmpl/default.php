<?php
/**
 * @package		Joomla.Site
 * @subpackage	mod_banners
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;
require_once 'geo/class/app/app.php';
$baseurl = JURI::base();
$ads_id = isset($_GET['ads_id']) ? $_GET['ads_id'] : 0;
$cat_id = isset($_GET['search_category']) ? $_GET['search_category'] : 0;

if (!$cat_id && $ads_id){
    $ads_manage = new Ads_AdsManage();
    $ads_manage->get_ads(false, $ads_id);
    $cat_id = $ads_manage->category;
}

$category = new ads_category();
$info = $category->get_arr_category_tree_cat_id($cat_id);
$cat_id = $info[0]['id_tree'];
$sef = new MySef();
//$sef->set_category(CAT_ID_SPECTECH)->get_sef_url()
//$sef->set_new_url();
?>
<div style="margin-top: 3px;">
	<ul class="razdel">
	
			
				<li><a href="/"><img src="/templates/compas_main/img/all-tech<?php echo ($cat_id == 0) ? "-active" : ""?>.png" width="139" /></a>
				</li>
                <li><a href="<?php echo JRoute::_('index.php?option=com_adsmanager&Itemid=138&search_category='.CAT_ID_SPECTECH.'&view=list');?>"><img src="/templates/compas_main/img/spec-tech<?php echo ($cat_id == CAT_ID_SPECTECH) ? "-active" : ""?>.png" width="155"/></a>
				</li>
				<li><a href="<?php echo JRoute::_('index.php?option=com_adsmanager&Itemid=138&search_category='.CAT_ID_TRASPORT.'&view=list');?>"><img src="/templates/compas_main/img/transport-tech<?php echo ($cat_id == CAT_ID_TRASPORT) ? "-active" : ""?>.png" width="134"/></a>
				</li>
				<li><a href="<?php echo JRoute::_('index.php?option=com_adsmanager&Itemid=138&search_category='.CAT_ID_SELHOZTECH.'&view=list');?>"><img src="/templates/compas_main/img/selhoz-tech<?php echo ($cat_id == CAT_ID_SELHOZTECH) ? "-active" : ""?>.png" width="164"/></a>
				</li>
				<li><a href="<?php echo JRoute::_('index.php?option=com_adsmanager&Itemid=138&search_category='.CAT_ID_KOMMTECH.'&view=list');?>"><img src="/templates/compas_main/img/kommun-tech<?php echo ($cat_id == CAT_ID_KOMMTECH) ? "-active" : ""?>.png" width="184"/></a>
				</li>
				<li><a href="<?php echo JRoute::_('index.php?option=com_adsmanager&Itemid=138&search_category='.CAT_ID_SKLADTECH.'&view=list');?>"><img src="/templates/compas_main/img/sklad-tech<?php echo ($cat_id == CAT_ID_SKLADTECH) ? "-active" : ""?>.png" width="190"/></a>
				</li>
                <li class="justify-helper"></li>

	</ul>
</div>