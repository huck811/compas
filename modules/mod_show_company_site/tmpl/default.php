<?php
/**
 * @package		Joomla.Site
 * @subpackage	mod_banners
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;
$baseurl = JURI::base();

require_once 'geo/class/app/app.php';
require_once "geo/cache.php";

$user =& JFactory::getUser();
$user_id = $user->get('id');

$session = JFactory::getSession();
$session->set('user', new JUser($user_id));
$user =& JFactory::getUser();

$user->company_site_enable;

$ads_user = new Ads_User();
$ads_manage = new Ads_AdsManage();
$form_c = new FormConstructor();
$company_id = JRequest::getVar('user_id');

$num_ads = $ads_manage->set_user($company_id)->get_list_ads(1);

$company_data = $ads_user->get_company($company_id);  


$user_has_company_edit = false; 

if ($company_id == $user_id){
    $user_has_company_edit = true; 
}

if ($company_data['company_site']){
    $company_data['company_site'] = str_replace(array('http://', 'www.'),'',$company_data['company_site']);
    $company_data['company_site_show'] = 'www.'.$company_data['company_site'];
    $company_data['company_site'] = 'http://'.$company_data['company_site'];
}

include 'default.phtml';