<?php
/**
 * @package		Joomla.Site
 * @subpackage	mod_banners
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;
$baseurl = JURI::base();

require_once 'geo/class/app/app.php';
include_once "geo/cache.php";

$user =& JFactory::getUser();
$user_id = $user->get('id');
$ads_id = htmlspecialchars($_GET['ads']);

$category = new ads_category();
$sef = new MySef();
$list_cat = $category->get_ar_category(0, true, 0);
$id_razdel_left = array(CAT_ID_SPECTECH);
$id_razdel_right = array(CAT_ID_SELHOZTECH,CAT_ID_KOMMTECH);

$left_colon = '';
$right_colon = '';
$middle_colon = '';

if (!$dhtml = readCache(CACHE_CATEGORY_LIST, CACHE_TIME)) { // кэшируем вывод
   
    $dhtml = '<div class="main_cont">';
     
    foreach ($list_cat as $lc){
        $list_sub_cat = $category->get_ar_category($lc['id'], false, 0);
        $colon = '';
        $colon .= '<div class="razdel" id="razdel_'.$lc['id'].'" style="float:left;">
            <a class="main" href="'. JRoute::_('index.php?option=com_adsmanager&Itemid=138&search_category='.$lc['id'].'&view=list') .'">'.$lc['name'].'</a>';
            
            foreach ($list_sub_cat as  $lsc){
                $list_sub_sub_cat = $category->get_ar_category($lsc['id'], false, 0);
                $colon .= '<br/><a class="sub" href="'. JRoute::_('index.php?option=com_adsmanager&Itemid=138&search_category='.$lsc['id'].'&view=list') .'">'.$lsc['name'].'</a>';
                foreach ($list_sub_sub_cat as $lssc){
                    $colon .= '<br/><a class="sub_1" href="'. JRoute::_('index.php?option=com_adsmanager&Itemid=138&search_category='.$lssc['id'].'&view=list') .'">'.$lssc['name'].'</a>';
                }
            }
            
        $colon .= '</div>';
        
        if (in_array($lc['id'], $id_razdel_left))
            $left_colon .= $colon;
        else if(in_array($lc['id'], $id_razdel_right))  
            $right_colon .= $colon;
        else
            $middle_colon .= $colon;     
    }
    

    $dhtml .= '<table class="category_list" width="100%"><tr><td align="left" valign="top" width="310">'.$left_colon.'</td>
    <td align="center" valign="top">'.$middle_colon.'</td>
    <td align="right" valign="top">'.$right_colon.'</td>'; 
    $dhtml .= '</tr></table>';
    
    writeCache($dhtml,CACHE_CATEGORY_LIST);
    
  
}

echo $dhtml;


//echo $left_colon;
?>
<style type="text/css">
div.razdel{margin-bottom: 20px; width: 250px; text-align: left; }
div.razdel a{font-size: 14px; text-transform: uppercase; font-weight: bold; padding-bottom: 3px; display: inline-block;}
div.razdel a.sub{font-size: 11px; color:#ffcc00;}
div.razdel a.main{padding-bottom: 20px;}
div.razdel a.sub_1{font-size: 11px; color:#595959; margin-left: 14px;}
.main_cont{margin-left: 24px;}
.colon{ margin-right: 10px;}
#razdel_110{ width: 250px;}
#razdel_<?=CAT_ID_SPECTECH?> a.main{ background: url(templates/compas_main/img/cat-list-spectech-icon.png) no-repeat left 0px; padding-left: 40px;}
#razdel_<?=CAT_ID_KOMMTECH?> a.main{ background: url(templates/compas_main/img/cat-list-comuntech-icon.png) no-repeat left 2px; padding-left: 43px;}
#razdel_<?=CAT_ID_SELHOZTECH?> a.main{ background: url(templates/compas_main/img/cat-list-selhoz-icon.png) no-repeat left 0px; padding-left: 25px;}
#razdel_<?=CAT_ID_TRASPORT?> a.main{ background: url(templates/compas_main/img/cat-list-transport-icon.png) no-repeat left 0px; padding-left: 35px;}
#razdel_<?=CAT_ID_SKLADTECH?> a.main{ background: url(templates/compas_main/img/cat-list-sklad-icon.png) no-repeat left 0px; padding-left: 36px;}
</style>