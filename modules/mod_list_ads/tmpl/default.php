<?php
/**
 * @package		Joomla.Site
 * @subpackage	mod_banners
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;
$baseurl = JURI::base();

require_once 'geo/class/app/app.php';
require_once "geo/cache.php";

$user =& JFactory::getUser();
$user_id = $user->get('id');
if ($user_id){ //huck  
    // go to profile if it's empty 
    include 'templates/compas_main/redirect_to_profile.php';     
}
$isAdmin = $user->get('isRoot');

// Устанавливаем права пользователя
Ads_User::set_permission(JFactory::getUser());

$form_c = new FormConstructor();
$city = new Cities();
$category = new Ads_Category();
$ads_manage = new Ads_AdsManage();
$sef = new MySef();

JPluginHelper::importPlugin( 'content', 'vrvote' ); 
$dispatcher =& JDispatcher::getInstance();

$cost_category = unserialize(COST_CATEGORY);

$images_cc = unserialize(COST_CATEGORY_IMG);

$arr_action = array('del', 'select','top', 'untop', 'up', 'unpublish', 'publish');

$action = Utils_Helpers::GetVar('action');


$ads_id = htmlspecialchars(Utils_Helpers::GetVar('ads'));

$type = Utils_Helpers::GetVar('rent', '');

 

$user =& JFactory::getUser();
$user_id = $user->get('id');

if (isset($_GET['page']))
    $page = 'page='.$_GET['page'];
else         
    $page = '';
if (in_array($action, $arr_action) && $user_id && $ads_id){
    include 'action/ads_'.$action.'.php';
    $p = new PostData();
    unset($p->action);
    unset($p->ads);
    if ($page != '')
        $page = '?'.$page;
    header('Location: '.$p->get_url().$page);
    exit();
    
}

if ($page != '')
    $page = '&'.$page;
   

if ($user_id && !$isAdmin){
    $ads_manage->set_user($user_id);
}   

if (Utils_Helpers::GetVar('s_subcategory') && Utils_Helpers::GetVar('s_subcategory') != SELECT_SUB_CATEGORY)
    $cat_id = Utils_Helpers::GetVar('s_subcategory');
else if (Utils_Helpers::GetVar('s_category') && Utils_Helpers::GetVar('s_category') != SELECT_CATEGORY)
    $cat_id = Utils_Helpers::GetVar('s_category');    
else if (Utils_Helpers::GetVar('s_type') && Utils_Helpers::GetVar('s_type') != SELECT_TYPE)
    $cat_id = Utils_Helpers::GetVar('s_type');

            
$ads_manage->set_rayon( Utils_Helpers::GetVar('s_raion'))->set_metro( Utils_Helpers::GetVar('s_metro'))->set_category($cat_id);

$ads_manage->set_keyword( Utils_Helpers::GetVar('s_keyword'),  Utils_Helpers::GetVar('s_brand'))->set_cost_category( Utils_Helpers::GetVar('s_cost_category'))->set_descript( Utils_Helpers::GetVar('s_descript'));

//$ads_manage->set_state($state, $_REQUEST['motoh_min'], $_REQUEST['motoh_max'])->set_type_user($_REQUEST['search_company'])->set_photo($_REQUEST['search_photo']);

$ads_manage->set_sort_type( Utils_Helpers::GetVar('sort'), Utils_Helpers::GetVar('direct'));

$city_id = Utils_Helpers::GetVar('s_city', $_COOKIE['compas_city'] ? $_COOKIE['compas_city'] : DEFAULT_CITY_ID);

if (isset($_REQUEST['countries'])){
  if(isset($_REQUEST['s_city']) && is_numeric($_REQUEST['s_city']))
    $ads_manage->set_city($city_id);
  else
    $ads_manage->set_city($_REQUEST['countries']);
}
else
  $ads_manage->set_city($city_id);


$rows = $ads_manage->set_ads_unpublish()->paginate(50, FALSE, $_REQUEST['page'])->get_list_ads();

if ($user_id){
    include "list_ads.php";
?>
<?php } else{?>
Чтобы посмотреть список объявлений нужно авторизоваться на сайте!
<style type='text/css'>.filter{display:none;}</style>
<?php }?>