<style type="text/css">
    .text_comment{
        font-size: 0.8em; font-style: italic; position: relative; top:-4px;
    }
    
    .move_top{
        position: relative; top:-2px;
    }
    
    .move_down{
        position: relative; top:2px;
    }
    
    .control_panel a{
        color: #414042;
        font-style: italic;
        font-size: 0.9em;
    }
    
    .control_panel a span{
        position: relative; top:-5px;
    }
    
    .control_panel .text_active{
        color: #9f8d5f;
        font-size: 0.9em;
        font-style: italic;
    }
    
    .text_active.move_top{
        top:-5px;
    }
    
    .control_panel .text_not_active{
        color: #990000;
    }
    
    .control_panel .text_in_top a, .control_panel .text_in_top{
        color: #d52150;
        font-size: 0.9em;
        font-style: italic;
    }
    
</style>      
<?php
    if (count($rows)){
    $arr_paginate = $ads_manage->arr_paginate; include "paginate.tpl.php";
    }
    ?>    
    <ul class="output">
        <?php foreach ($rows as $item):?>
          <?php
          if (isset($item['ad_kindof']))
            $ad_kindof = explode(',', str_replace(array('[',']','"'), "", $item['ad_kindof']));
          else
            $ad_kindof = false;  
          $cc_images = '';
          $small_img = $ads_manage->get_exist_thumb_image($item['images']);
          foreach ($ad_kindof as $ak){ 
          $cc_images .= "<div class='img' data-title='ценовая категория ".$cost_category[$ak]."'><img src='".$images_cc[$ak]."' alt='' ></div>";} 
          $url = $sef->set_ads($item['id'])->get_sef_url();  
          $company_url  = $sef->set_company($item['userid'])->get_sef_url(); 
          $brands = Ads_Brands::instance()->ads()->get_brands($item['id']);
          
          $list_brand = array();
          foreach($brands as $b){$list_brand[]= $b['name'];}
          
          $list_brand = implode('; ',$list_brand);
          $num_comments = $ads_manage->get_num_comments($item['id']);
          $item['ad_site'] = str_replace(array('www.','http://'),'',$item['ad_site']);
          
          $old = $item['ad_text'];
          $item['ad_text'] = iconv('UTF-8','windows-1251',trim($item['ad_text'])); 
          $item['ad_text'] = substr($item['ad_text'], 0, 210);
          $new = $item['ad_text'] = iconv('windows-1251','UTF-8',$item['ad_text']);
          
          if (strlen($new) != strlen($old))
              $item['ad_text'] .= ' ...';


          ?>
          <li>
            <a href="<?=$url?>" target="_blank"><img src="<?=($item['images'] ? '/images/com_ads/'.$small_img : '/templates/compas_person_cab/img/no-foto.jpg') ?>" width="80" alt=""></a>
            <div class="text-info-output">
                <span><a href="<?=$url?>" target="_blank"><?=$item['ad_headline']?></a></span>
                <p><?=$list_brand?>
                <?php if ($item['ad_text']!=''){?>
                    <br /><?=$item['ad_text']?>
                    
                <?php }?>
                </p>
                <div class="text-info-bottom">
					<div class="price-category-block-output" >
						<?=$cc_images?>
		    		</div>
					<div class="rating-output">
						<!--<p>Отзывы: <?=$num_comments[0]?></p>-->
						<p>Рейтинг: <?php $dispatcher->trigger( 'vrvote', $item['id'].'#0' )?></p>
					</div>
				</div><!--text-info-bottom-->
                
            </div><!--text-info-output-->
            <div class="contact-info">
                <?php if ($item['ad_adress']){?>
                    <span><?=$item['ad_adress']?></span>
                <?php } ?>
                <?php if ($item['ad_phone']){?>
                    <span>Тел.:  <?=$item['ad_phone']?></span>
                <?php } ?>
				    
                <?php if ($item['email']){?>
				<span>e-mail: <a href="mailto:<?=$item['email']?>"><?=$item['email']?></a>
				</span>
                <?php } ?>
                <?php if ($item['ad_site']){?>
				<span><a href="<?=Utils_Link::trueLink($item['ad_site'])?>"><?=$item['ad_site']?></a></span>
                <?php }?>
                <?php if ($company_url){?>
                    <span><a href="<?=$company_url?>">Сайт компании</a></span>
                <?php }?>
                <?php if (/*$item['ad_raiyon']*/0){?>
                    <span>Район:  <?=$ads_manage->get_raiyon($item['ad_raiyon'])?></span>
                <?php } ?>
			</div><!--contact-info-->
                <?php include "control_panel.tpl.php"?>        
          </li>   
        <?php endforeach;?>
    </ul>
    <?php
    if (count($rows)){
    $arr_paginate = $ads_manage->arr_paginate; include "paginate.tpl.php";
    }
    ?>

<script>  

function answer_top(ads_id, cost, date, publish){
        
    if (publish != 1){
        ads_not_active();
        return false;
    } 
    return confirm("Ваша карточка №"+ads_id+" будет поднята в ТОП на срок до "+date+". "); 
}

function answer_delete(ads_id){ 
    return confirm("Ваша карточка №"+ads_id+" будет удалена безвозвратно. Подтвердите действие."); 
}

function ads_not_active(){
    alert("Карточка не активна");
}

</script>

  