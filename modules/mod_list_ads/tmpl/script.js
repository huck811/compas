    function num_photo(){
        return jQuery('#files li').length-jQuery('#files li.no_photo').length;
    }
    
    function add_img_to_list(folder, prefix,file){
        if (num_photo()==<?php echo MAX_PHOTO_ITEMS;?>)
            return false;
        if (jQuery('#files li.main').html()==''){
            jQuery('#files li.main').html('<img src="/images/com_ads/'+folder+prefix+file+'" alt="" file="'+folder+'/'+file+'" /><a href="#" class="del">[Удалить]</a>');
            jQuery('#files li.main').removeClass('no_photo');
        }    
        else{
            var first = false;
            jQuery('#files li.small').each(function(){
                if (jQuery(this).html()=='' && !first){
                    jQuery(this).html('<img src="/images/com_ads/'+folder+prefix+file+'" alt="" file="'+folder+'/'+file+'" /><a href="#" class="del">[Удалить]</a>');
                    //jQuery(this).removeClass('no_photo');
                    first = true;
                }    
            });
        } 
        return true;   
    }
    
	jQuery(function(){
		var btnUpload=jQuery('#upload');
		var status=jQuery('#system-message-container');
		new AjaxUpload(btnUpload, {
			action: '/modules/mod_add_ads/upload/upload-file.php?upload=1',
			name: 'uploadfile',
			onSubmit: function(file, ext){
				if (! (ext && /^(jpg|png|jpeg)$/.test(ext))){ 
                    // extension is not allowed 
					status.text('загружать можно только JPG, PNG, вес не более '+<?php echo MAX_FILE_SIZE ?>+' Кб, не более '+<?php echo MAX_WIDTH ?>+' px');
					return false;
				}
                if (num_photo()==<?php echo MAX_PHOTO_ITEMS;?>){
                    status.text('Максимальное количество изображений: '+<?php echo MAX_PHOTO_ITEMS;?>);
                    return false;
                }
				status.text('Загрузка...');
			},
			onComplete: function(file, response){
				//On completion clear the status
				status.text('');
                response = response.split('##');
				//Add uploaded file to list
				if(response[0]==="success"){
                    add_img_to_list(response[1],'/<?php echo THUMB_PREFIX;?>',file);
				} else{
                    status.html(response[0]);
				}
			}
		});
		
	});
    
    jQuery(document).ready(function(){
        
        jQuery.listen("click", "#files a.del", function(){ // Привязываем событие к кнопке "удалить фото"
          var status=jQuery('#system-message-container');  
          src = jQuery(this).parent().find('img:first').attr('src'); // получаем путь к файлу
          var li = jQuery(this).parent(); // запоминаем контейнер картинки
          var first_li_small = jQuery('#files li.small img:first');
          //if (li.hasClass('small')){
          jQuery.get("/modules/mod_add_ads/upload/upload-file.php?del=1&img="+src, function(response){ // запрос на удаление
              response = response.split('##');
              if(response[0]==="success"){ // успех
                    li.html(''); // очищаем содержимое контейнера с фото
                    if (first_li_small.length)
                        first_li_small.click();
                    else 
                        li.addClass('no_photo');    
              }
              else{ // удалить не получилось
                status.html(response[0]+response[1]);
              }
              
          });
          return false;
    	});
        
        jQuery.listen("click", "#files li.small img", function(){
            if (jQuery(this).parent().hasClass('small')){
                main = jQuery('#files li.main').html();
                small = jQuery(this).parent().html();
                jQuery(this).parent().html(main);
                jQuery('#files li.main').html(small);
            }
            
        });
        
        jQuery("select[name=region]").change(function(){ // при смене региона подгружаем список городов этого региона
            var status=jQuery('#system-message-container');  
            if (jQuery(this).val()=='Выбрать'){
                jQuery("select[name=city]").html('<option>Выбрать</option>');
            }
            else{
              jQuery.get("/modules/mod_add_ads/upload/get_list_city.php?region="+jQuery(this).val(), function(response){ // запрос на список городов по региону
                  response = response.split('##');
                  if(response[0]==="success"){ // успех
                     jQuery("select[name=city]").html(response[1]);
                  }
                  else{ // удалить не получилось
                    status.html(response[0]+response[1]);
                  }
                  
              });
            }    
        });
        
        // Авто-подгрузка производителей
        
        jQuery("select[name=category]").change(function(){ // при смене региона подгружаем список городов этого региона
            var status=jQuery('#system-message-container');  
            if (jQuery(this).val()=='Выбрать'){
                jQuery("select[name=producer]").html('<option>Выбрать</option>');
            }
            else{
              jQuery.get("/modules/mod_add_ads/upload/get_list_producer.php?category="+jQuery(this).val(), function(response){ // запрос на список городов по региону
                  response = response.split('##');
                  if(response[0]==="success"){ // успех
                     jQuery("select[name=producer]").html(response[1]);
                  }
                  else{ // удалить не получилось
                    status.html(response[0]+response[1]);
                  }
                  
              });
            }    
        });
        
        // обрабатываем клик по радиобоксам
        jQuery('#rent_radio').click(function(){
                        jQuery('.block_in.cost').addClass('hide');
                        jQuery('.block_in.cost1, .block_in.cost2').removeClass('hide');
                        
        });
        jQuery('#sell_radio').click(function(){
            jQuery('.block_in.cost').removeClass('hide');
            jQuery('.block_in.cost1, .block_in.cost2').addClass('hide');
        });
        
        jQuery('#bu').click(function(){
            jQuery('.block_in.moto').removeClass('hide');                 
        });
        jQuery('#new').click(function(){
            jQuery('.block_in.moto').addClass('hide');                 
        });
        
        // валидация и отправка формы
        function if_num(item) 
        { 
           pattern = /^[1-9]\d*$/;
	       if (pattern.test(item)) 
              return true;
           else
              return false;                 
        }
        jQuery('#form_add_ads').submit(function (){
            var status=jQuery('#system-message-container');
            jQuery('.form_add_ads input, .form_add_ads select').removeClass('red');
            error = '';
            if (jQuery('input[name=head]').val()=='')
                {error +='Заполните заголовок...'+'<br/>'; jQuery('input[name=head]').addClass('red'); }
            if (jQuery('select[name=category]').val()=='Выбрать')
                {error +='Выберите категорию...'+'<br/>'; jQuery('select[name=category]').addClass('red');}
            if (jQuery('select[name=producer]').val()=='Выбрать')
                {error +='Выберите производителя...'+'<br/>'; jQuery('select[name=producer]').addClass('red');} 
            if (jQuery('input[name=model]').val()=='')
                {error +='Укажите модель...'+'<br/>'; jQuery('input[name=model]').addClass('red');}   
            if (jQuery('select[name=year]').val()=='Выбрать')
                {error +='Выберите год выпуска...'+'<br/>'; jQuery('select[name=year]').addClass('red');} 
            if ((jQuery('input[name=type]:checked').val()=='продам'))
                {
                    err = 0;
                    if(jQuery('input[name=cost]').val()=='')
                        {error +='Укажите цену...'+'<br/>'; err = 1;}
                    if (!if_num(jQuery('input[name=cost]').val())) 
                        {error +='Не верный формат цены...'+'<br/>'; err = 1;}
                    if (err)
                        jQuery('div.cost input[type=text]').addClass('red');

                }  
              
            else if (jQuery('input[name=type]:checked').val()=='сдам')
                {
                    err = 0;
                    if( (jQuery('input[name=cost_smena]').val()=='') && (jQuery('input[name=cost_hour]').val()==''))
                    { error +='Укажите цену аренды...'+'<br/>'; err = 1;}
                    if (!if_num(jQuery('input[name=cost_smena]').val()) || !if_num(jQuery('input[name=cost_hour]').val())) 
                        {error +='Не верный формат цены...'+'<br/>'; err = 1;}
                    if (err)
                        jQuery('div.cost input[type=text]').addClass('red');
                }
            if ((jQuery('input[name=state]:checked').val()=='б/у') && (jQuery('input[name=moto_hour]').val()==''))
                {error +='Укажите наработку в часах...'+'<br/>'; jQuery('input[name=moto_hour]').addClass('red');}    
            if (jQuery('select[name=region]').val()=='Выбрать')
                {error +='Выберите регион...'+'<br/>'; jQuery('select[name=region]').addClass('red');}  
            if (jQuery('select[name=city]').val()=='Выбрать')
                {error +='Выберите город...'+'<br/>'; jQuery('select[name=city]').addClass('red');}                 
 
            jQuery('input[name=images]').val('');
            jQuery('#files li img').each(function(){
                jQuery('input[name=images]').val(jQuery('input[name=images]').val()+jQuery(this).attr('file')+'##');
            }); 
               
            if (error!='')
                status.html(error);
            else{
                jQuery.post("/modules/mod_add_ads/upload/add_ads.php", $("#form_add_ads").serialize(),function(response){ // добавляем объявление
                  response = response.split('##');
                  if(response[0]==="success"){ // успех
                     status.html(response[0]+response[1]);
                  }
                  else{ // добавить не получилось
                    status.html(response[1]);
                  }
                  
                });
                
            }    
                
            return false;    
        });
        
    });