<?php
/**
 * @package		Joomla.Site
 * @subpackage	mod_banners
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;
$baseurl = JURI::base();
require_once 'geo/class/app/app.php';

$user =& JFactory::getUser();
$user_id = $user->get('id');
$ads_id = htmlspecialchars($_GET['ads']);

$ads_manage = new Ads_AdsManage(); 
if ($ads_id && $user_id)
    $ads_manage->ads_selled($user_id, $ads_id);
?>