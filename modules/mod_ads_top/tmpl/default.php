<?php
/**
 * @package		Joomla.Site
 * @subpackage	mod_banners
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;
$baseurl = JURI::base();

require_once 'geo/class/app/app.php';

$ads_manage = new Ads_AdsManage(); 
$sef = new MySef();

$show_top = NUM_ADS_IN_TOP;

$cost_category = unserialize(COST_CATEGORY);

$images_cc = unserialize(COST_CATEGORY_IMG);

if (isset($_GET['ads_id']) || isset($_GET['u_id']))
    $show_top = false;

JPluginHelper::importPlugin( 'content', 'vrvote' ); 
$dispatcher =& JDispatcher::getInstance();    
    
//$show_top = false;   // Временно отключаем топ  
if ($show_top){

    $city_id = Utils_Helpers::GetVar('s_city', $_COOKIE['compas_city'] ? $_COOKIE['compas_city'] : DEFAULT_CITY_ID);

    if (isset($_REQUEST['countries'])){
      if(isset($_REQUEST['s_city']) && is_numeric($_REQUEST['s_city']))
        $ads_manage->set_city($city_id);
      else
        $ads_manage->set_city($_REQUEST['countries']);
    }
    else
      $ads_manage->set_city($city_id);

    $rows = $ads_manage->get_list_top();
    //print_r($rows);
    
    $num_top_p = $num_top = count($rows);

    if ($num_top_p && $num_top_p > NUM_ADS_IN_TOP)
        $num_top_p = NUM_ADS_IN_TOP; 
    if ($num_top_p)
        include 'default.phtml';
} 

?>