<?php
/**
 * @package		Joomla.Site
 * @subpackage	mod_breadcrumbs
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;

require_once 'geo/class/app/app.php';

class modBreadCrumbsHelper
{
	public static function getList(&$params)
	{
		// Get the PathWay object from the application
		        
        $cat_id = Utils_Helpers::GetVar('search_category');
        $cat_id = Utils_Helpers::GetVar('search_category_1', $cat_id);
        //$cat_id = isset($_REQUEST['catid']) ? $_REQUEST['cat_id'] : $cat_id;
        $ads_id = Utils_Helpers::GetVar('ads_id');
        //$cat_id = 0;
        
        $sef = new MySef();
        if ($ads_id){
               $ads_manage = new Ads_AdsManage();
               $item = new stdClass();
               $items = array();
               $ads_manage->get_ads(false, $ads_id);
               $item->name = $ads_manage->head;
               $item->link = '';
               $item_ads = $item;
               $cat_id = $ads_manage->category;
               
               $item = new stdClass();
               $item->name =  stripslashes(htmlspecialchars('Главная', ENT_COMPAT, 'UTF-8'));
               $item->link =  '/';
               $items[] = $item;
                
               $item = new stdClass();
               $item->name =  stripslashes(htmlspecialchars('Магазины', ENT_COMPAT, 'UTF-8'));
               $item->link =  'magaziny.html';
               $items[] = $item;
               $items[] = $item_ads;
               //print_r($items);
               $count = count($items);
        }
        
        if ($cat_id){
            /*$category = new Ads_Category();
            
            $items = array();
            $rs = $category->get_arr_category_tree_cat_id($cat_id);
            //print_r($rs);
            
            //print_r($item);
            $num = count($rs)-1;
           
            for($i=$num; $i>=0; $i--){
               $item = new stdClass();
               $item->name = $rs[$i]['name'];
               $item->link = JRoute::_('index.php?option=com_adsmanager&Itemid=138&search_category='.$rs[$i]['id'].'&view=list');
               $items[] = $item;
               //print_r($item);
            }*/
            
            /*if ($ads_id)
                $items[] = $item_ads;*/
            //print_r($items);
            $count = count($items);
        }else{
            $app		= JFactory::getApplication();
    		$pathway	= $app->getPathway();
    		$items		= $pathway->getPathWay();
            //echo '<pre>';
            //print_r($items);
    		$count = count($items);
    		for ($i = 0; $i < $count; $i ++)
    		{
    			$items[$i]->name = stripslashes(htmlspecialchars($items[$i]->name, ENT_COMPAT, 'UTF-8'));
    			$items[$i]->link = JRoute::_($items[$i]->link);
    		}
    
    		if ($params->get('showHome', 1))
    		{
    			$item = new stdClass();
    			$item->name = htmlspecialchars($params->get('homeText', JText::_('MOD_BREADCRUMBS_HOME')));
    			$item->link = JRoute::_('index.php?Itemid='.$app->getMenu()->getDefault()->id);
    			array_unshift($items, $item);
    		}
        }
		return $items;
	}

	/**
	 * Set the breadcrumbs separator for the breadcrumbs display.
	 *
	 * @param	string	$custom	Custom xhtml complient string to separate the
	 * items of the breadcrumbs
	 * @return	string	Separator string
	 * @since	1.5
	 */
	public static function setSeparator($custom = null)
	{
		$lang = JFactory::getLanguage();

		// If a custom separator has not been provided we try to load a template
		// specific one first, and if that is not present we load the default separator
		if ($custom == null) {
			if ($lang->isRTL()){
				$_separator = JHtml::_('image', 'system/arrow_rtl.png', NULL, NULL, true);
			}
			else{
				$_separator = JHtml::_('image', 'system/arrow.png', NULL, NULL, true);
			}
		} else {
			$_separator = htmlspecialchars($custom);
		}

		return $_separator;
	}
}
