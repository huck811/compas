    <!-- sort -->
    <?php

    Utils_Link::parse($_SERVER['REQUEST_URI']);

    Utils_Link::params(array('direct' => 'DESC', 'sort' => 'rating'));
    $uri_rating_desc = Utils_Link::build_uri();

    Utils_Link::params(array('direct' => 'ASC'));
    $uri_rating_asc = Utils_Link::build_uri();

    Utils_Link::params(array('direct' => 'DESC', 'sort' => 'rayon'));
    $uri_rayon_desc = Utils_Link::build_uri();

    Utils_Link::params(array('direct' => 'ASC'));
    $uri_rayon_asc = Utils_Link::build_uri();

    Utils_Link::params(array('direct' => 'DESC', 'sort' => 'cost_category'));
    $uri_cost_category_desc = Utils_Link::build_uri();

    Utils_Link::params(array('direct' => 'ASC'));
    $uri_cost_category_asc = Utils_Link::build_uri();

    Utils_Link::params(array('direct' => 'DESC', 'sort' => 'name'));
    $uri_name_desc = Utils_Link::build_uri();

    Utils_Link::params(array('direct' => 'ASC'));
    $uri_name_asc = Utils_Link::build_uri();

    ?>
	<a name="sort" style="position: relative; top:-120px; display: block;"></a>
    <div class="sorting">
		<ul>
			<li>
				<span>Сортировать по:</span>
			</li>
			<li>
				<span>рейтингу магазина</span>
				<a href="<?=$uri_rating_desc?>#sort" class="sort-decrease <?=($_GET['direct']=='DESC' & $_GET['sort']=='rating') ? 'active' : ''?>"></a>
				<a href="<?=$uri_rating_asc?>#sort" class="sort-increase <?=($_GET['direct']=='ASC' & $_GET['sort']=='rating') ? 'active' : ''?>"></a>
			</li>
			<li>
				<span>району города</span>
				<a href="<?=$uri_rayon_desc?>#sort" class="sort-decrease <?=($_GET['direct']=='DESC' & $_GET['sort']=='rayon') ? 'active' : ''?>"></a>
				<a href="<?=$uri_rayon_asc?>#sort" class="sort-increase <?=($_GET['direct']=='ASC' & $_GET['sort']=='rayon') ? 'active' : ''?>"></a>
			</li>
			<li>
				<span>ценовой категории</span>
				<a href="<?=$uri_cost_category_desc?>#sort" class="sort-decrease <?=($_GET['direct']=='DESC' & $_GET['sort']=='cost_category') ? 'active' : ''?>"></a>
				<a href="<?=$uri_cost_category_asc?>#sort" class="sort-increase <?=($_GET['direct']=='ASC' & $_GET['sort']=='cost_category') ? 'active' : ''?>"></a>
			</li>
			<li>
				<span>по названию магазина</span>
				<a href="<?=$uri_name_desc?>#sort" class="sort-decrease <?=($_GET['direct']=='DESC' & $_GET['sort']=='name') ? 'active' : ''?>"></a>
				<a href="<?=$uri_name_asc?>#sort" class="sort-increase <?=($_GET['direct']=='ASC' & $_GET['sort']=='name') ? 'active' : ''?>"></a>
			</li>
		</ul>
	</div>
    <!-- Output sort -->