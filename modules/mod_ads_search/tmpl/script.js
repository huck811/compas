
       var default_values = new Array();
       var http_get_data  = new Array();
       
       // setting default values for selects
       default_values['s_category']    = '<?=SELECT_CATEGORY?>';
       default_values['s_subcategory'] = '<?=SELECT_SUB_CATEGORY?>';
       default_values['s_city']        = '<?=DEFAULT_SELECT_VAL1?>';
       default_values['s_raion']       = '<?=SELECT_RAION?>';
       default_values['s_metro']       = '<?=SELECT_METRO?>';
       default_values['countries']     = '<?=SELECT_COUNTRY?>';
       
       // setting get data from php form
       http_get_data['s_category']    = '<?=Utils_Helpers::GetVar('s_category', '')?>';
       http_get_data['s_subcategory'] = '<?=Utils_Helpers::GetVar('s_subcategory', '')?>';
       http_get_data['s_city']        = '<?=Utils_Helpers::GetVar('s_city', '')?>';
       http_get_data['s_raion']       = '<?=Utils_Helpers::GetVar('s_raion', '')?>';
       http_get_data['s_metro']       = '<?=Utils_Helpers::GetVar('s_metro', '')?>';
       http_get_data['countries']     = '<?=Utils_Helpers::GetVar('countries', '')?>';
       
       
       function isNumber(n) {
           return !isNaN(parseFloat(n)) && isFinite(n);
       }
         
       function set_disabled_select(name){
            if (jQuery('.filter select[name='+name+'] option').length < 2){
                jQuery('.filter select[name='+name+']').attr('disabled', 'disabled');
                //alert(jQuery('.filter select[name='+name+'] option').length);
            }    
            else
                jQuery('.filter select[name='+name+']').removeAttr('disabled');
            
        }
        
        function set_selected(select_name, selected_val){
            jQuery("select[name='"+select_name+"'] option[value='"+selected_val+"']").prop('selected', true);
            jQuery("select[name='"+select_name+"']").trigger('refresh');
        }
        
       function set_option_list(opt_list, select_name, not_set_selected){
            jQuery("select[name='"+select_name+"']").html(opt_list);
            set_disabled_select(select_name); 
            if (!not_set_selected && http_get_data[select_name] !='' && http_get_data[select_name] != default_values[select_name])
                set_selected(select_name, http_get_data[select_name]);
            jQuery("select[name='"+select_name+"']").trigger('refresh');
       }
       
        
        function set_sub_category(category){
            if (category=='<?=SELECT_CATEGORY?>'){
                //jQuery("select[name=s_subcategory]").html('<option><?=SELECT_SUB_CATEGORY?></option>');
                set_option_list('<option><?=SELECT_SUB_CATEGORY?></option>', 's_subcategory');
            }
            else{
            jQuery.get("/modules/mod_add_ads/upload/get_list_category.php?category="+category, function(response){ // запрос на список подкатегорий по категории
                  response = response.split('##');
                  //alert(response);
                  if(response[0]==="success"){ // успех
                    set_option_list(response[1], 's_subcategory');    
                  }
                  
              }); 
            }  
              
        }
        
        function set_country(type){
            
            if (type == 1 || http_get_data['countries'] == '') 
                var country_name = jQuery('select.header-select option[value="'+jQuery.cookie('compas_city')+'"]').parent().attr('cid');
            else
                var country_name = jQuery('select.header-select option[value="'+http_get_data['s_city']+'"]').parent().attr('cid');


            jQuery('select.countries option').each(function(){
                  if (jQuery(this).html() == country_name)
                        jQuery(this).prop('selected', true);
            });
            jQuery("select.countries").trigger('refresh');
            jQuery("select.countries").change();

        }
        
        function set_city(country, default_val, submit){
            if (country == default_values['countries']){
                set_option_list('<option>'+default_values['s_city']+'</option>', 's_city');
                jQuery('select[name="s_city"]').change();
            } 
            else{
                jQuery.get("/modules/mod_add_ads/upload/get_list_city.php?region="+country, function(response){ // запрос на список городов по стране
                      response = response.split('##');
                      if(response[0]==="success"){ // успех
                         set_option_list(response[1], 's_city');
                         
                         set_selected('s_city', default_val);
                         jQuery('select[name="s_city"]').change();
                         if (submit)
                             jQuery('.filter form').submit();
                      }

                  }); 
            }  
    
              
        }
        
        function set_rayon(city){
            jQuery.get("/modules/mod_add_ads/upload/get_list_rayon.php?city="+city, function(response){ // запрос на список районов по городу
                  response = response.split('##');
                  if(response[0]==="success"){ // успех
                     set_option_list(response[1], 's_raion'); 
                  }
                  
                  
              });
    
              
        }
        
        
        function set_metro(city){
            jQuery.get("/modules/mod_add_ads/upload/get_list_metro.php?city="+city, function(response){ // запрос на список районов по городу
                  response = response.split('##');
                  if(response[0]==="success"){ // успех
                      set_option_list(response[1], 's_metro');
                  }   
              });
    
              
        }
        
        
        
        jQuery("select[name='s_category']").change(function(){ // при смене категории подгружаем список подкатегорий
            set_sub_category(jQuery(this).val());              
        });
        
       
         jQuery("select.countries").change(function(){ // при смене страны погружаем список городов
             set_city(jQuery(this).val());
            
            //jQuery("select[name='s_city']").change();
              
        });
        
         jQuery("select[name=s_city]").change(function(){ // при смене города погружаем список районов
            
            if (jQuery(this).val() == '<?=DEFAULT_SELECT_VAL1?>'){
                set_option_list('<option><?=SELECT_RAION?></option>', 's_raion');
                set_option_list('<option><?=SELECT_METRO?></option>', 's_metro');
            }
            else{
               
              set_rayon(jQuery(this).val());
              set_metro(jQuery(this).val());
            }  
              
        });
        
        var isChanged = false;
        jQuery('div.filter select, div.filter input').change(function(){
            isChanged = true;
        });

        jQuery('div.filter form').submit(function(){
             if (jQuery('#brand_input').val() != '' && jQuery('input[name="s_brand"]').val() == '')
                jQuery('input[name="s_brand"]').val(jQuery('#brand_input').val());
             
             if (!isChanged)
                return false;
        });    
        
        set_selected('s_category', '<?=Utils_Helpers::GetVar('s_category', '')?>');
        set_sub_category(jQuery("select[name='s_category']").val()); 
        
        
        jQuery(document).ready(function(){ 
                jQuery('select.header-select').change(function(){                    
                     //alert(country_name);
                    set_country(1);
                    city_def = jQuery.cookie('compas_city') ? jQuery.cookie('compas_city') : 2012;
                    set_city(jQuery("select.countries").val(), city_def, true);
                    //document.location.reload();
                    
                });
                
                 //set_country(2);
                 /*city_def = jQuery.cookie('compas_city') ? jQuery.cookie('compas_city') : 2012;
                 if (http_get_data['s_city'] == ''){
                    set_city(jQuery("select.countries").val(), city_def);
                 }
                 else{
                    set_city(jQuery("select.countries").val(), http_get_data['s_city']); 
                 }*/
                 //set_city(jQuery("select.countries option:selected").val());

            city_def = http_get_data['s_city'] ? http_get_data['s_city'] : jQuery.cookie('compas_city');
            set_city(jQuery("select.countries").val(), city_def);
                   

                
                var cache = {},
                    lastXhr;
                    jQuery('input[name="s_keyword"]').autocomplete({
                       minLength: 3,
                       source: function( request, response ) {
                          var term = request.term;
                          
                          if ( term in cache ) {
                             response( cache[ term ] );
                             return;
                          }
                          
                          lastXhr = jQuery.get( "/modules/mod_ads_search/tmpl/get_brand_and_header_dynamic.php?callback=?", request, function( data, status, xhr ) {
                             cache[ term ] = data;
                             if ( xhr === lastXhr ) {
                                isChanged = true;
                                response( data );
                             }
                             
                          });
                           
                       },
                       select: function( event, ui ) {
                          jQuery('#brand_search').val(ui.item.id);
                      }
                    });
                
                if (jQuery('input[name="s_keyword"]').val() == '' && '<?=Utils_Helpers::GetVar('s_keyword', '')?>' != '')
                     jQuery('input[name="s_keyword"]').val('<?=Utils_Helpers::GetVar('s_keyword', '')?>');
                });