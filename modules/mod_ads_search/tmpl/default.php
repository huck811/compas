<?php
/**
 * @package		Joomla.Site
 * @subpackage	mod_banners
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;
$baseurl = JURI::base();

require_once 'geo/class/app/app.php';

$session =& JFactory::getSession(); 

$cat_id     = Utils_Helpers::GetVar('search_category');
$cat_id     = Utils_Helpers::GetVar('search_category_1', $cat_id);
$cat_sub_id = Utils_Helpers::GetVar('search_sub_category');


$s_brand   = Utils_Helpers::GetVar('s_brand');

$ads_manage = new Ads_AdsManage(); 
$category = new Ads_Category();
$city = new cities();

$city_id   = Utils_Helpers::GetVar('s_city', $_COOKIE['compas_city'] ? $_COOKIE['compas_city'] : DEFAULT_CITY_ID);
$raion_id  = Utils_Helpers::GetVar('s_raion');
$countries = Utils_Helpers::GetVar('countries', $city->get_ar_country_by_city_id($city_id));

$arr_category = $category->get_ar_category(0, true, 1, true);
$arr_subcategory = $category->get_ar_category(0, true, 2, true);
$country_list = $city->get_ar_countries('sel-default countries', '', 'countries', 'countries', array(SELECT_COUNTRY), $countries);

$city_list = $city->ajax()->set_city($city_id)->get_cities_by_country('','Город:','city','',array(DEFAULT_SELECT_VAL1), $countries);

$raion_list = '<option>'.SELECT_RAION.'</option>';
$raion_list .= $ads_manage->get_rayon_and_okrug($city_id)->get_options('id', 'name', false, $raion_id);

$metro_list = '<option>'.SELECT_METRO.'</option>';
$metro_list .= $ads_manage->get_list_metro_and_line($city_id)->get_options('id', 'name', false, $_REQUEST['s_metro']);


$uri = explode('?',$_SERVER['REQUEST_URI']);
$uri = $uri[0];

if ($s_brand) 
    Ads_Brands::instance()->ads()->get_brands($s_brand);

    if (!isset($_GET['ads_id']))
        include "default.phtml";
   
?>
     
       
    <script type="text/javascript">
        <?php include "script.js";?>
    </script>
<?php /*} else{ //главная*/?>
<style>div.breadcrumbs{padding: 0;}</style>
<?php /*}*/?>

<?php if (isset($_REQUEST['ads_id'])){?>
<style>div.module h3{display: none;}</style>
<?php }


?>
