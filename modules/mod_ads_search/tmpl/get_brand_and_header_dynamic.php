<?php
header("Content-Type: application/json;charset=UTF-8");
include "../../../geo/class/app/app.php";
$limit = 10; // сколько «подсказок» мы выдадим пользователю
$res_text = array();
$res = array();

if (!empty($_GET['term'])) {// переменную 'term' передает виджет «Autocomplete»
    $text = $_GET['term'];
    $db = new Db_Connect(SERVER, DB_NAME, DB_USER, DB_PASS);

    $q2 = '(SELECT DISTINCT LOWER(c.name) as name, c.id FROM #__adsmanager_categories AS c'
            . ' WHERE c.published = 1 AND UPPER(c.name) LIKE UPPER(:text) LIMIT ' . ($limit) . ')';
    $rows2 = $db->bind_param(array('text' => $text . '%'))->fetchAll($q2);
    
    $limit = $limit - count($rows2);

    if ($limit > 0){
        $q1 = '(SELECT DISTINCT LOWER(ad_headline) as name, LOWER(ad_text) as text'
                . ' FROM #__adsmanager_ads WHERE published = 1 AND UPPER(ad_headline) LIKE UPPER(:text)'
                . ' OR  UPPER(ad_text) LIKE UPPER(:text) GROUP BY ad_headline LIMIT ' . ($limit) . ')';
        $rows1 = $db->bind_param(array('text' => '%' . $text . '%'))->fetchAll($q1);
        $limit = $limit - count($rows1);
    }
    
    
    
    if ($limit > 0){
        $q = '(SELECT DISTINCT LOWER(br.name) as name, br.id FROM #__adsmanager_brands AS br,'
                . ' #__adsmanager_adsbrand AS ab, #__adsmanager_ads AS ads  WHERE ads.id = ab.id_ads AND'
                . ' ads.published = 1 AND ab.id_brand = br.id AND UPPER(br.name) LIKE UPPER(:text) LIMIT ' . ($limit) . ')';
        $rows = $db->bind_param(array('text' => $text . '%'))->fetchAll($q);
        $limit = $limit - count($rows);
    }
    
    if ($limit > 0){
        $q3 = '(SELECT DISTINCT LOWER(r.name) as name, r.id FROM #__list_rayon AS r,'
                . ' #__adsmanager_ads AS ads  WHERE ads.ad_raiyon = r.id AND'
                . ' ads.published = 1 AND UPPER(r.name) LIKE UPPER(:text) LIMIT ' . ($limit) . ')';
        $rows3 = $db->bind_param(array('text' => $text . '%'))->fetchAll($q3);
        $limit = $limit - count($rows3);
    }
    
    if ($limit > 0){
        $q4 = '(SELECT DISTINCT LOWER(m.name) as name, m.id FROM #__list_metro AS m,'
                . ' #__adsmanager_ads AS ads  WHERE ads.ad_near_metro = m.id AND'
                . ' ads.published = 1 AND UPPER(m.name) LIKE UPPER(:text) LIMIT ' . ($limit) . ')';
        $rows4 = $db->bind_param(array('text' => $text . '%'))->fetchAll($q4);
        $limit = $limit - count($rows4);
    }
    
    if ($limit > 0){
        $q5 = '(SELECT DISTINCT LOWER(ads.ad_adress) as name, ads.id FROM '
                . ' #__adsmanager_ads AS ads  WHERE '
                . ' ads.published = 1 AND UPPER(ads.ad_adress) LIKE UPPER(:text) LIMIT ' . ($limit) . ')';
        $rows5 = $db->bind_param(array('text' => '%' . $text . '%'))->fetchAll($q5);
        $limit = $limit - count($rows5);
    }
    
    
    

   foreach ($rows2 as $k => $v) {
        $name = str_replace('&amp;', '&', $v["name"]);
        //$word = find_word($name, $text);

        //$word = find_word($v["text"], $text);
        
        /*if(!$word)
            $word = $name;*/
        if (!in_array($name, $res_text)) {
   
            $res[] = array("cat_id" => $v["id"], "label" => $name, "value" => $name);
            $res_text[] = $name;
            
        }
    }

    foreach ($rows1 as $k => $v) {
        $name = str_replace('&amp;', '&', $v["name"]);
        $word = find_word($name, $text);
        if(!$word)
          $word = find_word($v["text"], $text);
        
        if(!$word)
            $word = $name;
        if (!in_array($word, $res_text)) {
   
            $res[] = array("ads_id" => $v["id"], "label" => $word, "value" => $word);
            $res_text[] = $word;
            
        }
    }
    
    foreach ($rows as $k => $v) {
        $name = str_replace('&amp;', '&', $v["name"]);
        $res[] = array("id" => $v["id"], "label" => $name, "value" => $name);
        $res_text[] = $name;
    }
    
    foreach ($rows3 as $k => $v) {
        $name = str_replace('&amp;', '&', $v["name"]);
        $res[] = array("id" => '', "label" => $name, "value" => $name);
        $res_text[] = $name;
    }
    
    foreach ($rows4 as $k => $v) {
        $name = str_replace('&amp;', '&', $v["name"]);
        $res[] = array("id" => '', "label" => $name, "value" => $name);
        $res_text[] = $name;
    }
    
    foreach ($rows5 as $k => $v) {
        $name = str_replace('&amp;', '&', $v["name"]);
        $word = find_word($name, $text);

        //$word = find_word($v["text"], $text);
        
        if(!$word)
            $word = $name;
        if (!in_array($word, $res_text)) {
   
            $res[] = array("ads_id" => $v["id"], "label" => $word, "value" => $word);
            $res_text[] = $word;
            
        }
    }
    // формируем нужный массив и прекращаем работу скрипта
    die(json_encode($res));
    echo '<pre>';
    print_r($res);
    echo '------------';
    print_r($rows1);
  
}

function find_word($haystack, $find) {
    $code = 'UTF-8';
    $find = trim($find);
    $find = trim($find, ',');
    $find = trim($find, '.');
    $haystack = htmlspecialchars($haystack);
    $haystack = str_replace(array(chr(13), chr(10)), ' ', $haystack);
    $str_length  = mb_strlen($haystack, $code);
    $find_length = mb_strlen($find, $code);
    if (mb_substr_count($haystack, $find, $code)) {
        $pos = mb_strpos($haystack, $find, 0, $code);
        $start = $stop = false;

        if ($pos != 0) {
            for ($i = $pos; $i > 0; $i--) {
                if (mb_substr($haystack, $i, 1, $code) == " ") {
                    $start = $i + 1;
                    break;
                }
            }
        } else {
            $start = 0;
        }

        for ($i = $pos+$find_length; $i < $str_length; $i++) {
            if (mb_substr($haystack, $i, 1, $code) == " ") {
                $stop = $i;
                break;
            }
        }
        
        if (!$stop)
           $stop = $str_length;
        
        if ($start !== false && $stop !== false && $stop > $start) {
            $res = mb_substr($haystack, $start, $stop - $start, $code);
            $res = trim($res, ',');
            $res = trim($res, '.');
            return $res;
        }
    }
    return false;
}

?>