<?php
header("Content-Type: application/json;charset=UTF-8");
include "../../../geo/class/app/app.php";
$limit = 10; // сколько «подсказок» мы выдадим пользователю
$res = array();
 if (!empty($_GET['term']))// переменную 'term' передает виджет «Autocomplete»
 {
    $text = $_GET['term'];
    $db  = new Db_Connect(SERVER, DB_NAME, DB_USER, DB_PASS);   

    $q = 'SELECT * FROM geo__cities WHERE UPPER(city) LIKE UPPER(:text) LIMIT '.$limit;
    
    $rows = $db->bind_param(array('text' => $text.'%'))->fetchAll($q);
    
    foreach ($rows as $k => $v) {
       $res[] = array("id"=>$v["city_id"],"label"=>$v["city"],"value"=>$v["city"]);
    }
    // формируем нужный массив и прекращаем работу скрипта
    die(json_encode($res));
    
    //print_r($res);
 }
?>