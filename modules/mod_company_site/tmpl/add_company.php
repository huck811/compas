<?php
require_once '../../../geo/class/app/app.php';
$p = new PostData();
$user = new Ads_User();
    
$valid = new Validate_Company();
if ($valid->run()){
    if($p->company_id && md5($p->company_id.'check') == $p->session){
        if(!$user->get_company($p->company_id))
            $company_no_exist = true;
        $user->save_company($p);
        if ($company_no_exist)
            exit('success##Ваш сайт компании создан.'); 
        else
            exit('success##Ваш сайт компании изменен.'); 
    }     
  
    exit('error##Ошибка: '.implode(' / ',$user->error));  
}
else{
    exit('##'.$valid->get_string_errors().'##'.$valid->get_errf_to_json());
}

?>