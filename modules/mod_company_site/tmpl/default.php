<?php
/**
 * @package		Joomla.Site
 * @subpackage	mod_banners
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;
$baseurl = JURI::base();

require_once 'geo/class/app/app.php';
require_once "geo/cache.php";

$user =& JFactory::getUser();
$user_id = $user->get('id');

$ads_manage = new Ads_AdsManage();

$isAdmin = $user->get('isRoot');

$ads_user = new ads_user();
$ads_user->get_user_data($user_id);

$ads_manage->set_user($user_id);
$count_ads = $ads_manage->get_list_ads(1);

$form_c = new FormConstructor();
$sef = new mysef();

$company_data = $ads_user->get_company($user_id);  


$company_name = $form_c
                ->type()
                ->name('company_name')
                ->init($company_data['company_name'])
                ->attr('maxlength="40"')
                ->attr('placeholder="Название компании *"')
                ->insert();

$logo_btn = $form_c
                ->type('INPUT','button')
                ->name('logo')
                ->init("Выберите файл")
                ->nowrapp()
                ->id('upload')
                ->insert();

$im = explode('/',$company_data['images']);
$logo_input = $form_c
                ->type()
                ->name('images_view')
                ->init($im[count($im)-1])
                ->attr('placeholder="Файл не выбран", disabled="disabled"')
                ->nowrapp()
                ->insert();

$short_descript = $form_c
                ->type('TEXTAREA')
                ->name('short_descript')
                ->init($company_data['short_descript'])
                ->attr('placeholder="Краткое описание *"')
                ->insert();

$full_descript = $form_c
                ->type('TEXTAREA')
                ->name('full_descript')
                ->init($company_data['full_descript'])
                ->attr('placeholder="Описание"')
                ->insert();

$name = $form_c
                ->type()
                ->name('name')
                ->init($company_data['name'])
                ->attr('placeholder="Имя"')
                ->insert();

$phone1 = $form_c
                ->type()
                ->name('phone1')
                ->init($company_data['phone1'])
                ->attr('placeholder="Телефон 1"')
                ->insert();

$phone2 = $form_c
                ->type()
                ->name('phone2')
                ->init($company_data['phone2']) 
                ->attr('placeholder="Телефон 2"')
                ->insert();

$adress = $form_c
                ->type()
                ->name('adress')
                ->init($company_data['adress'])    
                ->attr('placeholder="Адрес"')
                ->insert();

$email = $form_c
                ->type()
                ->name('email')
                ->init($company_data['email'])
                ->attr('placeholder="E-mail *"')
                ->insert();

$company_phone1 = $form_c
                ->type()
                ->init($company_data['company_phone1'])
                ->name('company_phone1')
                ->attr('placeholder="Телефон 1"')
                ->insert();

$company_phone2 = $form_c
                ->type()
                ->name('company_phone2')
                ->init($company_data['company_phone2'])
                ->attr('placeholder="Телефон 2"')
                ->insert();

$company_phone3 = $form_c
                ->type()
                ->name('company_phone3')
                ->init($company_data['company_phone3'])
                ->attr('placeholder="Телефон 3"')
                ->insert();

$company_adress = $form_c
                ->type()
                ->name('company_adress')
                ->init($company_data['company_adress'])
                ->attr('placeholder="Адрес"')
                ->insert();

$company_email = $form_c
                ->type()
                ->name('company_email')
                ->init($company_data['company_email'])
                ->attr('placeholder="E-mail"')
                ->insert();

$company_site = $form_c
                ->type()
                ->name('company_site')
                ->init($company_data['company_site'])
                ->attr('placeholder="Сайт:"')
                ->insert();

/*$show_all_shop = $form_c
                ->type('INPUT','checkbox')
                ->name('show_all_shop')
                ->_class('superstyle')
                ->nowrapp()
                ->insert();*/

$save = $form_c
                ->type('INPUT','submit')
                ->name('save')
                ->init("Сохранить")
                ->insert();

$company_url  = $sef->set_company($user_id)->get_sef_url(); 

include 'default.phtml';
//require_once "script.js.php";
 