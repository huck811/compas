jQuery(document).ready(function(){
     
     var option_is_clicked = false;
     jQuery('option').click(function(){
        option_is_clicked = true;
     });
     jQuery('optgroup').click(function(){
        if (!option_is_clicked)
            jQuery('option',this).each(function(){jQuery(this).prop('selected','selected')});
        option_is_clicked = false;    
     });
     
     //jQuery("select[name=ad_city]").attr('validate_error','Выберите город....');
     jQuery("select[name=region]").change(function(){ // при смене региона подгружаем список городов этого региона
            var status = jQuery('div.system_message > div');  
            if (jQuery(this).val()=='Выбрать'){
                jQuery("select[name=ad_city]").html('<option>Выбрать</option>');
            }
            else{
              jQuery.get("/modules/mod_add_ads/upload/get_list_city.php?region="+encodeURIComponent(jQuery(this).val()), function(response){ // запрос на список городов по региону
                  response = response.split('##');
                  if(response[0]==="success"){ // успех
                     jQuery("select[name=ad_city]").html(response[1]);
                  }
                  else{ // удалить не получилось
                    status.html(response[0]+response[1]);
                  }
                  
              });
            }    
        });

        jQuery("select[name='city']").change(function(){ // при смене города подгружаем районы
            if (jQuery(this).val() == DEFAULT_SELECT_VAL){
                jQuery("select[name=raiyon]").html('<option>' + DEFAULT_SELECT_VAL + '</option>');
            }
            else{
              set_rayon(jQuery(this).val());
              
            } 
            jQuery("select[name=metro]").html('<option>' + SELECT_METRO + '</option>');
            set_metro(jQuery(this).val());
              
        });
        
        jQuery("select[name='raiyon']").change(function(){ // при смене района подгружаем станции метро
            if (jQuery(this).val() == SELECT_RAION){
                jQuery("select[name=metro]").html('<option>' + SELECT_METRO + '</option>');
            } 
              
        });
        
        function set_rayon(city){

            jQuery.get("/modules/mod_add_ads/upload/get_list_rayon.php?optgroup=1&city="+city, function(response){ // запрос на список районов по городу
                  response = response.split('##');
                  if(response[0]==="success"){ // успех
                     //
                     jQuery("select[name='raiyon']").html('');
                     //alert(city);
                     jQuery("select[name='raiyon']").html(response[1])
                     jQuery("select[name='metro'] option").removeAttr('selected');
                     jQuery("select[name='s_raion']").trigger('refresh');
                  }
                   
              });
    
              
        }
        
        function set_metro(city){
            jQuery.get("/modules/mod_add_ads/upload/get_list_metro.php?optgroup=1&city="+city, function(response){ // запрос на список районов по городу
                  response = response.split('##');
                  //alert(response);
                  if(response[0]==="success"){ // успех
                     //
                     jQuery("select[name='metro']").html('');
                     jQuery("select[name='metro']").html(response[1]);
                  }
                  
              });
    
              
        }
        
    
     // валидация и отправка формы
        jQuery('#form_add_profile').submit(function (){
            var status = jQuery('div.system_message > div');
            jQuery('#form_add_profile input, #form_add_profile select, #form_add_profile textarea, div.cost_category').removeClass('red');
            error = '';
            
            // компонуем типы ценовых категорий
            var cost_cat  = [];
            jQuery('input[name="cost_category"]').val('');
            jQuery('input[type="checkbox"].cost_cat').each(function(){
                if (jQuery(this).prop('checked'))
                    cost_cat[cost_cat.length] = jQuery(this).val();
                
                if (cost_cat.length){                       
                   jQuery('input[name="cost_category"]').val(cost_cat);
                }   
            });
            
            //компонуем бренды
            var brands  = [];
            jQuery('input[name="brands"]').val('');
            jQuery('.all_brands input[type="text"]').each(function(){
                if (jQuery(this).val()!= jQuery('#default_brand_val').val()){
                    str = jQuery(this).val();
                    brands[brands.length] = str.replace(/,/g,'#');
                }    
    
            });


            if (brands.length) {  
                   jQuery('input[name="brands"]').val(brands);
            } 
 
            if (jQuery('.img_cont li.main img').length)
                jQuery('input[name="ad_avatar"]').val(jQuery('.img_cont li.main img').attr('src'));
            else
                jQuery('input[name="ad_avatar"]').val('');
            jQuery('div.loading').fadeIn(200);
                jQuery.post("/templates/compas_person_cab/html/com_users/profile/add_profile.php?uid=" + user_id + "&hash=" + hash, jQuery("#form_add_profile").serialize(),function(response){ // добавляем объявление
                  response = response.split('##');
                  if(response[0]==="success"){ // успех
                     status.html('<div class="message">'+response[1]+'</div>');
                     document.location.href = URL_MY_CAB;
                  }
                  else{ // добавить не получилось
                    status.html(response[0]+response[1]);
                    if (response[2] != ''){
                        var fields_error = JSON.parse(response[2]);
                        for(i=0; i < fields_error.length; i++){
                            jQuery('#form_add_profile *[name="'+fields_error[i]+'"]').addClass('red');
                            if (fields_error[i] == 'cost_category'){
                                jQuery('div.cost_category').addClass('red');
                            }
                        }
                    }
                  }
                  jQuery('div.loading').fadeOut(200);
                });
            return false;    
        });

 
});