<?php
/**
 * @package		Joomla.Site
 * @subpackage	com_users
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 * @since		1.6
 */

defined('_JEXEC') or die;
$baseurl = JURI::base();

require_once 'geo/class/app/app.php';
require_once 'default.php';


$user =& JFactory::getUser();
$user_id = $user->get('id');
$name = $user->get('name');

$form_c = new FormConstructor();
$city = new Cities();
$category = new Ads_Category();
$user2 = $ads_user;

//print_r($user2->user_data);
$email = $user2->main_email;
//print_r($user2->error);
//echo 'xfg'.$email;
//echo $city->get_select_list_cities();

if ($user_id){
    $user->set('name', $user2->name);
    $name = $user->get('name');
 
?>
<link rel="stylesheet" href="/modules/mod_add_ads/tmpl/style.css" type="text/css" />
<link rel="stylesheet" href="/templates/compas_main/jquery.formstyler.css" type="text/css" />
<script src="/templates/compas_main/js/ajaxupload.3.5.js"></script>

<?php
Utils_Helpers::var_to_js(array(
    'user_id'            => $user_id,
    'hash'               => md5($user_id),
    'avatar'             => true,
    'max_photo_items'    => MAX_PHOTO_ITEMS,
    'DEFAULT_SELECT_VAL' => DEFAULT_SELECT_VAL,
    'SELECT_METRO'       => SELECT_METRO,
    'SELECT_RAION'       => SELECT_RAION,
    'URL_MY_CAB'         => URL_MY_CAB

));
?>
<script src="/geo/js/upload_file.js"></script>
<script src="/geo/js/autocomplete_brands.js"></script>
<script src="/templates/compas_person_cab/html/com_users/profile/edit_script.js"></script>

<div class="item-page">
    <h2>Профиль пользователя</h2>
</div>
<?php if (!$ads_user->is_user_register()){?>
<div style="color: blue;"><i>Заполните и сохраните профиль, чтобы продолжить работу с системой!</i></div>
<br/>
<?php }?>
<form id="form_add_profile" method="POST"> 
    <table width="100%">
        <tr>
            <td valign="top" width="320">
                <div class="img_cont">
                    <ul id="files">
                        <li class="main <?php if(!$ads_user->ad_avatar) {?>no_photo<?php }?>"><?php if($ads_user->ad_avatar) {?><img src="<?=$ads_user->ad_avatar?>" file="<?=$ads_user->ad_avatar?>" /><a href="#" class="del" onClick="_delete(jQuery(this)); return false;">Удалить</a><?php }?></li>
                    </ul>
                </div>
            </td>
            <td valign="top">
                <?=$company_name?>
                <?=$btn_load?>
                <div class="block_in info" style="color: #cc0000; font-size: 0.7em;"><i><?php echo sprintf(MESSAGE_WRONG_IMAGE_FORMAT, (MAX_FILE_AVATAR_SIZE/1000).' Мб')?></i></div>
                <?=$comment_field?>
                
                
            </td>
        </tr>
    </table>
    <table width="976">
        <tr>
            <td valign="top" width="490" align="right">

                <?=$fio?>
                <?=$phone_predstav?>
                <?=$phone_predstav2?>
                <?=$email_field?>
   
                
            </td>
            <td valign="top" align="right"  class="right block1">             
    
                <?=$city_field?>
                <?=$rayon_field?>
                <?=$metro_field?>
                <?=$adress?>
                <?=$email_field2?>
                <?=$site?>
                <?=$phone1?>
                <?=$phone2?>
                <?=$phone3?>
                <?=$regim?>
                <div style="position:relative; left:-148px;">
                <?=$cost_category?>
                </div>    
            </td>
        </tr>
        <tr>
            <td colspan="2">&nbsp;</td>
        </tr>

    </table>
    <br /><br />
    <!--
    <?=$list_razdel?>
    <?=$list_category?>
    <br style="clear: both;" />
    <?=$list_subcategory?>
    -->
    <br />
    <div class="block6">
        <div class="all_brands">
            <?=$brand?>
        </div>
        <br style="clear: both;" />
        <div class="info" style="color: #cc0000; font-size: 0.7em;"><i>Вы можете добавить до <?=MAX_BRAND_NUM?> брендов</i></div>
    </div>
    <div class="system_message" style="margin-top:10px;"><div></div></div>
    <br style="clear: both;" />
    <?=$submit_btn?>
    <!--<?=$delete_btn?>-->
    <input type="hidden" name="published" value="1" />
    <input type="hidden" name="user_id" value="<?=$user_id?>" />
    <input type="hidden" name="ad_avatar" value="" />
    <input type="hidden" name="ads_id" value="<?=$ads_id?>" />
    <input type="text" value="" validate_error="Выберите хотя бы одну ценовую категорию..." name="cost_category" style="display: none;" />
    <input type="text" value="" name="list_category" style="display: none;" />
    <input type="hidden" value="<?=DEFAULT_BRAND_VAL?>" id="default_brand_val" />
    <input type="text" value="" name="brands" style="display: none;" />
    <?php if($isAdmin){?>
    <input type="hidden" name="u_attr" value="<?=md5('is_admin')?>"/>
    <?php }?>
</form>


<?php
Utils_Helpers::var_to_js(array(
        'def_brand_val' => DEFAULT_BRAND_VAL,
        'max_num_brand' => MAX_BRAND_NUM

));
?>
<script src="/geo/js/brands.js"></script>

<?php } else{?>
Чтобы редактировать профиль нужно авторизоваться на сайте!
<?php }?>

<style type="text/css">
  input[name="go"]{
    margin-left: 450px;
  }
</style>