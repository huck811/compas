<?php
/**
 * @package                Joomla.Site
 * @subpackage	Templates.beez_20
 * @copyright        Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license                GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access.

defined('_JEXEC') or die;
jimport('joomla.filesystem.file');

JHtml::_('behavior.framework', true);

// get params
$doc = JFactory::getDocument();

$doc->addStyleSheet($this->baseurl.'/templates/system/css/system.css');
$doc->addStyleSheet($this->baseurl.'/templates/compas_main/css/style.css');
$doc->addStyleSheet($this->baseurl.'/templates/compas_main/css/style.css');
$doc->addStyleSheet($this->baseurl.'/templates/compas_main/css/coin-slider-styles.css');
$doc->addStyleSheet($this->baseurl.'/templates/compas_main/css/jquery-ui-1.10.4.custom.min.css');
$doc->addStyleSheet('//cdnjs.cloudflare.com/ajax/libs/normalize/3.0.0/normalize.min.css');

require_once 'geo/class/app/app.php';
require_once "geo/cache.php";
require_once "templates/compas_main/remove_mootols.php";

$doc->setTitle(MAIN_PAGE_TITLE);

$city = new cities(); 

?>
<!doctype html>
<html lang="ru">
<head>
	<jdoc:include type="head" />
    <!--[if IE]>
        <link href="<?=$this->baseurl?>/templates/compas_main/css/ie.css" rel="stylesheet" type="text/css">
    <![endif]--> 
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.1/jquery.min.js"></script>   
    <script src="<?=$this->baseurl?>/templates/compas_main/js/jquery.cookie.js"></script>  
    <script src="<?=$this->baseurl?>/templates/compas_main/js/jquery-ui-1.10.4.custom.min.js"></script> 

</head>
<body class="compas_personal_cabinet">
	<div class="wrapper">
		<!-- header -->
		<div class="wrapper-header">
			<div class="container">
				<div class="logo">
					<a href="<?=$this->baseurl?>"><img src="<?=$this->baseurl?>/templates/compas_main/img/logo.png" alt=""></a>
				</div>
				<div class="button-list">
					<?php include "templates/compas_main/city_list.phtml";?>
				</div>
				<?php include "templates/compas_main/auth_panel.phtml";?>
				<jdoc:include type="modules" name="position-1" />
                
			</div>
            <div class="line_h"></div>
		</div>
		<!-- midddle -->
		<div class="wrapper-midddle">
			<div class="container shadown">
				<div class="block-breadcr">
                    <jdoc:include type="modules" name="position-7" />
				</div>
				<div class="content-block">
										
                    <div id="center_block">
    					<div class="system_message"><jdoc:include type="message" /></div>
                        <jdoc:include type="modules" name="position-8" />
                        <jdoc:include type="component" />
					</div>
					
					
				
				<!-- right block-->
				<div class="right-banner">
                    <!-- vertical banner -->
                    <jdoc:include type="modules" name="position-9" />
					<!-- partners -->
					<jdoc:include type="modules" name="position-5" />
				</div>
                
                <br style="clear: both;" />
				<!-- bottom banners -->
				<div class="bottom-banners">
                        <jdoc:include type="modules" name="position-3" />	
				</div>
                </div>
                <br style="clear: both;" />
			</div>

	</div><!-- end wrapper -->
    <br style="clear: both;" />
	<!-- footer -->
	<div class="wrapper-footer">
		<div class="container">
			<ul class="menu-bottom">
                <jdoc:include type="modules" name="position-2" />
			</ul>
			<div class="counter">
                <jdoc:include type="modules" name="position-10" />
            </div>
			<div class="copyright">
				<jdoc:include type="modules" name="position-6" />
			</div>
		</div>
	</div>

	<!-- scripts -->
	<script src="<?=$this->baseurl?>/templates/compas_main/js/prefixfree.min.js"></script>
	<script src="<?=$this->baseurl?>/templates/compas_main/js/jquery.formstyler.min.js"></script> 
        <script src="<?=$this->baseurl?>/templates/compas_main/js/styler.js"></script>
        <div class="loading"><div></div></div>  
</body>
</html>