<?php
/**
 * @package		Joomla.Site
 * @subpackage	com_content
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;
$this->special = true;
JHtml::addIncludePath(JPATH_COMPONENT.'/helpers');
$last_catid = 0;
$i=0;
?>
<div class="blog<?php echo $this->pageclass_sfx;?>">
<div class="header-block-interview" style="width: 708px;"><?php echo $this->category->title;?></div>
<br/>
<?php $leadingcount=0 ; ?>
<?php if (!empty($this->lead_items)) : ?>
<div class="items-leading">
	<?php foreach ($this->lead_items as &$item) : ?>
		<div class="leading-<?php echo $leadingcount; ?><?php echo $item->state == 0 ? ' system-unpublished' : null; ?>">
			<?php
				$this->item = &$item;
                
				echo '<div class="itemspec">'.$this->loadTemplate('item').'</div>';
			?>
		</div>
		<?php
			$leadingcount++;
		?>
	<?php endforeach; ?>
</div>
<?php endif; ?>
<?php
	$introcount=(count($this->intro_items));
	$counter=0;
?>

<?php if (!empty($this->intro_items)) : ?>

	<?php foreach ($this->intro_items as $key => &$item) : ?>
	<?php
        if ($item->catid != $last_catid){
            if ($last_catid!=0)
                echo '</div>';
                
            if ($i%2 == 0){
                if ($i)
                    echo '<br style="clear: both;" />';
                $chet = '';
            }  
            else
                $chet = 'chet';   
            $i++;     
            echo '<div class="cont interv '.$chet.'" cid'.$item->catid.'>';
            echo '<div class="inter-head">'.$item->category_title.'</div><br/>';
            $last_catid = $item->catid;
            
        }
	
			$this->item = &$item;
			echo '<div class="itemspec">'.$this->loadTemplate('item').'</div>';
            
		?>
    <?php  
        
	endforeach; 
    if ($last_catid)
        echo '</div>';
    ?>


<?php endif; ?>
</div>
<br style="clear: both;" />