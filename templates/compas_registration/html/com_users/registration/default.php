<?php
/**
 * @package		Joomla.Site
 * @subpackage	com_users
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 * @since		1.6
 */

defined('_JEXEC') or die;

JHtml::_('behavior.keepalive');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
require_once 'config.php';
?>
<div class="registration<?php echo $this->pageclass_sfx?>" style="margin-left: 20px;">
<?php if ($this->params->get('show_page_heading')) : ?>
	<h1><?php echo $this->escape($this->params->get('page_heading')); ?></h1>
<?php endif; ?>
<script type="text/javascript">
    <?php include "modules/mod_add_ads/tmpl/validate.js";?>
</script>

	<form id="member-registration" action="<?php echo JRoute::_('index.php?option=com_users&task=registration.register'); ?>" method="post" class="form-validate1">
<?php foreach ($this->form->getFieldsets() as $fieldset): // Iterate through the form fieldsets and display each one.?>
	<?php $fields = $this->form->getFieldset($fieldset->name);?>
	<?php if (count($fields)):?>
		<fieldset>
		<?php if (isset($fieldset->label)):// If the fieldset has a label set, display it as the legend.
		?>
			<legend><?php echo JText::_($fieldset->label);?></legend>
		<?php endif;?>
			<dl>
		<?php foreach($fields as $field):// Iterate through the fields in the set and display them.?>
			<?php if ($field->hidden):// If the field is hidden, just display the input.?>
				<?php echo $field->input;?>
			<?php else:?>
				<dt>
					<?php echo $field->label; ?>
					<?php if (!$field->required && $field->type!='Spacer'): ?>
						<span class="optional"><?php echo JText::_('COM_USERS_OPTIONAL'); ?></span>
					<?php endif; ?>
				</dt>
				<dd><?php 
                if ($field->id!='jform_password1' && $field->id!='jform_password2' && $field->id!='jform_email1'){
                echo ($field->type!='Spacer') ? $field->input : "&#160;"; }?>
                <?php if ($field->id=='jform_password1') $pass1 = $field->input;?>
                <?php if ($field->id=='jform_password2') $pass2 = $field->input;?>
                <?php if ($field->id=='jform_email1') $email1 = $field->input;?>
                </dd>
			<?php endif;?>
		<?php endforeach;?>
			</dl>
		</fieldset>
	<?php endif;?>
<?php endforeach;?>
            <i>Зарегистрируйтесь в нашей системе, чтобы добавить карточку магазина и создать сайт Вашей компании.</i>    
        <div style="font: bold italic 14px 'Trebuchet'; margin: 20px 0 10px 0;">Введите данные для входа в систему</div>
        <table>
            <tr>
                <td align="right">E-mail:</td><td><?=$email1;?></td>
            </tr>
            <tr>
                <td align="right">Пароль:</td><td><?=$pass1;?></td>
            </tr>
            <tr>
                <td align="right">Подтверждение пароля:</td><td><?=$pass2;?></td>
            </tr>
            <tr>
                <td align="right"></td>
                <td>
                    <div style="margin-top: 20px;">
                        <input type="checkbox" validate_error="Ознакомтесь с соглашением и примите условия..." /> Я прочитал и согласен с условиями <a href="index.php?Itemid=202">соглашения об использовании системы</a><br />
                        <input type="checkbox" validate_error="Ознакомтесь с договором и примите условия..." /> Я прочитал и согласен с <a href="index.php?Itemid=203">договором о конфиденциальности</a><br />
                        <!--<input type="checkbox" checked="checked" id="news-mail" /> Подписаться на новости сервиса<br />-->
                        <div style="margin-top: 20px;"> 
                            
                			<button type="submit" class="validate"><?php echo JText::_('JREGISTER');?></button>
                		    <input type="hidden" name="option" value="com_users" />
                			<input type="hidden" name="task" value="registration.register" />
                			<?php echo JHtml::_('form.token');?>
                		</div>
                    </div>
                </td>
            </tr>
        </table>

		
	</form>
</div>

<style type="text/css">
    #jform_email2, #jform_username,#jform_name { display: none;}
    #jform_password2-lbl, #jform_password1-lbl, #jform_email2-lbl, #jform_email1-lbl, #jform_username-lbl,#jform_name-lbl{ display: none;}
    fieldset {display: none;}
    #content, #content input[type=text], #content input[type=password]{ font: 12px "Trebuchet"; }
    #content input[type=text], #content input[type=password]{ padding: 0 10px; border: 1px solid #e0e0e0;}
    #content a{ font: italic 12px "Trebuchet"; text-decoration: underline; color: #000;}
    #content a:hover{ text-decoration: none;}
    input.red{ border:1px solid red;}
</style>

<script>
jQuery('#jform_email1').attr('validate_error','Не верно задан E-mail...');
jQuery('#jform_email1').attr('is_email',1);
jQuery('#jform_password1').attr('validate_error','Укажите пароль...');
jQuery('#jform_password2').attr('validate_error','Подтвердите пароль...');
jQuery('#jform_profile_phone').val(1); // Устанавливаем подписку на новости
jQuery('#news-mail').click(function(){
    if (jQuery(this).is(':checked'))
        jQuery('#jform_profile_phone').val(1);
    else
        jQuery('#jform_profile_phone').val(0);
        
});    


jQuery('#member-registration').submit(function(){
    var status=jQuery('#system-message-container');
    jQuery('#jform_email2').val(jQuery('#jform_email1').val());
    name = jQuery('#jform_email1').val();
    if (jQuery('#jform_email1').val()!='')
        {jQuery('#jform_username').val(name); jQuery('#jform_name').val(name);}
    else    
        {jQuery('#jform_username').val('dfdfbjk1'); jQuery('#jform_name').val('dfdfbjk1knl');}
    
    
    
    
    
    
    error = '';
    if (<?=VALIDATE_FORM;?>){
        error = validate('#member-registration'); 
        if (jQuery('#jform_password1').val()!=jQuery('#jform_password2').val())  
        error +='Пароли не совпадают...<br/>';            
    }
    
    if (error!=''){
        status.html(error);
        return false;
    }
});
</script>
