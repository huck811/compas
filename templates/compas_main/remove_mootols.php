<?php
$document = JFactory::getDocument();
unset($this->_scripts[$this->baseurl.'/media/system/js/mootools-core.js'], 
	$this->_scripts[$this->baseurl.'/media/system/js/mootools-more.js'],
	$this->_scripts[$this->baseurl.'/media/system/js/core.js'],
	$this->_scripts[$this->baseurl.'/media/system/js/caption.js']);
if (isset($this->_script['text/javascript']))
{
    $this->_script['text/javascript'] = preg_replace('%window\.addEvent\(\'load\',\s*function\(\)\s*{\s*new\s*JCaption\(\'img.caption\'\);\s*}\);\s*%', '', $this->_script['text/javascript']);
    if (empty($this->_script['text/javascript']))
        unset($this->_script['text/javascript']);
    
     preg_match("/window\.addEvent\('domready',\sfunction\(\)\s\{[\s]*[$]+\('\.hasTip'\)\.each\(function\(el\)\s\{[\s]*var\stitle\s=\sel\.get\('title'\);[\s]*if\s\(title\)\s\{[\s]*var\sparts\s=\stitle\.split\('::', 2\);[\s]*el\.store\('tip:title',\sparts\[0\]\);[\s]*el\.store\('tip:text',\sparts\[1\]\);[\s]*\}[\s]*\}\);[\s]*var\sJTooltips\s=\snew\sTips\([$]+\('\.hasTip'\),\s\{\smaxTitleChars:\s50,\sfixed:\sfalse\}\);[\s]*\}\);/iU", $this->_script['text/javascript'],$match2);
        if (isset($match2[0])) {
            $this->_script['text/javascript']=str_replace($match2[0],'',$this->_script['text/javascript']);
        }    
}    
//$this->_scripts['http://yandex.st/mootools/1.3.1/mootools.min.js'] = array('mime' => 'text/javascript', 'deffer' => '', 'async' => '');
//[mime] => text/javascript [defer] => [async] =>
?>    