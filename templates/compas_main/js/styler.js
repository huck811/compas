function refresh_fields(){

                  // Переносим информацию из Label поля в атрибут placeholder для формы обратной связи
                  jQuery('#adminForm_1 input[type=text], #adminForm_1 textarea').each(function(){
                     id = jQuery(this).attr('id');
                     if (jQuery('label[for="'+id+'"]').length){
                        item = jQuery('label[for="'+id+'"]').html();
                        
                        if(jQuery('label[for="'+id+'"]').parent().parent().find('label.required_field').length)
                            item += ' *';
                        jQuery(this).attr('placeholder',item);
                     }  
                  });
                  
                  jQuery('#captcha-code').attr('placeholder','Число с картинки / Captcha');
                  
                  // Активируем стилизатор
                  jQuery('select.header-select, .filter select, input.filter-brand, #adminForm_1 input, #form_company_site input, #form_company_site textarea').styler(); 

 } 
 
 refresh_fields();