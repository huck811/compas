<?php
      include "memcache.class.php";
      // Чтение кэша
      function readCache($filename, $expiry) {
      //echo Cache::get('key');  
      $_md5_fn = md5($filename);
      if (!ENABLE_CACHE_FUNCTION)
        return FALSE;
      
      //Cache::singleton();
      
      if (/*!Cache::$is_memcache*/1){
          if (file_exists(CACHE_FOLDER . $filename)) {
            if ((time() - $expiry) > filemtime(CACHE_FOLDER . $filename))
              return FALSE;
            $cache = file(CACHE_FOLDER . $filename);
            //$_SESSION[$_md5_fn] = $cache;
            return unserialize(gzuncompress(implode('', $cache)));
            //return implode('', $cache);
          }
      }else{
        $cache = Cache::get(md5(CACHE_FOLDER . $filename));
        return $cache ? $cache : false;
      }
      
      return FALSE;
      }
  
    //Запись кэша
    function writeCache($content, $filename) {
      if (!ENABLE_CACHE_FUNCTION)
        return FALSE;  
      
      
      //Cache::singleton();

      if (/*!Cache::$is_memcache*/1){
          $content = serialize($content);  
          $content = gzcompress(($content));
          $fp = fopen(CACHE_FOLDER . $filename, 'w');
          fwrite($fp, $content);
          fclose($fp);
      }
      else{
          Cache::set(md5(CACHE_FOLDER . $filename), $content, MEMCACHE_COMPRESSED, CACHE_TIME);
      }
    }
?>