<?php
require_once '../class/app/app.php';
require_once 'cron_config.php';

class user_activity{
    private $db;
    private $qb;
    
    function __construct(){
        $this->qb = new Db_ChainQueryBuilder();
        $this->db = new db_connect(SERVER, DB_NAME, DB_USER, DB_PASS);
    }
     
    
    function check($live_time, $period){
        
        $first_data = MyDateTime::GetDeltaTime('', $live_time - $period);
        $this->qb->set_exclusions(
        array('u.lastvisitDate', 'u.registerDate', 'u.id', 'um.user_id', 'um.group_id', 'u.block')
        );
        $q = $this->qb
                ->select(array('u.lastvisitDate, u.id'))
                ->from('#__users AS u, #__user_usergroup_map AS um')
                ->where('u.lastvisitDate', '<=', $first_data)
                ->andWhere('u.registerDate', '<=', $first_data)
                ->andWhere('u.id', '=', 'um.user_id')
                ->andWhere('um.group_id', '=', 2)
                ->andWhere('u.block', '=', 0)
                ->build();
        
        //exit($q);
        $res = $this->db->fetchAll($q); 
         
        
        //echo '<pre>';
        //print_r($res);
        if ($period)
            Message_Message::instance()->set_user_mail_message(LOST_ACTIVITY,$period,$res);
        else{
            
            return $res; 
        }       
        
    } 
    
    function del_user($user){
        $user_id = $user['id'];
        //удаляем пользователя
        $q = $this->qb
                    ->deleteFrom('#__users')
                    ->where('id', '=', $user_id)
                    ->limit(1)
                    ->build();
        try{   
            $this->db->beginTransaction(); // начало транзакции
            $this->db->execute($q, true);
             
            $this->del_info($user_id);
            $mess = 'Пользователь '.$user_id.' и все его данные удалены.';
            System_Log::set($mess, LOG_FILE_CRON, false, LOG_CRON_ENABLE);  
            $this->db->commit(); 
        }
        catch(Exception $e){
            $this->db->rollBack(); // откат
            $mess = "Ошибка (не возможно удалить пользователя): " . $e->getMessage();
            System_Log::set($mess, LOG_FILE_CRON_ERROR, ADMIN_EMAIL_FOR_LOG, LOG_CRON_ENABLE);
        } 
    }
    
    function block_user($user){
        $user_id = $user['id'];
        //блокируем пользователя
        $q = $this->qb
                    ->update('#__users')
                    ->set(array('block' => 1))
                    ->where('id', '=', $user_id)
                    ->limit(1)
                    ->build();
        try{   
            $this->db->execute($q, true);
            $mess = 'Пользователь '.$user_id.' заблокирован.';
            System_Log::set($mess, LOG_FILE_CRON, false, LOG_CRON_ENABLE);  
        }
        catch(Exception $e){
            $mess = "Ошибка (не возможно заблокировать пользователя): " . $e->getMessage();
            System_Log::set($mess, LOG_FILE_CRON_ERROR, ADMIN_EMAIL_FOR_LOG, LOG_CRON_ENABLE);
        } 
    }
    
    function del_info($user_id){
        //удаляем все записи пользователя и все файлы...
        $q = $this->qb
                    ->deleteFrom('#__user_mail_history')
                    ->where('id_user', '=', $user_id)
                    ->build();
        $this->db->execute($q, true);
        
        $q = $this->qb
                    ->deleteFrom('#__adsmanager_profile')
                    ->where('userid', '=', $user_id)
                    ->limit(1)
                    ->build();
        $this->db->execute($q, true);
        
        $q = $this->qb
                    ->select('id')
                    ->from('#__adsmanager_ads')
                    ->where('userid', '=', $user_id)
                    ->build();
        $ads = $this->db->fetchAll($q);
        
        /*********Удаляем все объявления пользователя и записи из других таблиц********/
            
            $this->qb
                    ->deleteFrom('#__adsmanager_adcat');
            $i=0;
            foreach ($ads as $a){
                if (!$i)
                    $this->qb        
                            ->where('adid', '=', $a['id']);
                else            
                    $this->qb        
                           ->andWhere('adid', '=', $a['id']);            
                $i++;        
            }
                    
            $q = $this->qb        
                    ->build(); 
            $this->db->execute($q, true);
            
            //------------
            $this->qb
                    ->deleteFrom('#__adsmanager_select');
            $i=0;
            foreach ($ads as $a){
                if (!$i)
                    $this->qb        
                            ->where('ads_id', '=', $a['id']);
                else            
                    $this->qb        
                           ->andWhere('ads_id', '=', $a['id']);            
                $i++;        
            }
                    
            $q = $this->qb        
                    ->build(); 
            $this->db->execute($q, true);
            
            //------------
            $this->qb
                    ->deleteFrom('#__adsmanager_top');
            $i=0;
            foreach ($ads as $a){
                if (!$i)
                    $this->qb        
                            ->where('ads_id', '=', $a['id']);
                else            
                    $this->qb        
                           ->andWhere('ads_id', '=', $a['id']);            
                $i++;        
            }
                    
            $q = $this->qb        
                    ->build(); 
            $this->db->execute($q, true);
            
            //------------
            $this->qb
                    ->deleteFrom('#__adsmanager_top');
            $i=0;
            foreach ($ads as $a){
                if (!$i)
                    $this->qb        
                            ->where('ads_id', '=', $a['id']);
                else            
                    $this->qb        
                           ->andWhere('ads_id', '=', $a['id']);            
                $i++;        
            }
                    
            $q = $this->qb        
                    ->build(); 
            $this->db->execute($q, true);
            /*****************/
        
        $q = $this->qb
                    ->deleteFrom('#__adsmanager_ads')
                    ->where('userid', '=', $user_id)
                    ->build();
        $this->db->execute($q, true);
        
        $q = $this->qb
                    ->deleteFrom('#__user_profiles')
                    ->where('user_id', '=', $user_id)
                    ->build();
        $this->db->execute($q, true);
        
        $q = $this->qb
                    ->deleteFrom('#__user_schet')
                    ->where('user_id', '=', $user_id)
                    ->limit(1)
                    ->build();
        $this->db->execute($q, true);
        
        $q = $this->qb
                    ->deleteFrom('#__user_payment_history')
                    ->where('user_id', '=', $user_id)
                    ->build();
        $this->db->execute($q, true);
    }
    
    function RemoveDir( $path ) {
        // если путь существует и это папка
        if ( file_exists( $path ) AND is_dir( $path ) ) {
            // открываем папку
            $dir = opendir($path);
            while ( false !== ( $element = readdir( $dir ) ) ) {
                // удаляем только содержимое папки
                if ( $element != '.' AND $element != '..' )  {
                    $tmp = $path . '/' . $element;
                    chmod( $tmp, 0777 );
                    // если елемент является папкой, то
                    // удаляем его используя нашу функцию RDir
                    if ( is_dir( $tmp ) ) {
                        $this->RemoveDir( $tmp );
                    // если елемент является файлом, то удаляем файл
                    } elseif ( file_exists( $tmp ) ) {
                        unlink( $tmp );
                    }
                }
            }
            // закрываем папку
            closedir($dir);
            // удаляем саму папку
            if ( file_exists( $path ) ) {
                rmdir( $path );
            }
        }
    }
} 

$ua = new user_activity();

if (FIRST_WARNING_BLOCK_USER)
    $ua->check(LIVE_TIME_USER, FIRST_WARNING_BLOCK_USER);
    
if (SECOND_WARNING_BLOCK_USER)  
    $ua->check(LIVE_TIME_USER, SECOND_WARNING_BLOCK_USER);
    
if (THIRD_WARNING_BLOCK_USER)    
    $ua->check(LIVE_TIME_USER, THIRD_WARNING_BLOCK_USER);

$user_lost_active = $ua->check(LIVE_TIME_USER, 0);

if (count($user_lost_active))
    array_map(array($ua,"block_user"), $user_lost_active);

print_r($user_lost_active);


$user_delete = $ua->check(TIME_USER_DELETE, 0);

/*if (count($user_delete))
    array_map(array($ua,"del_user"), $user_delete);*/

include "set_mail_queue.php";

$smq = new set_mail_queue();

$smq->mail_lost_active();