<?php
require_once '../class/app/app.php';
require_once 'cron_config.php';

class ads_top_check{
    private $db;
    private $qb;
    
    function __construct(){
        $this->qb = new Db_ChainQueryBuilder();
        $this->db = new db_connect(SERVER, DB_NAME, DB_USER, DB_PASS);
        $this->qb->set_exclusions(
        array('at.ads_id', 'am.id', 'u.id', 'am.userid', 'u.block', 'at.date_top')
        );
    }
    
    function check($period, $relation = true){
        
        //$first_data = MyDateTime::GetDeltaTime('', $live_time - $period);

        if ($relation)
            $q = $this->qb
                    ->select(array('at.date_top, u.id, am.id AS adsid'))
                    ->from('#__adsmanager_top AS at, #__adsmanager_ads AS am, #__users AS u')
                    ->where('at.date_top + INTERVAL (at.time - '.$period.') DAY', '<=', 'NOW()')
                    ->andWhere('am.id', '=', 'at.ads_id')
                    ->andWhere('u.id', '=', 'am.userid')
                    ->andWhere('u.block', '=', 0)
                    ->andWhere('am.published', '<>', Ads_AdsManage::DELETE_STATUS)
                    ->build();
        else
            $q = $this->qb
                    ->select(array('at.date_top, at.ads_id as adsid'))
                    ->from('#__adsmanager_top AS at')
                    ->where('at.date_top + INTERVAL (at.time - '.$period.') DAY', '<=', 'NOW()')
                    ->build();
                                
        $res = $this->db->fetchAll($q); 
        /*echo $q.'<br/><br/>'; 
        echo '<pre>';
        print_r($res);
        echo '</pre>';*/
        if ($period)
            Message_Message::instance()->set_user_mail_message(TOP_END,$period,$res, 'ads_top');
        else{
            
            return $res; 
        }       
        
    } 
    
    function clear_top($ads){
        $id = $ads['adsid'];
        
        //удаляем объявление из топа
        $q = $this->qb
                    ->deleteFrom('#__adsmanager_top')
                    ->where('ads_id', '=', $id)
                    ->build();
                    
        //Очищаем историю сообщений
        $q1 = $this->qb
                    ->deleteFrom('#__user_mail_history')
                    ->where('note', '=', $id)
                    ->andWhere('type_email', 'LIKE', TOP_END.'%')
                    ->build();            
        try{   
            $this->db->execute($q,  false);
            $this->db->execute($q1, false);
            $mess = 'Обявление '.$id.' снято из ТОПа.';
            System_Log::set($mess, LOG_FILE_CRON, false, LOG_CRON_ENABLE); 
        }
        catch(Exception $e){
            $mess = "Ошибка (не возможно удалить объявление из ТОПа): " . $e->getMessage();
            System_Log::set($mess, LOG_FILE_CRON_ERROR, ADMIN_EMAIL_FOR_LOG, LOG_CRON_ENABLE); 
        } 
    }
    
 
} 

$ua = new ads_top_check();

if (FIRST_WARNING_TOP)
    $ua->check(FIRST_WARNING_TOP);
    
if (SECOND_WARNING_TOP)  
    $ua->check(SECOND_WARNING_TOP);
    
if (THIRD_WARNING_TOP)    
    $ua->check(THIRD_WARNING_TOP);

$ads_top_is_over = $ua->check(0, false);

//print_r($ads_top_is_over);

array_map(array($ua,"clear_top"), $ads_top_is_over);

include "check_ads_select.php";
include "set_mail_queue.php";

$smq = new set_mail_queue();

$smq->mail_end_activity_ads(TOP_END);
$smq->mail_end_activity_ads(SELECT_END);