<?php
require_once '../class/app/app.php';
require_once 'cron_config.php';

class set_mail_queue{
    private $db;
    private $qb;
    private $def_fio;
    
    function __construct(){
        $this->qb = new Db_ChainQueryBuilder();
        $this->db = new db_connect(SERVER, DB_NAME, DB_USER, DB_PASS);
        $this->def_fio = "Уважаемый пользователь портала spt-portal.ru";
    }
    
    function days($item){
        if ($item == 1){
            return 'день';
        }
        else if($item >= 2 && $item < 5){
            return 'дня';
        }
        else
            return 'дней';
    }
    
    function mail_lost_active(){

         
        $res = Message_Message::get_user_mail_message(LOST_ACTIVITY);
        //echo '<pre>';
        //print_r($res); 
        
        
        
        foreach ($res as $r){
            
            // заменяем некоторые поля в шаблоне
            $period    = str_replace(LOST_ACTIVITY,'',$r['type_email']);
            
            $array_replace = array(
                'username' => $r['fio'] ? $r['fio'] : $this->def_fio,
                'period' => $period.' '.$this->days($period)
            );
            
            // вытаскиваем шаблон
            $template = Message_Message::get_template("cron/user/".LOST_ACTIVITY, $array_replace);

            $this->_turn_mail($r, SUBJECT_MAIL_1, $template);      
         
        }
        
    } 
    
    function mail_end_activity_ads($type_action = TOP_END){

        $res = Message_Message::get_user_mail_message($type_action); 
        
        foreach ($res as $r){
            
            // заменяем некоторые поля в шаблоне
            /*$url = 'http://'.$_SERVER['HTTP_HOST'].$sef->set_ads($r['note'])->get_sef_url();
            $url = '<a href="'.$url.'">'.$url.'</a>';*/
            $period    = str_replace($type_action,'',$r['type_email']);
            $ads_manage = new Ads_AdsManage();
            
            if ($ads_manage->is_ads_exsist(false, '', $r['note'])){
                //$ads_info = $this->_get_template_ads("ads_template.phtml", $r['note']);
               
                $ads_info = $ads_manage->get_template_ads($r['note']);
                
                $dates = $ads_manage->get_dates($r['note']);
                
                if ($type_action == TOP_END)
                    $date_final = Utils_Helpers::date_format($ads_manage->date_final_top($dates));
                else if($type_action == SELECT_END)
                    $date_final = Utils_Helpers::date_format($ads_manage->date_final_select($dates));    
                
                $array_replace = array(
                    'username' => $r['fio'] ? $r['fio'] : $this->def_fio,
                    'days' => $date_final,
                    'description' => $ads_info
                );
                
                // вытаскиваем шаблон
                $template = Message_Message::get_template("cron/user/".$type_action, $array_replace);            
         
                $this->_turn_mail($r, SUBJECT_MAIL_1, $template); 
                
                /*****testing cron*****/
                $ads_info .='<hr/>';
                $ads_info .='ads_id = '.$r['note'].'<br/>';
                $ads_info .='dates :'.print_r($dates, true);
                $ads_info .='<hr/>';
                $array_replace = array(
                    'username' => $r['fio'] ? $r['fio'] : $this->def_fio,
                    'days' => $date_final,
                    'description' => $ads_info
                );
                
                // вытаскиваем шаблон
                $template = Message_Message::get_template("cron/user/".$type_action, $array_replace); 
                
                Mail_Mailouts::add( 'Портал спецтехники ', 'admin@spt-portal.ru', 'Админу', 'vovchik81@bk.ru', 'тест крона', $template);
                /**********************/ 
            }    
       
        }
        
    } 
    
    
    private function _turn_mail($data, $subject, $message){
        
        $fio = $data['fio'] ? $data['fio'] : '';
        $to_email = $data['email'];
        $id = $data['id'];
        
        Mail_Mailouts::add( 'Портал спецтехники ', 'admin@spt-portal.ru', $fio, $to_email, $subject, $message);
            
        // меняем запись в таблице - письмо в очереди на отправку
        Message_Message::set_sended_mail_message($id);
    }
    

} 