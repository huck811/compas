<?php

/**
 * The class makes it easier to work with memcached servers and provides hints in the IDE like Zend Studio
 * @author Grigori Kochanov http://www.grik.net/
 * @version 1
 *
 */
class Cache {
/**
 * Resources of the opend memcached connections
 * @var array [memcache objects]
 */
public static $mc_server = '127.0.0.1';
/**
 * Quantity of servers used
 * @var int
 */
public static $is_memcache;
public static $_memcache;

static $instance;

/**
 * Singleton to call from all other functions
 */
public static function singleton(){
    //Write here where from to get the servers list from, like 
    // global $servers
    
    $server = self::$mc_server;
    if (null === self::$instance) {
        // ������� ����� ���������
        self::$instance = new self($server);
    }
    // ���������� ��������� ��� ������������ ���������
    return self::$instance;
}

/**
 * Accepts the 2-d array with details of memcached servers
 *
 * @param array $servers
 */
private function __construct($server){
    self::$_memcache = new Memcache;    
    self::$is_memcache = self::$_memcache->connect($server, 11211);
}
private function __clone(){
    }



/**
 * Returns the value stored in the memory by it's key
 *
 * @param string $key
 * @return mix
 */
static function get($key) {
    return self::singleton()->get_memcache()->get($key);
}

/**
 * Store the value in the memcache memory (overwrite if key exists)
 *
 * @param string $key
 * @param mix $var
 * @param bool $compress
 * @param int $expire (seconds before item expires)
 * @return bool
 */
static function set($key, $var, $compress=0, $expire=0) {
    return self::singleton()->get_memcache()->set($key, $var, null, $expire);
}

static public function get_memcache(){
    return self::$_memcache;
}
/**
 * Set the value in memcache if the value does not exist; returns FALSE if value exists
 *
 * @param sting $key
 * @param mix $var
 * @param bool $compress
 * @param int $expire
 * @return bool
 */
static function add($key, $var, $compress=0, $expire=0) {
    return self::singleton()->get_memcache()->add($key, $var, null, $expire);
}

/**
 * Replace an existing value
 *
 * @param string $key
 * @param mix $var
 * @param bool $compress
 * @param int $expire
 * @return bool
 */
static function replace($key, $var, $compress=0, $expire=0) {
    return self::singleton()->get_memcache()->replace($key, $var, null, $expire);
}
/**
 * Delete a record or set a timeout
 *
 * @param string $key
 * @param int $timeout
 * @return bool
 */
static function delete($key, $timeout=0) {
    return self::singleton()->get_memcache()->delete($key, $timeout);
}
/**
 * Increment an existing integer value
 *
 * @param string $key
 * @param mix $value
 * @return bool
 */
static function increment($key, $value=1) {
    return self::singleton()->get_memcache()->increment($key, $value);
}

/**
 * Decrement an existing value
 *
 * @param string $key
 * @param mix $value
 * @return bool
 */
static function decrement($key, $value=1) {
    returnself::singleton()->get_memcache()->decrement($key, $value);
}


//class end
}

?>