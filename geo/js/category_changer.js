function make(){
    // общая логика для 0-1 уровней
    jQuery('input[tree_id][level="1"], input[tree_id][level="2"]').attr('disabled','disabled');

    jQuery('input[tree_id][level="0"]:checked, input[tree_id][level="1"]:checked').each(function(){
        parent = jQuery(this).val();
        level = 1*jQuery(this).attr('level');
        //alert('input[tree_id][level="1"][parent="'+parent+'"]');
        jQuery('input[tree_id][level="'+(level+1)+'"][parent="'+parent+'"]').removeAttr('disabled');
    });


    //логика для 1го уровня
    jQuery('input[tree_id][level="1"]').each(function(){
        id = jQuery(this).attr('id');
        _val = jQuery(this).val();
        if (jQuery(this).attr('disabled') == 'disabled'){
            jQuery('span.wrap-checkbox[id="'+id+'-wrapp"]').attr('disabled','disabled');
            jQuery('div.cont'+_val).addClass('hide');
        }
        else{
            jQuery('span.wrap-checkbox[id="'+id+'-wrapp"]').removeAttr('disabled');
            jQuery('div.cont'+_val).removeClass('hide');
        }
    });


    jQuery('div.cont.hide span.wrap-checkbox[checked] input').removeAttr('checked').prop('checked',false);
    jQuery('div.cont.hide span.wrap-checkbox[checked]').removeAttr('checked');

    //логика для 2го уровня
    jQuery('input[tree_id][level="2"]').each(function(){
        id = jQuery(this).attr('id');
        parent = jQuery(this).attr('parent');
        _val = jQuery(this).val();
        if (jQuery(this).attr('disabled') == 'disabled'){
            jQuery('span.wrap-checkbox[id="'+id+'-wrapp"]').attr('disabled','disabled');
            jQuery('.block_in[level="2"].id'+parent).addClass('hide');
        }
        else{
            jQuery('span.wrap-checkbox[id="'+id+'-wrapp"]').removeAttr('disabled');
            //alert('.block_in[level="2"].id'+parent);
            jQuery('.block_in[level="2"].id'+parent).removeClass('hide');
        }

        jQuery('.block_in.hide span.wrap-checkbox[checked] input').removeAttr('checked').prop('checked',false);
        jQuery('.block_in.hide span.wrap-checkbox[checked]').removeAttr('checked');
    });

}


jQuery(document).ready(function(){
    jQuery('.wrap-checkbox').live('click',function(){
        make();
        if (jQuery('input',this).attr('level')==0)
            make();
    });

    jQuery('input[level="1"], input[level="2"]').attr('disabled','disabled');
    make();

});