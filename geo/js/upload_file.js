jQuery(function(){
    var btnUpload=jQuery('#upload');
    var status=jQuery('#system-message-container');
    var action = '/modules/mod_add_ads/upload/upload-file.php?upload=1&user_id='+user_id+'&hash='+hash;
    name = "images";

    // for avatar and ads cart
    OnSuccess = function (response){
        file_thumb = response[2];
        file = response[1]+'/'+response[3];
        jQuery('input[name="'+name+'"]').val(file);
        jQuery('.img_cont li.main').html('<img src="'+file_thumb.toLowerCase()+'" file="'+file.toLowerCase()+'" width="300" /><a href="#" class="del" onClick="_delete(jQuery(this)); return false;">Удалить</a>');
    }

    if(typeof news !== 'undefined'){// for news
        btnUpload = jQuery('#upload_news');
        action += '&news=1';


        OnSuccess = function (response){
            file = response[2];
            jQuery('ul.photo_list').html(jQuery('ul.photo_list').html()+'<li><img width="170" src="'+file+'"/><br/><a href="#" onClick="return _delete(jQuery(this));" class="del">Удалить</a></li>');
            jQuery('ul.photo_list li:last img').attr('file', response[3]);
            jQuery('.img_cont li.main').html('<img src="' + file.toLowerCase() + '" width="300" /><a href="#" class="del" onClick="_delete(jQuery(this)); return false;">Удалить</a>');
        }
    }
    else if (typeof avatar !== 'undefined'){ // for avatar
        action += '&avatar=1';
        name = "ad_avatar";
    }
    else if (typeof company_site !== 'undefined'){// for company site

        action += '&company_site=1';


        OnSuccess = function (response){
            file = response[2];
            jQuery('input[name="images"]').val(file);
            im = file.split('/');
            jQuery('input[name="images_view"]').val(im[im.length-1]);
            jQuery('.show_img img').attr('src', file);
        }
    }

    new AjaxUpload(btnUpload, {
        action: action,
        name: 'uploadfile',
        onSubmit: function(file, ext){
            if (! (ext && /^(jpg|png|jpeg)$/.test(ext))){
                // extension is not allowed
                status.html('<div class="error">не верный формат, загружать можно только фото в формате JPG, PNG</div>');
                return false;
            }

            if (num_photo() >= max_photo_items){
                if (name == 'images' && jQuery('input[name=ads_id]').val())
                    _delete(jQuery('#files li:first img'));
                else
                    jQuery('a.del').click();
            }
            var is_photo = false;


            if (is_photo){
                status.text('Такое изображение уже существует...');
                return false;
            }

            jQuery('div.loading').fadeIn(200);
            status.text('Загрузка...');
        },
        onComplete: function(file, response){
            //On completion clear the status

            status.text('');
            jQuery('div.loading').fadeOut(200);
            response = response.split('##');
            //Add uploaded file to list

            if(response[0]==="success"){
                OnSuccess(response);
            } else{
                status.html(response[0]+response[1]);
            }
        }
    });

});

function getRandomInt(min, max)
{
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function num_photo(){
    return jQuery('#files li').length - jQuery('#files li.no_photo').length;
}

function _delete(obj){ // Удаляем картинку по клику.
    var status=jQuery('#system-message-container');
    var li = obj.parent(); // запоминаем контейнер картинки
    src = li.find('img:first').attr('src'); // получаем путь к файлу
    var first_li_small = jQuery('#files li.small img:first');
    jQuery.get("/modules/mod_add_ads/upload/delete-file.php?img="+src+'&rand='+getRandomInt(1,10000)+'&user_id='+user_id+'&hash='+hash, function(response){ // запрос на удаление
        response = response.split('##');
        if(response[0]==="success"){ // успех
            li.html(''); // очищаем содержимое контейнера с фото
            li.addClass('no_photo');
            //restruct_img();
        }
        else{ // удалить не получилось
            status.html('<div class="inner">'+response[0]+response[1]+'</div>');
        }

    });

    return false;
}
