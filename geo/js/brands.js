// автозаполнение полей бренда
if (jQuery('.all_brands .item input[type="text"]').val()=='')
    jQuery('.all_brands .item input[type="text"]').val(def_brand_val);

jQuery('.all_brands .item input[type="text"]').live('focus',function(){
    if (jQuery(this).val() == def_brand_val)
        jQuery(this).val('');
}).live('focusout',function(){
    if (jQuery(this).val()=='')
        jQuery(this).val(def_brand_val);
});

//добавляем новое поле бренда
jQuery('.all_brands .item .add').live('click',function(){

    if (jQuery('.all_brands .item').length >= max_num_brand)
        return false;

    if (jQuery(this).parent().find('.block_in input').val() == def_brand_val)
        return false;

    jQuery('.all_brands').append('<div class="item">'+jQuery('.all_brands .item:first').clone().html()+'</div>');
    jQuery('.all_brands .item:last input[type="text"]').val('');

    jQuery('.all_brands .item input[type="text"]').each(function(){
        if (jQuery(this).val()=='')
            jQuery(this).val(def_brand_val);
    });

    return false;
 });

//удаляем поле бренда
jQuery('.all_brands .item .delete').live('click',function(){
    jQuery(this).parent().remove();
    return false;
});
