jQuery(".all_brands input[type=text]").live("focus", function () {
    var cache = {},
        lastXhr;
    jQuery(this).autocomplete({
        minLength: 3,
        source: function( request, response ) {
            var term = request.term;

            if ( term in cache ) {
                response( cache[ term ] );
                return;
            }

            lastXhr = jQuery.get( "/modules/mod_add_ads/upload/get_brand_dynamic.php?callback=?", request, function( data, status, xhr ) {
                cache[ term ] = data;
                if ( xhr === lastXhr ) {
                    response( data );
                }

            });

        },
        select: function( event, ui ) {

        }
    });

});