<?php
//header('Content-Type: text/html; charset=utf-8');
//setlocale(LC_ALL, 'ru_RU.UTF-8', 'rus');

      function get_micro_time()
      {
           list($usec, $sec) = explode(" ",microtime());
           return ((float)$usec + (float)$sec);
      }
      
      
      function translitIt($text, $direct = 'ru_en') 
        { 
         
         
            $L['ru'] = array( 
                              'Ё', 'Ж', 'Ц', 'Ч', 'Щ', 'Ш', 'Ы',  
                              'Э', 'Ю', 'Я', 'ё', 'ж', 'ц', 'ч',  
                              'ш', 'щ', 'ы', 'э', 'ю', 'я', 'А',  
                              'Б', 'В', 'Г', 'Д', 'Е', 'З', 'И',  
                              'Й', 'К', 'Л', 'М', 'Н', 'О', 'П',  
                              'Р', 'С', 'Т', 'У', 'Ф', 'Х', 'Ъ',  
                              'Ь', 'а', 'б', 'в', 'г', 'д', 'е',  
                              'з', 'и', 'й', 'к', 'л', 'м', 'н',  
                              'о', 'п', 'р', 'с', 'т', 'у', 'ф',  
                              'х', 'ъ', 'ь', 'é' 
                            ); 
    
                     
            $L['en'] = array( 
                              "yo", "zh",  "cz", "ch", "shh","sh", "yy",  
                              "ee", "yu",  "ya", "yo", "zh", "cz", "ch",  
                              "sh", "shh", "yy", "ee", "yu", "ya", "A",  
                              "b" , "v" ,  "g",  "d",  "e",  "z",  "i",  
                              "j",  "k",   "l",  "m",  "n",  "o",  "p",  
                              "r",  "s",   "t",  "u",  "f",  "x",  "", 
                              "",  "a",   "b",  "v",  "g",  "d",  "e",  
                              "z",  "i",   "j",  "k",  "l",  "m",  "n",   
                              "o",  "p",   "r",  "s",  "t",  "u",  "f",   
                              "x",  "",  "", "" 
                            ); 
                      
             
            // Конвертируем хилый и немощный в великий и могучий... 
            if($direct == 'en_ru') 
            { 
                $translated = str_replace($L['en'], $L['ru'], $text);         
                // Теперь осталось проверить регистр мягкого и твердого знаков. 
                $translated = preg_replace('/(?<=[а-яё])Ь/u', 'ь', $translated); 
                $translated = preg_replace('/(?<=[а-яё])Ъ/u', 'ъ', $translated); 
            } 
            else // И наоборот 
                $translated = str_replace($L['ru'], $L['en'], $text);         
            // Возвращаем получателю. 
            return $translated; 
        } 
        
        function adapt_head($str){
            $str = str_replace(array('&','amp;'),'',$str);
            
            $str = translitIt($str);
            $str = str_replace(array(',','\'','.',' ', '&'),'_',$str);
            
            $str = str_replace(array('/','\\',' ','&nbsp;','-'),'_',$str);
            $str = preg_replace('%[^A-Za-z0-9|(\_)]%', '', $str);
            $str = str_replace(chr(194).chr(160),'',$str);
            $str = mb_strtolower(trim($str));
            return $str;
        }



    
?>