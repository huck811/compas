<?php

if (!defined('APP_COMPAS')) {
  define('APP_COMPAS', true);
  define("BASE_SLASH_MAX", 10);

  if (!isset($testing) || !$testing)
    error_reporting(E_ALL & ~E_NOTICE & ~E_WARNING & ~E_STRICT);
  else
    error_reporting(E_ALL);

  if (file_exists("config.php")){
      include "config.php";
      set_include_path($_SERVER['DOCUMENT_ROOT'].BASE_URL);
      include "geo/cache.php";
      include "service.php";
      include "autoloader.php";
      include "geo/geo.php";

  }
  else{

      for ($i=1; $i<BASE_SLASH_MAX; $i++){
          $base = str_repeat('../',$i);
          if (file_exists($base."config.php")){
              include $base."config.php";
              break;
          }
      }
      set_include_path($_SERVER['DOCUMENT_ROOT'].BASE_URL);
      include "geo/cache.php";
      include "geo/class/app/service.php";
      include "geo/class/app/autoloader.php";
      include "geo/geo.php";
  }
}

?>