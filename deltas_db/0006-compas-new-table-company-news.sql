-- phpMyAdmin SQL Dump
-- version 4.2.7
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Сен 29 2014 г., 15:23
-- Версия сервера: 5.5.25
-- Версия PHP: 5.3.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- База данных: `compas`
--

-- --------------------------------------------------------

--
-- Структура таблицы `tsj35_company_news`
--

CREATE TABLE IF NOT EXISTS `tsj35_company_news` (
`id` int(11) unsigned NOT NULL,
  `user_id` int(11) unsigned NOT NULL,
  `head` varchar(256) COLLATE utf8_bin NOT NULL,
  `text` varchar(10000) COLLATE utf8_bin NOT NULL,
  `images` varchar(5000) COLLATE utf8_bin NOT NULL,
  `create_date` datetime NOT NULL,
  `recall_date` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=14 ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tsj35_company_news`
--
ALTER TABLE `tsj35_company_news`
 ADD PRIMARY KEY (`id`), ADD KEY `user_id` (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tsj35_company_news`
--
ALTER TABLE `tsj35_company_news`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;