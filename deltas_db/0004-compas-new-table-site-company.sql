-- phpMyAdmin SQL Dump
-- version 4.2.7
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Сен 09 2014 г., 22:09
-- Версия сервера: 5.5.25
-- Версия PHP: 5.3.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- База данных: `compas`
--

-- --------------------------------------------------------

--
-- Структура таблицы `tsj35_company_profile`
--

CREATE TABLE IF NOT EXISTS `tsj35_company_profile` (
  `user_id` int(11) NOT NULL,
  `company_name` varchar(256) COLLATE utf8_bin NOT NULL,
  `logo` varchar(512) COLLATE utf8_bin NOT NULL,
  `short_descript` varchar(5000) COLLATE utf8_bin NOT NULL,
  `full_descript` text COLLATE utf8_bin NOT NULL,
  `name` varchar(256) COLLATE utf8_bin NOT NULL,
  `phone1` varchar(50) COLLATE utf8_bin NOT NULL,
  `phone2` varchar(50) COLLATE utf8_bin NOT NULL,
  `adress` varchar(512) COLLATE utf8_bin NOT NULL,
  `email` varchar(256) COLLATE utf8_bin NOT NULL,
  `company_phone1` varchar(50) COLLATE utf8_bin NOT NULL,
  `company_phone2` varchar(50) COLLATE utf8_bin NOT NULL,
  `company_phone3` varchar(50) COLLATE utf8_bin NOT NULL,
  `company_adress` varchar(512) COLLATE utf8_bin NOT NULL,
  `company_email` varchar(256) COLLATE utf8_bin NOT NULL,
  `company_site` varchar(256) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1 ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tsj35_company_profile`
--
ALTER TABLE `tsj35_company_profile`
 ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tsj35_company_profile`
--
ALTER TABLE `tsj35_company_profile`