ALTER TABLE tsj35_adsmanager_ads MODIFY COLUMN ad_text VARCHAR(10000);
ALTER TABLE tsj35_adsmanager_ads MODIFY COLUMN ad_headline VARCHAR(500);
ALTER TABLE tsj35_adsmanager_ads MODIFY COLUMN ad_city INT(11);
ALTER TABLE tsj35_adsmanager_ads MODIFY COLUMN ad_map VARCHAR(2500);
ALTER TABLE tsj35_adsmanager_ads MODIFY COLUMN email VARCHAR(254);
ALTER TABLE tsj35_adsmanager_ads ADD INDEX(ad_text);
ALTER TABLE tsj35_adsmanager_ads ADD INDEX(ad_headline);
ALTER TABLE `tsj35_adsmanager_ads` ADD FULLTEXT(`ad_headline`);