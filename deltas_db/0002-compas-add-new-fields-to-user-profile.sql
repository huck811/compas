-- удаляем лишние поля из профиля
ALTER TABLE tsj35_adsmanager_profile DROP COLUMN company_phone;
ALTER TABLE tsj35_adsmanager_profile DROP COLUMN ad_phone;
ALTER TABLE tsj35_adsmanager_profile DROP COLUMN region;
ALTER TABLE tsj35_adsmanager_profile DROP COLUMN brands;

-- добавляем недостающие поля
ALTER TABLE tsj35_adsmanager_profile ADD COLUMN phone_predstav VARCHAR(50);
ALTER TABLE tsj35_adsmanager_profile ADD COLUMN phone_predstav_1 VARCHAR(50);
ALTER TABLE tsj35_adsmanager_profile ADD COLUMN phone   VARCHAR(50);
ALTER TABLE tsj35_adsmanager_profile ADD COLUMN phone_1 VARCHAR(50);
ALTER TABLE tsj35_adsmanager_profile ADD COLUMN phone_2 VARCHAR(50);
ALTER TABLE tsj35_adsmanager_profile ADD COLUMN near_metro VARCHAR(512);
ALTER TABLE tsj35_adsmanager_profile ADD COLUMN work_type VARCHAR(256);
ALTER TABLE tsj35_adsmanager_profile ADD COLUMN cost_category VARCHAR(60);
ALTER TABLE tsj35_adsmanager_profile ADD COLUMN categories VARCHAR(512);
ALTER TABLE tsj35_adsmanager_profile ADD COLUMN raiyon VARCHAR(256);
ALTER TABLE tsj35_adsmanager_ads MODIFY COLUMN ad_kindof VARCHAR(60);

CREATE TABLE tsj35_profile_brands (user_id INT(11), brand_id INT(11));
CREATE TABLE tsj35_profile_categories (user_id INT(11), cat_id INT(11));