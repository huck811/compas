-- разделение по уровням
-- parent  = 0  - округ / линия метро
-- parent != 0 - район / станция метро
ALTER TABLE tsj35_list_rayon ADD COLUMN parent int(11) DEFAULT 0;
ALTER TABLE tsj35_list_metro ADD COLUMN parent int(11) DEFAULT 0;
